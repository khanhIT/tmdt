<section class="awe-section-7">	
	<section class="section-news margin-bottom-20">
		<div class="container">
			<div class="blogs-content">
				<div class="heading a-center">
					<h2 class="title-head">					
						<a href="/cong-thuc">Công thức mới</a>					
					</h2>
				</div>
				<div class="list-blogs-link">
					<div class="row">	
						<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
							<div class="blog-item-frame">
								<div class="blog-item-thumbnail">								
									<a href="/cach-lam-banh-matcha-delight">						
										<img class="img-responsive" src="//bizweb.dktcdn.net/thumb/grande/100/312/791/articles/blog4-min.jpg?v=1527699885557" data-lazyload="//bizweb.dktcdn.net/thumb/grande/100/312/791/articles/blog4-min.jpg?v=1527699885557" alt="Cách làm bánh Matcha Delight">
									</a>
									<div class="post-time">
										31/05/2018
									</div>
								</div>
								<div class="blog-item-info">
									<h3 class="blog-item-name"><a href="/cach-lam-banh-matcha-delight" title="Cách làm bánh Matcha Delight">Cách làm bánh Matcha Delight</a></h3>							
									<p class="blog-item-summary margin-bottom-5 line-clamp">   Tuy có cách làm khá cầu kỳ với nhiều lớp bánh khác nhau, nhưng thành quả thì lại rất “ngọt ngào” với vị matcha thanh ngọt cùng vị beo béo của kem tươi. Matcha Delight chắc chắn sẽ làm bạn “đổ” n...</p>
								</div>
							</div>				
						</div>
														
						<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
							<div class="blog-item-frame">
								<div class="blog-item-thumbnail">								
									<a href="/cach-lam-mousse-chanh-leo">	
										<img class="img-responsive" src="//bizweb.dktcdn.net/thumb/grande/100/312/791/articles/blog3-min.jpg?v=1527699488943" data-lazyload="//bizweb.dktcdn.net/thumb/grande/100/312/791/articles/blog3-min.jpg?v=1527699488943" alt="Cách làm mousse chanh leo">	
									</a>
									<div class="post-time">
										30/05/2018
									</div>
								</div>
								<div class="blog-item-info">
									<h3 class="blog-item-name"><a href="/cach-lam-mousse-chanh-leo" title="Cách làm mousse chanh leo">Cách làm mousse chanh leo</a></h3>							
									<p class="blog-item-summary margin-bottom-5 line-clamp">   Mousse chanh leo là món bánh tráng miệng khá phổ biến và rất được yêu thích hiện nay. Bánh thơm mềm với vị chua chua, ngọt ngọt dễ ăn hẳn sẽ làm bạn thích mê ngay từ lần thưởng thức đầu tiên luô...</p>
								</div>
							</div>				
						</div>
									
						<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
							<div class="blog-item-frame">
								<div class="blog-item-thumbnail">								
									<a href="/cach-lam-banh-black-forest-me-hoac">				
										<img class="img-responsive" src="//bizweb.dktcdn.net/thumb/grande/100/312/791/articles/blog2-min.jpg?v=1527698391267" data-lazyload="//bizweb.dktcdn.net/thumb/grande/100/312/791/articles/blog2-min.jpg?v=1527698391267" alt="Cách làm bánh Black Forest mê hoặc">	
									</a>
									<div class="post-time">
										30/05/2018
									</div>
								</div>
								<div class="blog-item-info">
									<h3 class="blog-item-name"><a href="/cach-lam-banh-black-forest-me-hoac" title="Cách làm bánh Black Forest mê hoặc">Cách làm bánh Black Forest mê hoặc</a></h3>							
									<p class="blog-item-summary margin-bottom-5 line-clamp">   Là một món bánh truyền thống của Đức – Black Forest sẵn sàng làm bạn đổ ngay lập tức bởi cốt bánh xốp mềm kết hợp cùng chocolate đắng hảo hạng và cherry chua ngọt! Cùng Luz Bakery học cách làm b...</p>
								</div>
							</div>				
						</div>
								
						<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
							<div class="blog-item-frame">
								<div class="blog-item-thumbnail">								
									<a href="/cach-lam-banh-panna-cotta-ta-xanh-thom-mat">		
										<img class="img-responsive" src="//bizweb.dktcdn.net/thumb/grande/100/312/791/articles/blog1-min.jpg?v=1527697793133" data-lazyload="//bizweb.dktcdn.net/thumb/grande/100/312/791/articles/blog1-min.jpg?v=1527697793133" alt="Cách làm bánh Panna Cotta trà xanh thơm mát">
									</a>
									<div class="post-time">
										30/05/2018
									</div>
								</div>
								<div class="blog-item-info">
									<h3 class="blog-item-name"><a href="/cach-lam-banh-panna-cotta-ta-xanh-thom-mat" title="Cách làm bánh Panna Cotta trà xanh thơm mát">Cách làm bánh Panna Cotta trà xanh thơm mát</a></h3>							
									<p class="blog-item-summary margin-bottom-5 line-clamp">   Làm bánh panna cotta trà xanh thơm mát tại nhà không hề khó. Với những ai còn chưa biết, panna cotta là món tráng miệng đặc trưng của nước Ý. “Panna cotta” trong tiếng Ý có nghĩa là kem nấu, đây...</p>
								</div>
							</div>				
						</div>
						<div class="see-more">
							<a href="/cong-thuc">Xem tất cả</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</section>