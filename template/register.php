<!DOCTYPE html>
<html lang="vi">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <title>Cake</title>
        <meta name="description" content="">
        <meta name="keywords" content="Luz Bakery, cake">
        <meta name="revisit-after" content="1 days">
        <meta name="robots" content="noodp,index,follow">

        <?php include('style.php') ?>
        
    </head>

    <body>

    	<?php include('header.php') ?>

    	<div class="module-title">
			<div class="bg-frame"></div>
			<h2>
				<a href="javascript:void(0);">
					Đăng Ký
				</a>
			</h2>
			<section class="bread-crumb margin-bottom-10">
				<div class="container">
					<div class="row">
						<div class="col-xs-12" style="width: 100%">
							<ul class="breadcrumb" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">					
								<li class="home">
									<a itemprop="url" href="/" title="Trang chủ"><span itemprop="title">Trang chủ</span></a>						
									<span><i class="fa fa-angle-right"></i></span>
								</li>
								<li>Đăng Ký</li>
							</ul>
						</div>
					</div>
				</div>
			</section>
		</div>
		<div class="container mr-bottom-20">
			<div class="row">
				<div class="col-md-6 offset-md-3">
					<div class="page-login account-box-shadow">
						<div id="login">
							<h1 class="title-head text-center">Đăng ký tài khoản</h1>
							<div class="text-center">
								<span>Nếu chưa có tài khoản vui lòng đăng ký tại đây</span>
							</div>
							<form accept-charset="UTF-8" action="/account/register" id="customer_register" method="post" class="has-validation-callback">
								<input name="FormType" type="hidden" value="customer_register">
								<input name="utf8" type="hidden" value="true">
								<div class="form-signup" style="color: #e00;"></div>
								<div class="form-signup clearfix">
									<div class="row">
										<div class="col-md-12">
											<fieldset class="form-group">
												<label>Họ<span class="required">*</span></label>
												<input type="text" class="form-control form-control-lg" value="" name="lastName" id="lastName" placeholder="" required="" data-validation-error-msg="Không được để trống" data-validation="required">
											</fieldset>
										</div>
										<div class="col-md-12">
											<fieldset class="form-group">
												<label>Tên<span class="required">*</span></label>
												<input type="text" class="form-control form-control-lg" value="" name="firstName" id="firstName" placeholder="" required="" data-validation-error-msg="Không được để trống" data-validation="required">
											</fieldset>
										</div>
										<div class="col-md-12">
											<fieldset class="form-group">
												<label>Email<span class="required">*</span></label>
												<input type="email" class="form-control form-control-lg" data-validation="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,63}$" data-validation-error-msg="Email sai định dạng" value="" name="email" id="email" placeholder="" required="">
											</fieldset>
										</div>
										<div class="col-md-12">
											<fieldset class="form-group">
												<label>Mật khẩu<span class="required">*</span></label>
												<input type="password" class="form-control form-control-lg" value="" name="password" id="password" placeholder="" required="" data-validation-error-msg="Không được để trống" data-validation="required">
											</fieldset>
										</div>
										<div class="col-md-12 text-center" style="margin-top:15px; padding: 0">
											<button type="submit" value="Đăng ký" class="btn btn-style btn-blues">Đăng ký</button>
											<a href="login.php" class="btn-link-style btn-register btn-style-login" style="margin-left: 20px; color: #1c011f; text-decoration: underline;">Đăng nhập</a>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>

    	<?php include('footer.php') ?>

	</body>

        <?php include('script.php') ?>

</html>