<header class="header"> 
    <div class="under-header"></div>    
    <div class="container">     
        <div class="header-main d-none d-sm-none d-md-block d-lg-block">
            <div class="row">
                <div class="col-sm-3">
                    <div class="logo">
                        <a href="/" class="logo-wrapper ">                              
                            <img src="images/logo/logo.png" alt="logo Luz Bakery">
                        </a>                      
                    </div>
                </div>
                <div class="col-sm-9">
                    <nav>
                        <ul id="nav" class="nav">
                            
                            <li class="nav-item active">
                                <a class="nav-link" href="index.php">Trang chủ</a>
                            </li>
                            
                            <li class="nav-item ">
                                <a class="nav-link" href="intro.php">Giới thiệu</a>
                            </li>
                            
                            <li class="nav-item dropdown">
                                <a href="products.php" class="nav-link dropdown-toggle">Sản phẩm</a>                                    
                                <ul class="menu-ul">
                                     
                                    <li class="nav-item-lv2">
                                        <a class="nav-link" href="cookiesPie.php">Bánh Cookies</a>
                                    </li>
                                    
                                    <li class="nav-item-lv2">
                                        <a class="nav-link" href="coldPie.php">Bánh Lạnh</a>
                                    </li>
                                    
                                    <li class="nav-item-lv2">
                                        <a class="nav-link" href="pies.php">Bánh Nướng</a>
                                    </li>
                                    
                                    <li class="nav-item-lv2">
                                        <a class="nav-link" href="cake.php">Bánh Kem</a>
                                    </li>
                                
                                </ul>           
                            </li>
                            
                            <li class="nav-item ">
                                <a class="nav-link" href="recipes.php">Công thức</a>
                            </li>
                            
                            <li class="nav-item ">
                                <a class="nav-link" href="contact.php">Liên hệ</a>
                            </li>
                            
                            <li class="heading-search-frame pd-15">
                                <a href="javascript:void(0);">
                                    <i class="fa fa-search"></i>             
                                </a>
                                <div class="header_search search_form">
                                    <form class="input-group search-bar search_form has-validation-callback" action="/search" method="get" role="search">       
                                        <input type="search" name="query" value="" placeholder="Tìm kiếm... " class="input-group-field st-default-search-input search-text" autocomplete="off">
                                        <span class="input-group-btn">
                                            <button class="btn icon-fallback-text">
                                                <i class="fa fa-search"></i>
                                            </button>
                                        </span>
                                    </form>
                                </div>
                            </li>
                            <li class="heading-user-frame pd-15">
                                <a class="header-user-btn" href="/account">
                                    <i class="fa fa-user"></i>              
                                </a>
                                <div class="heading-user">
                                    
                                    <a href="login.php">Đăng nhập</a>
                                    <a href="register.php">Đăng ký</a>
                                        
                                </div>
                            </li>
                            <li class="heading-cart-frame pd-15">
                                <a href="cart.php">
                                    <i class="fa fa-shopping-bag icon-cart"></i>
                                    <span class="cartCount count_item_pr cart-total">0</span>
                                </a>
                            </li>
                        </ul>   
                    </nav>                  
                </div>              
            </div>
        </div>
        <div class="header-mobile d-lg-none d-md-none">
            <div class="row">               
                <div class="col-sm-4 col-12">
                    <div class="menu-bar d-md-none d-lg-none">
                        <img src="images/menubar.png" class="menu_bar" alt="menu bar">
                    </div>
                </div>
                <div class="col-sm-4 col-12 a-center">
                    <div class="logo">
                        
                        <a href="/" class="logo-wrapper ">                  
                            <img src="images/logo/logo.png" alt="logo ">                 
                        </a>
                                                
                    </div>
                </div>
                <div class="col-sm-4 col-12">
                    <ul class="mod-header"> 
                        <li class="heading-user-frame d-none d-sm-none">
                            <a class="header-user-btn" href="/account">
                                <i class="fa fa-user"></i>              
                            </a>
                            <div class="heading-user">
                                
                                <a href="login.php">Đăng nhập</a>
                                <a href="register.php">Đăng ký</a>
                                    
                            </div>
                        </li>
                        <li class="heading-cart-frame">
                            <a href="cart.php">
                                <i class="fa fa-shopping-bag icon-cart"></i>
                                <span class="cartCount count_item_pr cart-total">0</span>
                            </a>
                        </li>
                    </ul>   
                </div>
                <div class="col-sm-12 col-12">
                    <div class="menu-mobile menu-mobile-active" style="display: none;">
                        <ul class="nav-mobile">
                            <li class="nav-item active">
                                <a class="nav-link" href="/">Trang chủ</a>
                            </li>
                            <li class="nav-item ">
                                <a class="nav-link" href="/gioi-thieu">Giới thiệu</a>
                            </li>
                            <li class="li-has-subs nav-item ">
                                <a href="/collections/all" class="nav-link nav-menu">Sản phẩm 
                                </a>
                                <i class="fa fa-angle-down open-close sub_menu"></i>
                                <ul class="submenu" style="display: none;">
                                    <li class="nav-item-lv2">
                                        <a class="nav-link" href="/banh-cookies">Bánh Cookies</a>
                                    </li>
                                    <li class="nav-item-lv2">
                                        <a class="nav-link" href="/banh-lanh">Bánh Lạnh</a>
                                    </li>
                                    <li class="nav-item-lv2">
                                        <a class="nav-link" href="/banh-nuong">Bánh Nướng</a>
                                    </li>
                                    <li class="nav-item-lv2">
                                        <a class="nav-link" href="/banh-kem">Bánh Kem</a>
                                    </li>
                                </ul>           
                            </li>
                            <li class="nav-item ">
                                <a class="nav-link" href="/cong-thuc">Công thức</a>
                            </li>
                            <li class="nav-item ">
                                <a class="nav-link" href="/lien-he">Liên hệ</a>
                            </li>
                            <li class="nav-item search_mobile">                             
                                <div class="header_search search_form">
                                    <form class="input-group search-bar search_form has-validation-callback" action="/search" method="get" role="search">       
                                        <input type="search" name="query" value="" placeholder="Tìm kiếm... " class="input-group-field st-default-search-input search-text" autocomplete="off" style="width: 100%">
                                        <span class="input-group-btn">
                                            <button class="btn icon-fallback-text">
                                                <i class="fa fa-search"></i>
                                            </button>
                                        </span>
                                    </form>
                                </div>
                            </li>
                            <li class="nav-item account-mobile">    
                                <a class="nav-link" href="login.php">Đăng nhập</a> /
                                <a class="nav-link" href="register.php">Đăng ký</a>
                            </li>
                        </ul>   
                    </div>          
                </div>              
            </div>
        </div>
    </div>  
</header>