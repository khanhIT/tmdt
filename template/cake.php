<!DOCTYPE html>
<html lang="vi">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <title>Cake</title>
        <meta name="description" content="">
        <meta name="keywords" content="Luz Bakery, cake">
        <meta name="revisit-after" content="1 days">
        <meta name="robots" content="noodp,index,follow">

        <?php include('style.php') ?>
        
    </head>

    <body>

    	<?php include('header.php') ?>

    	<div class="module-title">
			<div class="bg-frame"></div>
			<h2>
				<a href="javascript:void(0);">
					Bánh Kem
			</h2>
			<section class="bread-crumb margin-bottom-10">
				<div class="container">
					<div class="row">
						<div class="col-xs-12" style="width: 100%">
							<ul class="breadcrumb" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">					
								<li class="home">
									<a itemprop="url" href="/" title="Trang chủ"><span itemprop="title">Trang chủ</span></a>						
									<span><i class="fa fa-angle-right"></i></span>
								</li>
								<li>Bánh Kem</li>
							</ul>
						</div>
					</div>
				</div>
			</section>
		</div>
		<div class="container">
			<div class="row">
				<?php include('aside.php') ?>
				<section class="main_container collection col-lg-9 col-md-9 col-lg-push-3 col-md-push-3">
					<h1 class="title-head margin-top-0">Bánh Kem</h1>		
					<div class="category-products products">
						<div class="sortPagiBar">
							<div class="row">
								<div class="hidden-xs col-sm-6">						
									
								</div>
								<div class="col-xs-12 col-sm-6 text-xs-left text-sm-right">
									<div id="sort-by">
										<label class="left hidden-xs">Sắp xếp: </label>
										<ul>
											<li>
												<span>Sắp xếp</span>
												<ul>								
													<li>
														<a href="javascript:void(0);">A → Z</a>
													</li>
													<li>
														<a href="javascript:void(0);">Z → A</a>
													</li>
													<li>
														<a href="javascript:void(0);">Giá tăng dần</a>
													</li>
													<li>
														<a href="javascript:void(0);">Giá giảm dần</a>
													</li>
													<li>
														<a href="javascript:void(0);">Hàng mới nhất</a>
													</li>
													<li>
														<a href="javascript:void(0);">Hàng cũ nhất</a>
													</li>
												</ul>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>	
						<section class="products-view products-view-gird">
							<div class="row">
								<?php for($i = 0; $i<=5; $i++) : ?>
								<div class="col-xs-6 col-sm-4 col-lg-4">
									<div class="product-box">
										<div class="product-thumbnail-frame">
											<div class="sale-flash">SALE</div>
											<a href="/sac-xuan">
												<img class="img-responsive" src="images/product/sp8-min-cream.jpg" data-lazyload="images/product/sp8-min-cream.jpg" alt="Sắc Xuân">
											</a>	
										</div>	
										<div class="product-info">
											<h5 class="product-name">
												<a href="/sac-xuan" title="Sắc Xuân">Ốc kem</a>
											</h5>		
											<div class="price-box clearfix">
												<div class="special-price">
													<span class="price product-price">Giá: 240.000₫</span>
												</div>		
												<div class="old-price">                                
			                                        <span class="price product-price-old">
			                                            55.000₫         
			                                        </span>
			                                    </div>									
											</div>
											<div class="product-action clearfix">
												<form action="/cart/add" method="post" class="variants form-nut-grid has-validation-callback" data-id="product-actions-11803975" enctype="multipart/form-data">
													<div>
														<input type="hidden" name="variantId" value="18783371">
														<button class="add2cart btn-buy btn-cart btn btn-gray left-to btn-primary add_to_cart" title="Thêm vào giỏ"><span>
															Thêm vào giỏ</span>
														</button>
														<a href="/sac-xuan" data-handle="sac-xuan" class="btn-white btn_view btn right-to quick-view"><i class="fa fa-search"></i></a>		
													</div>
												</form>
											</div>		
										</div>	
									</div>
								</div>
								<?php endfor; ?>
							</div>
						</section>
						<div class="shop-pag text-xs-right">
							<nav class="pagination-frame">
							  <ul class="pagination clearfix">
							    <li class="page-item">
							    	<a class="page-link" href="javascript:;">
							    		<i class="fa fa-angle-left"></i>
							    	</a>
							    </li>
							    <li class="page-item">
							    	<a class="page-link" href="javascript:;">1</a>	
							    </li>
							  	<li class="active page-item disabled">
							  		<a class="page-link" href="javascript:;">2</a>
							  	</li>
							    <li class="page-item">
							    	<a class="page-link" href="javascript:;">3</a>
							    </li>
							    <li class="page-item">
							    	<a class="page-link" href="javascript:;">
							    		<i class="fa fa-angle-right"></i>
							    	</a>
							    </li>
							  </ul>
							</nav>
						</div>
					</div>
				</section>
			</div>
		</div>

    	<?php include('footer.php') ?>

	</body>

        <?php include('script.php') ?>

</html>