<aside class="sidebar left-content col-lg-3 col-md-3 col-lg-pull-9 col-md-pull-9">
	<aside class="aside-item collection-category">
		<div class="aside-title">
			<h3 class="title-head"><span>Danh mục sản phẩm</span></h3>
		</div>
		<div class="aside-content">
			<nav class="nav-category navbar-toggleable-md">
				<ul class="nav navbar-pills">
					<li class="nav-item ">
						<a class="nav-link" href="/banh-cookies">Bánh Cookies</a>
					</li>
					<li class="nav-item ">
						<a class="nav-link" href="/banh-lanh">Bánh Lạnh</a>
					</li>
					<li class="nav-item ">
						<a class="nav-link" href="/banh-nuong">Bánh Nướng</a>
					</li>
					<li class="nav-item ">
						<a class="nav-link" href="/banh-kem">Bánh Kem</a>
					</li>
				</ul>
			</nav>
		</div>
	</aside>
	<div class="aside-item aside-filter collection-category">	
		<div class="aside-hidden-mobile">
			<div class="filter-container">
				<aside class="aside-item filter-price">
					<div class="aside-title">
						<h2 class="title-head margin-top-0"><span>Giá sản phẩm</span></h2>
					</div>
					<div class="aside-content filter-group">
						<ul class="filter">
							<li class="filter-item filter-item--check-box filter-item--green">
								<span>
									<label for="filter-duoi-100-000d">
										<input type="checkbox" id="filter-duoi-100-000d" data-group="Khoảng giá" data-field="price_min" data-text="Dưới 100.000đ" value="(<100000)" data-operator="OR">
										<i class="fa"></i>
										Giá dưới 100.000đ
									</label>
								</span>
							</li>
							<li class="filter-item filter-item--check-box filter-item--green">
								<span>
									<label for="filter-100-000d-200-000d">
										<input type="checkbox" id="filter-100-000d-200-000d" onchange="toggleFilter(this)" data-group="Khoảng giá" data-field="price_min" data-text="100.000đ - 200.000đ" value="(>100000 AND <200000)" data-operator="OR">
										<i class="fa"></i>
										100.000đ - 200.000đ							
									</label>
								</span>
							</li>	
							<li class="filter-item filter-item--check-box filter-item--green">
								<span>
									<label for="filter-200-000d-300-000d">
										<input type="checkbox" id="filter-200-000d-300-000d" onchange="toggleFilter(this)" data-group="Khoảng giá" data-field="price_min" data-text="200.000đ - 300.000đ" value="(>200000 AND <300000)" data-operator="OR">
										<i class="fa"></i>
										200.000đ - 300.000đ							
									</label>
								</span>
							</li>	
							<li class="filter-item filter-item--check-box filter-item--green">
								<span>
									<label for="filter-300-000d-500-000d">
										<input type="checkbox" id="filter-300-000d-500-000d" onchange="toggleFilter(this)" data-group="Khoảng giá" data-field="price_min" data-text="300.000đ - 500.000đ" value="(>300000 AND <500000)" data-operator="OR">
										<i class="fa"></i>
										300.000đ - 500.000đ							
									</label>
								</span>
							</li>	
							<li class="filter-item filter-item--check-box filter-item--green">
								<span>
									<label for="filter-500-000d-1-000-000d">
										<input type="checkbox" id="filter-500-000d-1-000-000d" onchange="toggleFilter(this)" data-group="Khoảng giá" data-field="price_min" data-text="500.000đ - 1.000.000đ" value="(>500000 AND <1000000)" data-operator="OR">
										<i class="fa"></i>
										500.000đ - 1.000.000đ							
									</label>
								</span>
							</li>	
							<li class="filter-item filter-item--check-box filter-item--green">
								<span>
									<label for="filter-tren1-000-000d">
										<input type="checkbox" id="filter-tren1-000-000d" onchange="toggleFilter(this)" data-group="Khoảng giá" data-field="price_min" data-text="Trên 1.000.000đ" value="(>1000000)" data-operator="OR">
										<i class="fa"></i>
										Giá trên 1.000.000đ
									</label>
								</span>
							</li>							
						</ul>
					</div>
				</aside>
				
				<aside class="aside-item filter-vendor">
					<div class="aside-title">
						<h2 class="title-head margin-top-0"><span>Quốc gia</span></h2>
					</div>
					<div class="aside-content filter-group">
						<ul class="filter-vendor">
							<li class="filter-item filter-item--check-box filter-item--green ">
								<label data-filter="hàn quốc" for="filter-han-quoc" class="han-quoc">
									<input type="checkbox" id="filter-han-quoc" onchange="toggleFilter(this)" data-group="Hãng" data-field="vendor" data-text="Hàn Quốc" value="(Hàn Quốc)" data-operator="OR">
									<i class="fa"></i>
									
									Hàn Quốc
									
								</label>
							</li>
							<li class="filter-item filter-item--check-box filter-item--green ">
								<label data-filter="việt nam" for="filter-viet-nam" class="viet-nam">
									<input type="checkbox" id="filter-viet-nam" onchange="toggleFilter(this)" data-group="Hãng" data-field="vendor" data-text="Việt Nam" value="(Việt Nam)" data-operator="OR">
									<i class="fa"></i>
									
									Việt Nam
									
								</label>
							</li>
							
							
						</ul>
					</div>
				</aside>
				
				
				<aside class="aside-item filter-type">
					<div class="aside-title">
						<h2 class="title-head margin-top-0"><span>Hương vị</span></h2>
					</div>
					<div class="aside-content filter-group">
						<ul class="filter-type filter">
							<li class="filter-item filter-item--check-box filter-item--green">
								<label data-filter="caramel" for="filter-caramel">
									<input type="checkbox" id="filter-caramel" onchange="toggleFilter(this)" data-group="Loại" data-field="product_type" data-text="Caramel" value="(Caramel)" data-operator="OR">
									<i class="fa"></i>
									Caramel
								</label>
							</li>
							<li class="filter-item filter-item--check-box filter-item--green">
								<label data-filter="coconut" for="filter-coconut">
									<input type="checkbox" id="filter-coconut" onchange="toggleFilter(this)" data-group="Loại" data-field="product_type" data-text="Coconut" value="(Coconut)" data-operator="OR">
									<i class="fa"></i>
									Coconut
								</label>
							</li>
							<li class="filter-item filter-item--check-box filter-item--green">
								<label data-filter="raisin" for="filter-raisin">
									<input type="checkbox" id="filter-raisin" onchange="toggleFilter(this)" data-group="Loại" data-field="product_type" data-text="Raisin" value="(Raisin)" data-operator="OR">
									<i class="fa"></i>
									Raisin
								</label>
							</li>
							<li class="filter-item filter-item--check-box filter-item--green">
								<label data-filter="socola" for="filter-socola">
									<input type="checkbox" id="filter-socola" onchange="toggleFilter(this)" data-group="Loại" data-field="product_type" data-text="Socola" value="(Socola)" data-operator="OR">
									<i class="fa"></i>
									Socola
								</label>
							</li>
							<li class="filter-item filter-item--check-box filter-item--green">
								<label data-filter="suger" for="filter-suger">
									<input type="checkbox" id="filter-suger" onchange="toggleFilter(this)" data-group="Loại" data-field="product_type" data-text="Suger" value="(Suger)" data-operator="OR">
									<i class="fa"></i>
									Suger
								</label>
							</li>
							<li class="filter-item filter-item--check-box filter-item--green">
								<label data-filter="vani" for="filter-vani">
									<input type="checkbox" id="filter-vani" onchange="toggleFilter(this)" data-group="Loại" data-field="product_type" data-text="Vani" value="(Vani)" data-operator="OR">
									<i class="fa"></i>
									Vani
								</label>
							</li>
						</ul>
					</div>
				</aside>	
			</div>
		</div>
	</div>

	<aside class="aside-item banner hidden-sm hidden-xs">
		<div class="aside-title">
			<h3 class="title-head margin-top-0"><span>Khuyến mãi hot</span></h3>
		</div>
		<div class="aside-content">
			<div class="col-banner">
				<a href="#">
					<img src="//bizweb.dktcdn.net/100/312/791/themes/667098/assets/aside_banner.png?1535770651792" alt="banner" class="banner-img">
				</a>
			</div>
		</div>
	</aside>
</aside>