<section class="awe-section-3"> 
    <section class="section-selling margin-bottom-20">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 selling-content">             
                    <div class="heading a-center">
                        <h2 class="title-head">                 
                            <a href="#">Sản phẩm bán chạy</a>                  
                        </h2>
                    </div>          
                    <div id="owl-demo-2" class="owl-carousel owl-theme">
                        <article class="thumbnail item" itemscope="" itemtype="http://schema.org/CreativeWork">
                            <div class="product-thumbnail-frame">
                                <div class="sale-flash">SALE</div>
                                <a class="blog-thumb-img" href="productDetail.php" title="">
                                    <img src="images/product/sp1a-min.jpg" class="img-responsive" />
                                </a>
                            </div>
                            <div class="caption">
                                <h5 itemprop="headline" class="product-name">
                                    <a href="productDetail.php" rel="bookmark">Tiramisu</a>
                                </h5>
                                <div class="price-box clearfix">            
                                    <div class="special-price">
                                        <span class="price product-price">Giá: 35.000₫</span>
                                    </div>
                                    
                                    <div class="old-price">                                
                                        <span class="price product-price-old">
                                            55.000₫         
                                        </span>
                                    </div>    
                                </div>
                                <div class="product-action clearfix">
                                    <form action="" method="post" class="variants form-nut-grid has-validation-callback" data-id="product-actions-11796111" enctype="multipart/form-data">
                                        <div>
                                            <input type="hidden" name="variantId" value="18765980">
                                            <button class="add2cart btn-buy btn-cart btn btn-gray left-to btn-primary add_to_cart" title="Thêm vào giỏ">
                                                <span>Thêm vào giỏ</span>
                                            </button>
                                            <a href="javascript:void(0)" data-handle="banh-kem-oreo" class="btn-white btn_view btn right-to quick-view">
                                                <i class="fa fa-search"></i>
                                            </a>               
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </article>

                        <article class="thumbnail item" itemscope="" itemtype="http://schema.org/CreativeWork">
                            <div class="product-thumbnail-frame">
                                <div class="sale-flash">SALE</div>
                                <a class="blog-thumb-img" href="productDetail.php" title="">
                                    <img src="images/product/sp2a-min.jpg" class="img-responsive" />
                                </a>
                            </div>
                            <div class="caption">
                                <h5 itemprop="headline" class="product-name">
                                    <a href="productDetail.php" rel="bookmark">Mousse Socola</a>
                                </h5>
                                <div class="price-box clearfix">            
                                    <div class="special-price">
                                        <span class="price product-price">Giá: 35.000₫</span>
                                    </div>
                                    
                                    <div class="old-price">                                  
                                        <span class="price product-price-old">
                                            55.000₫         
                                        </span>
                                    </div>          
                                </div>
                                <div class="product-action clearfix">
                                    <form action="" method="post" class="variants form-nut-grid has-validation-callback" data-id="product-actions-11796111" enctype="multipart/form-data">
                                        <div>
                                            <input type="hidden" name="variantId" value="18765980">
                                            <button class="add2cart btn-buy btn-cart btn btn-gray left-to btn-primary add_to_cart" title="Thêm vào giỏ">
                                                <span>Thêm vào giỏ</span>
                                            </button>
                                            <a href="javascript:void(0)" data-handle="banh-kem-oreo" class="btn-white btn_view btn right-to quick-view">
                                                <i class="fa fa-search"></i>
                                            </a>               
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </article>
                        
                        <article class="thumbnail item" itemscope="" itemtype="http://schema.org/CreativeWork">
                            <div class="product-thumbnail-frame">
                                <div class="sale-flash">SALE</div>
                                <a class="blog-thumb-img" href="#" title="">
                                    <img src="images/product/sp3-min.jpg" class="img-responsive" />
                                </a>
                            </div>
                            <div class="caption">
                                <h5 itemprop="headline" class="product-name">
                                    <a href="#" rel="bookmark">Cheesecake</a>
                                </h5>
                                <div class="price-box clearfix">            
                                    <div class="special-price">
                                        <span class="price product-price">Giá: 35.000₫</span>
                                    </div>
                                    
                                    <div class="old-price">                                                   
                                        <span class="price product-price-old">
                                            55.000₫         
                                        </span>
                                    </div>          
                                </div>
                                <div class="product-action clearfix">
                                    <form action="" method="post" class="variants form-nut-grid has-validation-callback" data-id="product-actions-11796111" enctype="multipart/form-data">
                                        <div>
                                            <input type="hidden" name="variantId" value="18765980">
                                            <button class="add2cart btn-buy btn-cart btn btn-gray left-to btn-primary add_to_cart" title="Thêm vào giỏ">
                                                <span>Thêm vào giỏ</span>
                                            </button>
                                            <a href="javascript:void(0)" data-handle="banh-kem-oreo" class="btn-white btn_view btn right-to quick-view">
                                                <i class="fa fa-search"></i>
                                            </a>               
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </article>
                        
                        <article class="thumbnail item" itemscope="" itemtype="http://schema.org/CreativeWork">
                            <div class="product-thumbnail-frame">
                                <div class="sale-flash">SALE</div>
                                <a class="blog-thumb-img" href="#" title="">
                                    <img src="images/product/sp5-min.jpg" class="img-responsive" />
                                </a>
                            </div>
                            <div class="caption">
                                <h5 itemprop="headline" class="product-name">
                                    <a href="#" rel="bookmark">Bánh Kem Bắp</a>
                                </h5>
                                <div class="price-box clearfix">            
                                    <div class="special-price">
                                        <span class="price product-price">Giá: 35.000₫</span>
                                    </div>
                                    
                                    <div class="old-price">                                                   
                                        <span class="price product-price-old">
                                            55.000₫         
                                        </span>
                                    </div>        
                                </div>
                                <div class="product-action clearfix">
                                    <form action="" method="post" class="variants form-nut-grid has-validation-callback" data-id="product-actions-11796111" enctype="multipart/form-data">
                                        <div>
                                            <input type="hidden" name="variantId" value="18765980">
                                            <button class="add2cart btn-buy btn-cart btn btn-gray left-to btn-primary add_to_cart" title="Thêm vào giỏ">
                                                <span>Thêm vào giỏ</span>
                                            </button>
                                            <a href="javascript:void(0)" data-handle="banh-kem-oreo" class="btn-white btn_view btn right-to quick-view">
                                                <i class="fa fa-search"></i>
                                            </a>               
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </article>
                        
                        <article class="thumbnail item" itemscope="" itemtype="http://schema.org/CreativeWork">
                            <div class="product-thumbnail-frame">
                                <div class="sale-flash">SALE</div>
                                <a class="blog-thumb-img" href="#" title="">
                                    <img src="images/product/sp7a-min.jpg" class="img-responsive" />
                                </a>
                            </div>
                            <div class="caption">
                                <h5 itemprop="headline" class="product-name">
                                    <a href="#" rel="bookmark">Bánh Kem Oreo</a>
                                </h5>
                                <div class="price-box clearfix">            
                                    <div class="special-price">
                                        <span class="price product-price">Giá: 35.000₫</span>
                                    </div>
                                    
                                    <div class="old-price">                                                   
                                        <span class="price product-price-old">
                                            55.000₫         
                                        </span>
                                    </div>        
                                </div>
                                <div class="product-action clearfix">
                                    <form action="" method="post" class="variants form-nut-grid has-validation-callback" data-id="product-actions-11796111" enctype="multipart/form-data">
                                        <div>
                                            <input type="hidden" name="variantId" value="18765980">
                                            <button class="add2cart btn-buy btn-cart btn btn-gray left-to btn-primary add_to_cart" title="Thêm vào giỏ">
                                                <span>Thêm vào giỏ</span>
                                            </button>
                                            <a href="javascript:void(0)" data-handle="banh-kem-oreo" class="btn-white btn_view btn right-to quick-view">
                                                <i class="fa fa-search"></i>
                                            </a>               
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </article>
                        
                        <article class="thumbnail item" itemscope="" itemtype="http://schema.org/CreativeWork">
                            <div class="product-thumbnail-frame">
                                <div class="sale-flash">SALE</div>
                                <a class="blog-thumb-img" href="#" title="">
                                    <img src="images/product/sp8-min.jpg" class="img-responsive" />
                                </a>
                            </div>
                            <div class="caption">
                                <h5 itemprop="headline" class="product-name">
                                    <a href="#" rel="bookmark">Bánh Kem Phô Mai Dâu</a>
                                </h5>
                                <div class="price-box clearfix">            
                                    <div class="special-price">
                                        <span class="price product-price">Giá: 35.000₫</span>
                                    </div>
                                    
                                    <div class="old-price">                                                          
                                        <span class="price product-price-old">
                                            55.000₫         
                                        </span>
                                    </div>       
                                </div>
                                <div class="product-action clearfix">
                                    <form action="" method="post" class="variants form-nut-grid has-validation-callback" data-id="product-actions-11796111" enctype="multipart/form-data">
                                        <div>
                                            <input type="hidden" name="variantId" value="18765980">
                                            <button class="add2cart btn-buy btn-cart btn btn-gray left-to btn-primary add_to_cart" title="Thêm vào giỏ">
                                                <span>Thêm vào giỏ</span>
                                            </button>
                                            <a href="javascript:void(0)" data-handle="banh-kem-oreo" class="btn-white btn_view btn right-to quick-view">
                                                <i class="fa fa-search"></i>
                                            </a>               
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </article>
                    </div><!-- #owl-demo-2 -->  
                </div>
            </div>
        </div>
    </section>
</section>