<!DOCTYPE html>
<html lang="vi">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <title>Cake</title>
        <meta name="description" content="">
        <meta name="keywords" content="Luz Bakery, cake">
        <meta name="revisit-after" content="1 days">
        <meta name="robots" content="noodp,index,follow">

        <?php include('style.php') ?>
        
    </head>

    <body class="body--custom-background-color ">
        <div class="banner" data-header="">
            <div class="wrap">
                <div class="shop logo logo--left ">
                    <h1 class="shop__name">
                        <a href="/" class="color">
                            Luz Bakery
                        </a>
                    </h1>
                </div>
            </div>
        </div>

        <button class="order-summary-toggle">
            <div class="wrap">
                <h5>
                    <label class="control-label">Đơn hàng</label>
                    <label class="control-label d-sm-noneall-device">
                        (1 sản phẩm)
                    </label>
                </h5>
            </div>
        </button>

		<form method="post" action="" data-toggle="validator" class="content stateful-form formCheckout" novalidate="true">
	        <div class="wrap" context="checkout">
	            <div class="sidebar ">
	                <div class="sidebar_header">
	                    <h5>
	                        <label class="control-label">Đơn hàng</label>
	                        <label class="control-label">(1 sản phẩm)</label>
	                    </h5>
	                    <hr class="full_width">
	                </div>
	                <div class="sidebar__content">
	                    <div class="order-summary order-summary--product-list order-summary--is-collapsed">
	                        <div class="summary-body summary-section summary-product">
	                            <div class="summary-product-list">
	                                <table class="product-table">
	                                    <tbody>
                                            <tr class="product product-has-image clearfix">
                                                <td>
                                                    <div class="product-thumbnail">
                                                        <div class="product-thumbnail__wrapper">
                                                            <img src="//bizweb.dktcdn.net/thumb/thumb/100/312/791/products/sp7a-min.jpg?v=1532245721370" class="product-thumbnail__image">
                                                        </div>
                                                        <span class="product-thumbnail__quantity" aria-hidden="true">1</span>
                                                    </div>
                                                </td>
                                                <td class="product-info">
                                                    <span class="product-info-name">
                                                        Bánh Kem Oreo
                                                    </span>
                                                </td>
                                                <td class="product-price text-right">
                                                    32.000₫
                                                </td>
                                            </tr>
	                                    </tbody>
	                                </table>
	                                <div class="order-summary__scroll-indicator">
	                                    Cuộn chuột để xem thêm
	                                    <i class="fa fa-long-arrow-down" aria-hidden="true"></i>
	                                </div>
	                            </div>
	                        </div>
	                        <hr class="m0">
	                    </div>
	                    <div class="order-summary order-summary--discount">
	                        <div class="summary-section">
	                            <div class="form-group m0">
	                                <div class="field__input-btn-wrapper">
	                                    <div class="field__input-wrapper">
	                                        <input bind="code" name="code" type="text" class="form-control discount_code" placeholder="Nhập mã giảm giá" value="" id="checkout_reduction_code">
	                                    </div>
	                                    <button class="btn btn-primary event-voucher-apply" type="button">Áp dụng</button>
	                                </div>
	                            </div>
	                           
	                            <div class="error mt10" bind-show="inValidCode">
	                                Mã khuyến mãi không hợp lệ
	                            </div>
	                        </div>
	                        <hr class="m0">
	                    </div>
	                    <div class="order-summary order-summary--total-lines">
	                        <div class="summary-section border-top-none--mobile">
	                            <div class="total-line total-line-subtotal clearfix">
	                                <span class="total-line-name pull-left">
	                                    Tạm tính
	                                </span>
	                                <span class="total-line-subprice pull-right">32.000₫
	                                </span>
	                            </div>
	                            <div class="total-line total-line-shipping clearfix">
	                                <span class="total-line-name pull-left">
	                                    Phí vận chuyển
	                                </span>
	                                <span class="total-line-shipping pull-right">40.000₫</span>
	                            </div>
	                            <div class="total-line total-line-total clearfix">
	                                <span class="total-line-name pull-left">
	                                    Tổng cộng
	                                </span>
	                                <span class="total-line-price pull-right">72.000₫</span>
	                            </div>
	                        </div>
	                    </div>
	                    <div class="form-group clearfix d-sm-none d-none d-md-block d-lg-block">
	                        <div class="field__input-btn-wrapper mt10">
	                            <a class="previous-link color" href="cart.php">
	                                <i class="fa fa-angle-left fa-lg" aria-hidden="true"></i>
	                                <span>Quay về giỏ hàng</span>
	                            </a>
	                            <input class="btn btn-primary btn-checkout" data-loading-text="Đang xử lý" type="button" value="ĐẶT HÀNG">
	                        </div>
	                    </div>
	                    <div class="form-group has-error">
	                        <div class="help-block ">
	                            <ul class="list-unstyled">
	                                
	                            </ul>
	                        </div>
	                    </div>
	                </div>
	            </div>
	            <div class="main" role="main">
	                <div class="main_header">
	                    <div class="shop logo logo--left ">
					        <h1 class="shop__name">
					            <a href="/">
					                Luz Bakery
					            </a>
					        </h1>
					    </div>
	                </div>
	                <div class="main_content">
	                    <div class="row">
	                        <div class="col-md-6 col-lg-6">
	                            <div class="section">
	                                <div class="section__header">
	                                    <div class="layout-flex layout-flex--wrap">
	                                        <h2 class="section__title layout-flex__item layout-flex__item--stretch">
	                                            <i class="fa fa-id-card-o fa-lg section__title--icon d-md-none d-lg-none" aria-hidden="true"></i>
	                                            <label class="control-label">Thông tin mua hàng</label>
	                                        </h2>
                                            <a class="layout-flex__item section__title--link color" href="login.php">
                                                <i class="fa fa-user-circle-o fa-lg" aria-hidden="true"></i>
                                                Đăng nhập 
                                            </a>
	                                    </div>
	                                </div>
	                                <div class="section__content">
                                        <div class="form-group has-error">
                                            <div>
                                                <div class="field__input-wrapper" >
                                                    <span class="field__label" >
                                                        Email
                                                    </span>
                                                    <input name="Email" type="email" class="field__input form-control" id="_email" data-error="Vui lòng nhập email đúng định dạng" required="" value="" pattern="^([a-zA-Z0-9_\-\.\+]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$" bind="email">
                                                </div>
                                            </div>
                                            <div class="help-block with-errors">
                                            	<ul class="list-unstyled">
                                            		<li>Vui lòng nhập email đúng định dạng</li>
                                            	</ul>
                                            </div>
                                        </div>
	                                    <div class="billing">
	                                        <div>
	                                            <div class="form-group">
	                                                <div class="field__input-wrapper">
	                                                    <span class="field__label">
	                                                        Họ và tên
	                                                    </span>
	                                                    <input name="BillingAddress.LastName" type="text" class="field__input form-control" id="_billing_address_last_name" data-error="Vui lòng nhập họ tên" required="" bind="billing_address.full_name">
	                                                </div>
	                                                <div class="help-block with-errors"></div>
	                                            </div>
                                                <div class="form-group">
                                                    <div class="field__input-wrapper">
                                                        <span class="field__label">
                                                            Số điện thoại
                                                        </span>
                                                        <input name="BillingAddress.Phone" type="tel" class="field__input form-control" id="_billing_address_phone" data-error="Vui lòng nhập số điện thoại" pattern="^([0-9,\+,\-,\(,\),\.]{8,20})$" bind="billing_address.phone">
                                                    </div>
                                                    <div class="help-block with-errors"></div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="field__input-wrapper">
                                                        <span class="field__label">
                                                            Địa chỉ
                                                        </span>
                                                        <input name="BillingAddress.Address1" class="field__input form-control" id="_billing_address_address1" bind="billing_address.address1">
                                                    </div>
                                                    <div class="help-block with-errors"></div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="field__input-wrapper field__input-wrapper--select">
                                                        <div class="field__label" for="BillingProvinceId">
                                                            Tỉnh thành
                                                        </div>
                                                        <select class="field__input field__input--select form-control" name="BillingProvinceId" id="billingProvince" required="" data-error="Bạn chưa chọn tỉnh thành">
                                                            <option value="">--- Chọn tỉnh thành ---</option>
															<option value="1">Hà Nội</option>
														
															<option value="2">TP Hồ Chí Minh</option>
														
															<option value="3">An Giang</option>
														
															<option value="4">Bà Rịa-Vũng Tàu</option>
														
															<option value="5">Bắc Giang</option>
														
															<option value="6">Bắc Kạn</option>
														
															<option value="7">Bạc Liêu</option>
														
															<option value="8">Bắc Ninh</option>
														
															<option value="9">Bến Tre</option>
														
															<option value="10">Bình Định</option>
														
															<option value="11">Bình Dương</option>
														
															<option value="12">Bình Phước</option>
														
															<option value="13">Bình Thuận</option>
														
															<option value="14">Cà Mau</option>
														
															<option value="15">Cần Thơ</option>
														
															<option value="16">Cao Bằng</option>
														
															<option value="17">Đà Nẵng</option>
														
															<option value="18">Đắk Lắk</option>
														
															<option value="19">Đắk Nông</option>
														
															<option value="20">Điện Biên</option>
														
															<option value="21">Đồng Nai</option>
														
															<option value="22">Đồng Tháp</option>
														
															<option value="23">Gia Lai</option>
														
															<option value="24">Hà Giang</option>
														
															<option value="25">Hà Nam</option>
														
															<option value="26">Hà Tĩnh</option>
														
															<option value="27">Hải Dương</option>
														
															<option value="28">Hải Phòng</option>
														
															<option value="29">Hậu Giang</option>
														
															<option value="30">Hòa Bình</option>
														
															<option value="31">Hưng Yên</option>
														
															<option value="32">Khánh Hòa</option>
														
															<option value="33">Kiên Giang</option>
														
															<option value="34">Kon Tum</option>
														
															<option value="35">Lai Châu</option>
														
															<option value="36">Lâm Đồng</option>
														
															<option value="37">Lạng Sơn</option>
														
															<option value="38">Lào Cai</option>
														
															<option value="39">Long An</option>
														
															<option value="40">Nam Định</option>
														
															<option value="41">Nghệ An</option>
														
															<option value="42">Ninh Bình</option>
														
															<option value="43">Ninh Thuận</option>
														
															<option value="44">Phú Thọ</option>
														
															<option value="45">Phú Yên</option>
														
															<option value="46">Quảng Bình</option>
														
															<option value="47">Quảng Nam</option>
														
															<option value="48">Quảng Ngãi</option>
														
															<option value="49">Quảng Ninh</option>
														
															<option value="50">Quảng Trị</option>
														
															<option value="51">Sóc Trăng</option>
														
															<option value="52">Sơn La</option>
														
															<option value="53">Tây Ninh</option>
														
															<option value="54">Thái Bình</option>
														
															<option value="55">Thái Nguyên</option>
														
															<option value="56">Thanh Hóa</option>
														
															<option value="57">Thừa Thiên Huế</option>
														
															<option value="58">Tiền Giang</option>
														
															<option value="59">Trà Vinh</option>
														
															<option value="60">Tuyên Quang</option>
														
															<option value="61">Vĩnh Long</option>
														
															<option value="62">Vĩnh Phúc</option>
														
															<option value="63">Yên Bái</option>
                                                        </select>
                                                    </div>
                                                    <div class="help-block with-errors"></div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="field__input-wrapper field__input-wrapper--select">
                                                        <div class="field__label" for="BillingDistrictId">
                                                            Quận huyện
                                                        </div>
                                                        <select class="field__input field__input--select form-control" name="BillingDistrictId" id="billingDistrict">
                                                            <option value="">--- Chọn quận huyện ---</option>
                                                        </select>
                                                    </div>
                                                    <div class="help-block with-errors"></div>
                                                </div>
	                                            <div class="form-group">
	                                                <div class="error hide">
	                                                    <label>Khu vực này không hỗ trợ vận chuyển</label>
	                                                </div>
	                                            </div>
	                                        </div>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                        <div class="col-md-6 col-lg-6">
	                            <div class="section shipping-method">
	                                <div class="section__header">
	                                    <h2 class="section__title">
	                                        <i class="fa fa-truck fa-lg section__title--icon d-md-none d-lg-none" aria-hidden="true"></i>
	                                        <label class="control-label">Vận chuyển</label>
	                                    </h2>
	                                </div>
	                                <div class="section__content">
	                                    <div class="content-box">
	                                    	<div class="content-box__row">
	                                    		<div class="radio-wrapper">
	                                    			<div class="radio__input">
	                                    				<input class="input-radio" type="radio" value="379170,0" name="ShippingMethod" id="shipping_method_379170" 
	                                    				fee="40000">
	                                    			</div>
	                                    			<label class="radio__label" for="shipping_method_379170"> 
	                                    				<span class="radio__label__primary">Giao hàng tận nơi</span>
	                                    				<span class="radio__label__accessory">
	                                    					<span class="content-box__emphasis">40.000₫</span>
	                                    				</span>
	                                    			</label> 
	                                    		</div> <!-- /radio-wrapper--> 
	                                    	</div>
	                                    </div>
	                                </div>
	                            </div>
	                            <div class="section payment-methods">
	                                <div class="section__header">
	                                    <h2 class="section__title">
	                                        <i class="fa fa-credit-card fa-lg section__title--icon d-md-none d-lg-none" aria-hidden="true"></i>
	                                        <label class="control-label">Thanh toán</label>
	                                    </h2>
	                                </div>
	                                <div class="section__content">
	                                    <div class="content-box">
                        				    <div class="content-box__row">
                        				        <div class="radio-wrapper">
                        				            <div class="radio__input">
                        				                <input class="input-radio" type="radio" value="320697" name="PaymentMethodId" id="payment_method_320697" data-check-id="4" bind="payment_method_id" checked="">
                        				            </div>
                        				            <label class="radio__label" for="payment_method_320697">
                        				                <span class="radio__label__primary">Thanh toán khi giao hàng (COD)</span>
                        				                <span class="radio__label__accessory">
                        				                    <ul>
                        				                        <li class="payment-icon-v2 payment-icon--4">
                        											<i class="fa fa-money payment-icon-fa" aria-hidden="true"></i>
                        				                        </li>
                        				                    </ul>
                        				                </span>
                        				            </label> 
                        				        </div> <!-- /radio-wrapper--> 
                        				    </div>
                    				        <div class="radio-wrapper content-box__row content-box__row--secondary" id="payment-gateway-subfields-320697">
                    				            <div class="blank-slate">
                    				                <p>cod</p>
                    				            </div>
                    				        </div>
                            				<a href="javascript:void(0)" data-toggle="modal" data-target="#moca-modal" data-backdrop="static" data-keyboard="false" class="trigger-moca-modal" style="display: none;" title="Thanh toán qua Moca">
                            				</a>
                            				<a href="javascript:void(0)" data-toggle="modal" data-target="#qr-error-modal" class="trigger-qr-error-modal" style="display: none;" title="Lỗi thanh toán">
                            				</a>
                            				<a data-toggle="modal" data-target="#zalopay_modal" data-backdrop="static" data-keyboard="false" class="trigger-zalopay-modal" style="display: none;" title="Thanh toán qua ZaloPay">
                            				</a>
                            				<div class="modal fade moca-modal" id="moca-modal" tabindex="-1" role="dialog">
                            				    <div class="modal-dialog">
                            				        <div class="modal-content">
                            				            <div>
                            				                <iframe style="border: 0px; width: 100%; height: 100%;" src=""></iframe>
                            				            </div>
                            				        </div>
                            				    </div>
                            				</div>
                            				<div class="modal fade" id="qr-error-modal" data-width="" tabindex="-1" role="dialog">
                            				    <div class="modal-dialog">
                            				        <div class="modal-content">
                            				            <div class="modal-body">
                            				                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                            				                <div class="invalid_order">
                            				                    <p>Giao dịch của bạn chưa đủ hạn mức thanh toán</p>
                            				                    <p>Hạn mức tối thiểu để thanh toán được là <span>1đ</span></p>
                            				                    <p>Vui lòng chọn hình thức thanh toán khác</p>
                            				                </div>
                            								<div class="custom_error_message"></div>
                            				            </div>
                            				        </div>
                            				    </div>
                            				</div>
                            				<div class="modal fade zalopay_modal" id="zalopay_modal" tabindex="-1" role="dialog">
                            				    <div class="modal-dialog">
                            				        <div class="modal-content">
                            				            <div class="modal-body">
                            				                <div style="display:flex; justify-content: space-around;">
                            								    <div class="qr-wrapper">
                            								        <img>
                            										<div class="qr-timer-container">
                            										    Thời gian quét mã QR để thanh toán còn <span class="qr-timer" style="color:#4286f6;">300</span> giây
                            										</div>
                            								    </div>
                            									<div class="qr-guide-content">
                            									    <p><b>Thực hiện theo hướng dẫn sau để thanh toán:</b></p>
                            										<p>Bước 1: Mở ứng dụng ZaloPay</p>
                            										<p>Bước 2: Chọn "Thanh Toán" <img src="//bizweb.dktcdn.net/assets/images/barcode-zalo.png" class="zalopay-qr-payment-icon"> và quét mã QR code bên cạnh</p>
                            										<p>Bước 3: Hoàn thành các bước thanh toán theo hướng dẫn trên ứng dụng</p>
                            									</div>
                            								</div>
                            								<div style="justify-content: flex-end;display: flex;"><button type="button" class="btn btn-default" data-dismiss="modal">Hủy thanh toán</button></div>
                            				            </div>
                            				        </div>
                            				    </div>
                            				</div>
	                                    </div>
	                                </div>
	                            </div>
	                            <div class="section d-md-none d-lg-none">
	                                <div class="form-group clearfix m0">
	                                    <input class="btn btn-primary btn-checkout" data-loading-text="Đang xử lý" type="button" value="ĐẶT HÀNG">
	                                </div>
	                                <div class="text-center mt20">
	                                    <a class="previous-link color" href="cart.php">
	                                        <i class="fa fa-angle-left fa-lg" aria-hidden="true"></i>
	                                        <span>Quay về giỏ hàng</span>
	                                    </a>
	                                </div>
	                            </div>
	                        </div>
	                    </div>
	                </div>
                    <div class="main_footer footer unprint">
				        <div class="mt10"></div>
    				</div>
    				<div class="modal fade" id="refund-policy" data-width="" tabindex="-1" role="dialog">
    				    <div class="modal-dialog modal-lg">
    				        <div class="modal-content">
    				            <div class="modal-header">
    				                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
    				                <h4 class="modal-title">Chính sách hoàn trả</h4>
    				            </div>
    				            <div class="modal-body">
    				                <pre></pre>
    				            </div>
    				        </div>
    				    </div>
    				</div>
    				<div class="modal fade" id="privacy-policy" data-width="" tabindex="-1" role="dialog">
    				    <div class="modal-dialog modal-lg">
    				        <div class="modal-content">
    				            <div class="modal-header">
    				                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
    				                <h4 class="modal-title">Chính sách bảo mật</h4>
    				            </div>
    				            <div class="modal-body">
    				                <pre></pre>
    				            </div>
    				        </div>
    				    </div>
    				</div>
    				<div class="modal fade" id="terms-of-service" data-width="" tabindex="-1" role="dialog">
    				    <div class="modal-dialog modal-lg">
    				        <div class="modal-content">
    				            <div class="modal-header">
    				                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
    				                <h4 class="modal-title">Điều khoản sử dụng</h4>
    				            </div>
    				            <div class="modal-body">
    				                <pre></pre>
    				            </div>
    				        </div>
    				    </div>
				    </div>
                </div>
            </div>
    	</form>
	</body>

        <?php include('script.php') ?>

</html>