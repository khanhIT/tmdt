<!DOCTYPE html>
<html lang="vi">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <title>Cake</title>
        <meta name="description" content="">
        <meta name="keywords" content="Luz Bakery, cake">
        <meta name="revisit-after" content="1 days">
        <meta name="robots" content="noodp,index,follow">

        <?php include('style.php') ?>
        
    </head>

    <body>

    	<?php include('header.php') ?>

    	<div class="module-title">
			<div class="bg-frame"></div>
			<h2>
				<a href="javascript:void(0);">
					Giới thiệu
				</a>
			</h2>
			<section class="bread-crumb margin-bottom-10">
				<div class="container">
					<div class="row">
						<div class="col-xs-12" style="width: 100%">
							<ul class="breadcrumb" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">					
								<li class="home">
									<a itemprop="url" href="/" title="Trang chủ"><span itemprop="title">Trang chủ</span></a>						
									<span><i class="fa fa-angle-right"></i></span>
								</li>
								
								<li>Giới thiệu</li>
								
							</ul>
						</div>
					</div>
				</div>
			</section>
		</div>
		<section class="page">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12">
						<div class="page-title category-title">
							<h1 class="title-head"><a href="#">Giới thiệu</a></h1>
						</div>
						<div class="content-page rte">
							<p>Đắm mình trong không gian ồn ào nhộn nhịp của thành phố mang tên Bác, bạn không những được tận hưởng cảm xúc thi vị của Sài Gòn, mà còn được thưởng thức hương vị Pháp ngay trong lòng Sài Gòn.</p>
							<p>Nằm trên con phố đông đúc và cổ kính, Luz Bakery&nbsp;từ lâu đã trở thành điểm đến của những người yêu thích bánh ngọt Pháp. Với niềm đam mê về chất Pháp, Luz Bakery&nbsp;đã mang hương vị pháp “nguyên chất” đến với những thực khách Việt Nam.<br>
							<br>
							Luz Bakery&nbsp;là một thương hiệu nổi tiếng cho những ai yêu thích văn hóa ẩm thực Pháp. Nằm trên cái “chất” của Sài Gòn, chất của người tìm về cội nguồn văn hóa, du khách sẽ bắt gặp Luz Bakery tại Số 70 Lữ Gia, Quận 10. Vừa tìm hiểu về văn hóa Sài Gòn, du khách có thể thưởng thức hương vị Pháp tại đây, với một không gian rất nhộn nhịp&nbsp;của Sài&nbsp;Thành.&nbsp; &nbsp;<br>
							<br>
							Luz Bakery được sản xuất trên dây chuyền hiện đại, với những nguyên liệu được nhập khẩu trực tiếp từ các nước có truyền thống làm bánh lâu đời trên thế giới. Thực khách tới đây có thể thưởng thức rất nhiều loại bánh : Bánh Sinh Nhật, Bánh Cưới, Bánh Valentine, Bánh Giáng sinh… Barkery, Bánh mỳ Pháp, Pizza, Hotdog, Patechaux, Cookies, và cả Bánh Trung thu ….</p>
							<p>Tất cả những yếu tố đó đã tạo lên một thương hiệu&nbsp;Luz Bakery.</p>
						</div>
					</div>
				</div>
			</div>
		</section>

    	<?php include('footer.php') ?>

	</body>

        <?php include('script.php') ?>

</html>