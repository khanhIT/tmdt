<!DOCTYPE html>
<html lang="vi">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <title>Cake</title>
        <meta name="description" content="">
        <meta name="keywords" content="Luz Bakery, cake">
        <meta name="revisit-after" content="1 days">
        <meta name="robots" content="noodp,index,follow">

        <?php include('style.php') ?>
        
    </head>

    <body>

    	<?php include('header.php') ?>

    	<div class="module-title">
			<div class="bg-frame"></div>
			<h2>
				<a href="javascript:void(0);">
					Công thức
				</a>
			</h2>
			<section class="bread-crumb margin-bottom-10">
				<div class="container">
					<div class="row">
						<div class="col-xs-12" style="width: 100%">
							<ul class="breadcrumb" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">					
								<li class="home">
									<a itemprop="url" href="/" title="Trang chủ"><span itemprop="title">Trang chủ</span></a>						
									<span><i class="fa fa-angle-right"></i></span>
								</li>
								
								<li>Công thức</li>
								
							</ul>
						</div>
					</div>
				</div>
			</section>
		</div>
		<div class="luz-blog container" itemscope="" itemtype="http://schema.org/Blog">
			<meta itemprop="name" content="Công thức"> 
			<meta itemprop="description" content="Chủ đề không có mô tả"> 
			<div class="row">
				<aside class="sidebar left left-content col-md-3 col-md-pull-9">
					<div class="border-bg">
						<aside class="aside-item collection-category blog-category">	
							<div class="heading">
								<h2 class="title-head">
									<span>Danh mục</span>
								</h2>
							</div>	
							<div class="aside-content">
								<nav class="nav-category  navbar-toggleable-md">
									<ul class="nav navbar-pills">
										<li class="nav-item ">
											<a class="nav-link" href="/">Trang chủ</a>
										</li>
										<li class="nav-item ">
											<a class="nav-link" href="/gioi-thieu">Giới thiệu</a>
										</li>
										<li class="nav-item ">
											<a href="/collections/all" class="nav-link">Sản phẩm</a>
											<i class="fa fa-angle-down"></i>
											<ul class="dropdown-menu">
												<li class="nav-item">
													<a class="nav-link" href="/banh-cookies">Bánh Cookies</a>
												</li>
												<li class="nav-item">
													<a class="nav-link" href="/banh-lanh">Bánh Lạnh</a>
												</li>
												<li class="nav-item">
													<a class="nav-link" href="/banh-nuong">Bánh Nướng</a>
												</li>
												<li class="nav-item">
													<a class="nav-link" href="/banh-kem">Bánh Kem</a>
												</li>
											</ul>
										</li>
										<li class="nav-item active">
											<a class="nav-link" href="/cong-thuc">Công thức</a>
										</li>
										<li class="nav-item ">
											<a class="nav-link" href="/lien-he">Liên hệ</a>
										</li>
									</ul>
								</nav>
							</div>
						</aside>

						<div class="aside-item">
							<div>
								<div class="heading">
									<h2 class="title-head"><a href="#">Bài viết nổi bật</a></h2>
								</div>
								<div class="list-blogs">
									<div class="row">										
										<article class="col-md-12 col-sm-12 col-xs-12 clearfix">
											<div class="blog-item">
												<div class="blog-item-thumbnail">						
													<a href="/cach-lam-banh-matcha-delight">
														<picture>
															<source media="(min-width: 1200px)" srcset="//bizweb.dktcdn.net/thumb/medium/100/312/791/articles/blog4-min.jpg?v=1527699885557">
															<source media="(min-width: 992px)" srcset="//bizweb.dktcdn.net/thumb/medium/100/312/791/articles/blog4-min.jpg?v=1527699885557">
															<source media="(min-width: 767px)" srcset="//bizweb.dktcdn.net/thumb/grande/100/312/791/articles/blog4-min.jpg?v=1527699885557">
															<source media="(min-width: 666px)" srcset="//bizweb.dktcdn.net/thumb/grande/100/312/791/articles/blog4-min.jpg?v=1527699885557">
															<source media="(min-width: 569px)" srcset="//bizweb.dktcdn.net/thumb/medium/100/312/791/articles/blog4-min.jpg?v=1527699885557">
															<source media="(min-width: 480px)" srcset="//bizweb.dktcdn.net/thumb/grande/100/312/791/articles/blog4-min.jpg?v=1527699885557">
															<img src="//bizweb.dktcdn.net/thumb/large/100/312/791/articles/blog4-min.jpg?v=1527699885557" alt="Cách làm bánh Matcha Delight" class="img-responsive center-block">
														</picture>
													</a>						
												</div>
												<div class="blog-item-mains">
													<h3 class="blog-item-name">
														<a href="/cach-lam-banh-matcha-delight" title="Cách làm bánh Matcha Delight">Cách làm bánh Matcha Delight</a>
													</h3>	
												</div>
											</div>
										</article>
																							
										<article class="col-md-12 col-sm-12 col-xs-12 clearfix">
											<div class="blog-item">
												<div class="blog-item-thumbnail">						
													<a href="/cach-lam-mousse-chanh-leo">
														<picture>
															<source media="(min-width: 1200px)" srcset="//bizweb.dktcdn.net/thumb/medium/100/312/791/articles/blog3-min.jpg?v=1527699488943">
															<source media="(min-width: 992px)" srcset="//bizweb.dktcdn.net/thumb/medium/100/312/791/articles/blog3-min.jpg?v=1527699488943">
															<source media="(min-width: 767px)" srcset="//bizweb.dktcdn.net/thumb/grande/100/312/791/articles/blog3-min.jpg?v=1527699488943">
															<source media="(min-width: 666px)" srcset="//bizweb.dktcdn.net/thumb/grande/100/312/791/articles/blog3-min.jpg?v=1527699488943">
															<source media="(min-width: 569px)" srcset="//bizweb.dktcdn.net/thumb/medium/100/312/791/articles/blog3-min.jpg?v=1527699488943">
															<source media="(min-width: 480px)" srcset="//bizweb.dktcdn.net/thumb/grande/100/312/791/articles/blog3-min.jpg?v=1527699488943">
															<img src="//bizweb.dktcdn.net/thumb/large/100/312/791/articles/blog3-min.jpg?v=1527699488943" alt="Cách làm mousse chanh leo" class="img-responsive center-block">
														</picture>
													</a>						
												</div>
												<div class="blog-item-mains">
													<h3 class="blog-item-name">
														<a href="/cach-lam-mousse-chanh-leo" title="Cách làm mousse chanh leo">Cách làm mousse chanh leo</a>
													</h3>										
												</div>
											</div>
										</article>
																							
										<article class="col-md-12 col-sm-12 col-xs-12 clearfix">
											<div class="blog-item">
												<div class="blog-item-thumbnail">						
													<a href="/cach-lam-banh-black-forest-me-hoac">
														<picture>
															<source media="(min-width: 1200px)" srcset="//bizweb.dktcdn.net/thumb/medium/100/312/791/articles/blog2-min.jpg?v=1527698391267">
															<source media="(min-width: 992px)" srcset="//bizweb.dktcdn.net/thumb/medium/100/312/791/articles/blog2-min.jpg?v=1527698391267">
															<source media="(min-width: 767px)" srcset="//bizweb.dktcdn.net/thumb/grande/100/312/791/articles/blog2-min.jpg?v=1527698391267">
															<source media="(min-width: 666px)" srcset="//bizweb.dktcdn.net/thumb/grande/100/312/791/articles/blog2-min.jpg?v=1527698391267">
															<source media="(min-width: 569px)" srcset="//bizweb.dktcdn.net/thumb/medium/100/312/791/articles/blog2-min.jpg?v=1527698391267">
															<source media="(min-width: 480px)" srcset="//bizweb.dktcdn.net/thumb/grande/100/312/791/articles/blog2-min.jpg?v=1527698391267">
															<img src="//bizweb.dktcdn.net/thumb/large/100/312/791/articles/blog2-min.jpg?v=1527698391267" alt="Cách làm bánh Black Forest mê hoặc" class="img-responsive center-block">
														</picture>
													</a>						
												</div>
												<div class="blog-item-mains">
													<h3 class="blog-item-name">
														<a href="/cach-lam-banh-black-forest-me-hoac" title="Cách làm bánh Black Forest mê hoặc">Cách làm bánh Black Forest mê hoặc</a>
													</h3>									
												</div>
											</div>
										</article>
																							
										<article class="col-md-12 col-sm-12 col-xs-12 clearfix">
											<div class="blog-item">
												<div class="blog-item-thumbnail">						
													<a href="/cach-lam-banh-panna-cotta-ta-xanh-thom-mat">
														<picture>
															<source media="(min-width: 1200px)" srcset="//bizweb.dktcdn.net/thumb/medium/100/312/791/articles/blog1-min.jpg?v=1527697793133">
															<source media="(min-width: 992px)" srcset="//bizweb.dktcdn.net/thumb/medium/100/312/791/articles/blog1-min.jpg?v=1527697793133">
															<source media="(min-width: 767px)" srcset="//bizweb.dktcdn.net/thumb/grande/100/312/791/articles/blog1-min.jpg?v=1527697793133">
															<source media="(min-width: 666px)" srcset="//bizweb.dktcdn.net/thumb/grande/100/312/791/articles/blog1-min.jpg?v=1527697793133">
															<source media="(min-width: 569px)" srcset="//bizweb.dktcdn.net/thumb/medium/100/312/791/articles/blog1-min.jpg?v=1527697793133">
															<source media="(min-width: 480px)" srcset="//bizweb.dktcdn.net/thumb/grande/100/312/791/articles/blog1-min.jpg?v=1527697793133">
															<img src="//bizweb.dktcdn.net/thumb/large/100/312/791/articles/blog1-min.jpg?v=1527697793133" alt="Cách làm bánh Panna Cotta trà xanh thơm mát" class="img-responsive center-block">
														</picture>
													</a>						
												</div>
												<div class="blog-item-mains">
													<h3 class="blog-item-name">
														<a href="/cach-lam-banh-panna-cotta-ta-xanh-thom-mat" title="Cách làm bánh Panna Cotta trà xanh thơm mát">Cách làm bánh Panna Cotta trà xanh thơm mát</a>
													</h3>										
												</div>
											</div>
										</article>
									</div>
								</div>
							</div>
						</div>
					</div>
				</aside>
				<section class="right-content col-md-9 col-md-push-3 list-blog-page">	
					<div class="border-bg clearfix">
						<div class="col-md-12">
							<div class="box-heading d-none">
								<h1 class="title-head">Công thức</h1>
							</div>
							<section class="list-blogs blog-main">
								<div class="row">
									<article class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
										<div class="blog-item-frame">
											<div class="blog-item-thumbnail">	
												<a href="/cach-lam-banh-matcha-delight">
													<img class="img-responsive" src="//bizweb.dktcdn.net/thumb/grande/100/312/791/articles/blog4-min.jpg?v=1527699885557" data-lazyload="//bizweb.dktcdn.net/thumb/grande/100/312/791/articles/blog4-min.jpg?v=1527699885557" alt="Cách làm bánh Matcha Delight">	
												</a>
												<div class="post-time">
													31/05/2018
												</div>
											</div>
											<div class="blog-item-info">
												<h3 class="blog-item-name">
													<a href="recipesDetail.php" title="Cách làm bánh Matcha Delight">Cách làm bánh Matcha Delight</a>
												</h3>							
												<p class="blog-item-summary margin-bottom-5 line-clamp">   Tuy có cách làm khá cầu kỳ với nhiều lớp bánh khác nhau, nhưng thành quả thì lại rất “ngọt ngào” với vị matcha thanh ngọt cùng vị beo béo của kem tươi. Matcha Delight chắc chắn sẽ làm bạn “đổ” n...</p>
											</div>
										</div>
									</article>
										
									<article class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
										<div class="blog-item-frame">
											<div class="blog-item-thumbnail">
												<a href="/cach-lam-mousse-chanh-leo">	
													<img class="img-responsive" src="//bizweb.dktcdn.net/thumb/grande/100/312/791/articles/blog3-min.jpg?v=1527699488943" data-lazyload="//bizweb.dktcdn.net/thumb/grande/100/312/791/articles/blog3-min.jpg?v=1527699488943" alt="Cách làm mousse chanh leo">			
												</a>
												<div class="post-time">
													30/05/2018
												</div>
											</div>
											<div class="blog-item-info">
												<h3 class="blog-item-name"><a href="/cach-lam-mousse-chanh-leo" title="Cách làm mousse chanh leo">Cách làm mousse chanh leo</a></h3>							
												<p class="blog-item-summary margin-bottom-5 line-clamp">   Mousse chanh leo là món bánh tráng miệng khá phổ biến và rất được yêu thích hiện nay. Bánh thơm mềm với vị chua chua, ngọt ngọt dễ ăn hẳn sẽ làm bạn thích mê ngay từ lần thưởng thức đầu tiên luô...</p>
											</div>
										</div>
									</article>
										
									<article class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
										<div class="blog-item-frame">
											<div class="blog-item-thumbnail">	
												<a href="/cach-lam-banh-black-forest-me-hoac">		
													<img class="img-responsive" src="//bizweb.dktcdn.net/thumb/grande/100/312/791/articles/blog2-min.jpg?v=1527698391267" data-lazyload="//bizweb.dktcdn.net/thumb/grande/100/312/791/articles/blog2-min.jpg?v=1527698391267" alt="Cách làm bánh Black Forest mê hoặc">		
												</a>
												<div class="post-time">
													30/05/2018
												</div>
											</div>
											<div class="blog-item-info">
												<h3 class="blog-item-name"><a href="/cach-lam-banh-black-forest-me-hoac" title="Cách làm bánh Black Forest mê hoặc">Cách làm bánh Black Forest mê hoặc</a></h3>							
												<p class="blog-item-summary margin-bottom-5 line-clamp">   Là một món bánh truyền thống của Đức – Black Forest sẵn sàng làm bạn đổ ngay lập tức bởi cốt bánh xốp mềm kết hợp cùng chocolate đắng hảo hạng và cherry chua ngọt! Cùng Luz Bakery học cách làm b...</p>
											</div>
										</div>
									</article>
										
									<article class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
										<div class="blog-item-frame">
											<div class="blog-item-thumbnail">		
												<a href="/cach-lam-banh-panna-cotta-ta-xanh-thom-mat">
													<img class="img-responsive" src="//bizweb.dktcdn.net/thumb/grande/100/312/791/articles/blog1-min.jpg?v=1527697793133" data-lazyload="//bizweb.dktcdn.net/thumb/grande/100/312/791/articles/blog1-min.jpg?v=1527697793133" alt="Cách làm bánh Panna Cotta trà xanh thơm mát">	
												</a>
												<div class="post-time">
													30/05/2018
												</div>
											</div>
											<div class="blog-item-info">
												<h3 class="blog-item-name"><a href="/cach-lam-banh-panna-cotta-ta-xanh-thom-mat" title="Cách làm bánh Panna Cotta trà xanh thơm mát">Cách làm bánh Panna Cotta trà xanh thơm mát</a></h3>							
												<p class="blog-item-summary margin-bottom-5 line-clamp">   Làm bánh panna cotta trà xanh thơm mát tại nhà không hề khó. Với những ai còn chưa biết, panna cotta là món tráng miệng đặc trưng của nước Ý. “Panna cotta” trong tiếng Ý có nghĩa là kem nấu, đây...</p>
											</div>
										</div>
									</article>
									
									<div class="col-md-12 col-sm-12 col-xs-12">
										
									</div>
								</div>
							</section>
						</div>
					</div>
				</section>
			</div>
		</div>

    	<?php include('footer.php') ?>

	</body>

        <?php include('script.php') ?>

</html>