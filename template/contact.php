<!DOCTYPE html>
<html lang="vi">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <title>Cake</title>
        <meta name="description" content="">
        <meta name="keywords" content="Luz Bakery, cake">
        <meta name="revisit-after" content="1 days">
        <meta name="robots" content="noodp,index,follow">

        <?php include('style.php') ?>
        
    </head>

    <body>

    	<?php include('header.php') ?>

    	<div class="module-title">
			<div class="bg-frame"></div>
			<h2>
				<a href="javascript:void(0);">
					Liên hệ
				</a>
			</h2>
			<section class="bread-crumb margin-bottom-10">
				<div class="container">
					<div class="row">
						<div class="col-xs-12" style="width: 100%">
							<ul class="breadcrumb" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">					
								<li class="home">
									<a itemprop="url" href="/" title="Trang chủ"><span itemprop="title">Trang chủ</span></a>						
									<span><i class="fa fa-angle-right"></i></span>
								</li>
								
								<li>Liên hệ</li>
								
							</ul>
						</div>
					</div>
				</div>
			</section>
		</div>
		<div class="container contact">
			<div class="row">
				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
					<div class="sec-page-contact">
						<h1>Thông tin liên hệ</h1>
						<table>
							<tbody><tr>
								<td><i class="fa fa-home"></i></td>
								<td>Số 280 Cổ Nhuế- Bắc Từ Liêm - Hà Nội</td>
							</tr>
							<tr>
								<td><i class="fa fa-phone"></i></td>
								<td><a href="tel:0123456789">0973 605 319</a></td>
							</tr>
							<tr>
								<td><i class="fa fa-envelope"></i></td>
								<td><a href="mailto:khanhhoang22051996@gmail.com">khanhhoang22051996@gmail.com</a></td>
							</tr>
						</tbody></table>
					</div>
				</div>
				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
					<div class="sec-map-contact">
						<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d658.1715832048664!2d105.77940251399231!3d21.065443182666655!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3134552b772f8fc5%3A0xa0c61a0ef996f668!2zMjgwIEPhu5UgTmh14bq_LCBU4burIExpw6ptLCBIw6AgTuG7mWksIFZp4buHdCBOYW0!5e0!3m2!1svi!2s!4v1536169110938" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
					</div>
				</div>
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 margin-top-15">
					<div class="page-login">
						<div id="login">
							<h3 class="title-head text-center">Gửi thông tin</h3>
							<span class="text-center block"></span>
							<span class="text-center margin-bottom-10 block">Bạn hãy điền nội dung tin nhắn vào form dưới đây và gửi cho chúng tôi. Chúng tôi sẽ trả lời bạn sau khi nhận được.</span>
							<form accept-charset="UTF-8" action="" id="contact" method="post" class="has-validation-callback">
							<input name="FormType" type="hidden" value="contact">
							<input name="utf8" type="hidden" value="true">
							<div class="form-signup clearfix">
								<div class="row">
									<div class="col-md-4 col-sm-12 col-xs-12">
										<fieldset class="form-group has-error">
											<label>Họ tên<span class="required">*</span></label>
											<input type="text" name="contact[name]" id="name" class="form-control  form-control-lg error" data-validation-error-msg="Không được để trống" data-validation="required" required="" style="border-color: rgb(185, 74, 72);">
											<span class="help-block form-error">Không được để trống</span>
										</fieldset>
									</div>
									<div class="col-md-4 col-sm-12 col-xs-12">
										<fieldset class="form-group has-error">
											<label>Email<span class="required">*</span></label>
											<input type="email" name="contact[email]" data-validation="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,63}$" data-validation-error-msg="Email sai định dạng" id="email" class="form-control form-control-lg error" required="" style="border-color: rgb(185, 74, 72);">
										<span class="help-block form-error">Email sai định dạng</span></fieldset>
									</div>
									<div class="col-md-4 col-sm-12 col-xs-12">
										<fieldset class="form-group">
											<label>Điện thoại<span class="required">*</span></label>
											<input type="tel" name="contact[phone]" data-validation="tel" data-validation-error-msg="Không được để trống" id="tel" class="number-sidebar form-control form-control-lg" required="">
										</fieldset>
									</div>
									<div class="col-sm-12 col-xs-12">
										<fieldset class="form-group has-error">
											<label>Nội dung<span class="required">*</span></label>
											<textarea name="contact[body]" id="comment" class="form-control form-control-lg error" rows="5" data-validation-error-msg="Không được để trống" data-validation="required" required="" style="border-color: rgb(185, 74, 72);"></textarea>
										<span class="help-block form-error">Không được để trống</span></fieldset>
										<div class="a-center" style="margin-top:20px;">
											<button type="submit" class="btn btn-blues btn-style btn-style-active">Gửi tin nhắn</button>
										</div> 
									</div>
								</div>
							</div>
							</form>
						</div>
					</div>
				</div>		
			</div>
		</div>

    	<?php include('footer.php') ?>

	</body>

        <?php include('script.php') ?>

</html>