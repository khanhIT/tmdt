<section class="awe-section-5"> 
    <section class="section-selling margin-bottom-20">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 selling-content">             
                    <div class="heading a-center">
                        <h2 class="title-head">                 
                            <a href="#">Sản phẩm</a>                  
                        </h2>
                    </div>
                    <div>
                        <ul class="tabs tabs-title ajax clearfix hidden-xs">
                            <li class="tab-link has-content current" data-tab="tab-1" data-url="/san-pham-ban-chay">
                                <span>Bán chạy</span>
                            </li>
                            
                            <li class="tab-link has-content" data-tab="tab-2" data-url="/san-pham-noi-bat">
                                <span>Nổi bật</span>
                            </li>
                            
                            <li class="tab-link has-content" data-tab="tab-3" data-url="/san-pham-khuyen-mai">
                                <span>Khuyến mãi</span>
                            </li>
                        </ul>
                    </div>          
                    <div id="owl-demo" class="owl-carousel owl-theme tab-1 tab-content current">
                        <article class="thumbnail item" itemscope="" itemtype="http://schema.org/CreativeWork">
                            <div class="product-thumbnail-frame">
                                <div class="sale-flash">SALE</div>
                                <a class="blog-thumb-img" href="#" title="">
                                    <img src="images/product/sp1a-min.jpg" class="img-responsive" />
                                </a>
                            </div>
                            <div class="caption">
                                <h5 itemprop="headline" class="product-name">
                                    <a href="#" rel="bookmark">Tiramisu</a>
                                </h5>
                                <div class="price-box clearfix">            
                                    <div class="special-price">
                                        <span class="price product-price">Giá: 35.000₫</span>
                                    </div>
                                    
                                    <div class="old-price">                                
                                        <span class="price product-price-old">
                                            55.000₫         
                                        </span>
                                    </div>    
                                </div>
                                <div class="product-action clearfix">
                                    <form action="" method="post" class="variants form-nut-grid has-validation-callback" data-id="product-actions-11796111" enctype="multipart/form-data">
                                        <div>
                                            <input type="hidden" name="variantId" value="18765980">
                                            <button class="add2cart btn-buy btn-cart btn btn-gray left-to btn-primary add_to_cart" title="Thêm vào giỏ">
                                                <span>Thêm vào giỏ</span>
                                            </button>
                                            <a href="javascript:void(0)" data-handle="banh-kem-oreo" class="btn-white btn_view btn right-to quick-view">
                                                <i class="fa fa-search"></i>
                                            </a>               
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </article>

                        <article class="thumbnail item" itemscope="" itemtype="http://schema.org/CreativeWork">
                            <div class="product-thumbnail-frame">
                                <div class="sale-flash">SALE</div>
                                <a class="blog-thumb-img" href="#" title="">
                                    <img src="images/product/sp2a-min.jpg" class="img-responsive" />
                                </a>
                            </div>
                            <div class="caption">
                                <h5 itemprop="headline" class="product-name">
                                    <a href="#" rel="bookmark">Mousse Socola</a>
                                </h5>
                                <div class="price-box clearfix">            
                                    <div class="special-price">
                                        <span class="price product-price">Giá: 35.000₫</span>
                                    </div>
                                    
                                    <div class="old-price">                                  
                                        <span class="price product-price-old">
                                            55.000₫         
                                        </span>
                                    </div>          
                                </div>
                                <div class="product-action clearfix">
                                    <form action="" method="post" class="variants form-nut-grid has-validation-callback" data-id="product-actions-11796111" enctype="multipart/form-data">
                                        <div>
                                            <input type="hidden" name="variantId" value="18765980">
                                            <button class="add2cart btn-buy btn-cart btn btn-gray left-to btn-primary add_to_cart" title="Thêm vào giỏ">
                                                <span>Thêm vào giỏ</span>
                                            </button>
                                            <a href="javascript:void(0)" data-handle="banh-kem-oreo" class="btn-white btn_view btn right-to quick-view">
                                                <i class="fa fa-search"></i>
                                            </a>               
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </article>
                        
                        <article class="thumbnail item" itemscope="" itemtype="http://schema.org/CreativeWork">
                            <div class="product-thumbnail-frame">
                                <div class="sale-flash">SALE</div>
                                <a class="blog-thumb-img" href="#" title="">
                                    <img src="images/product/sp3-min.jpg" class="img-responsive" />
                                </a>
                            </div>
                            <div class="caption">
                                <h5 itemprop="headline" class="product-name">
                                    <a href="#" rel="bookmark">Cheesecake</a>
                                </h5>
                                <div class="price-box clearfix">            
                                    <div class="special-price">
                                        <span class="price product-price">Giá: 35.000₫</span>
                                    </div>
                                    
                                    <div class="old-price">                                                   
                                        <span class="price product-price-old">
                                            55.000₫         
                                        </span>
                                    </div>          
                                </div>
                                <div class="product-action clearfix">
                                    <form action="" method="post" class="variants form-nut-grid has-validation-callback" data-id="product-actions-11796111" enctype="multipart/form-data">
                                        <div>
                                            <input type="hidden" name="variantId" value="18765980">
                                            <button class="add2cart btn-buy btn-cart btn btn-gray left-to btn-primary add_to_cart" title="Thêm vào giỏ">
                                                <span>Thêm vào giỏ</span>
                                            </button>
                                            <a href="javascript:void(0)" data-handle="banh-kem-oreo" class="btn-white btn_view btn right-to quick-view">
                                                <i class="fa fa-search"></i>
                                            </a>               
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </article>
                        
                        <article class="thumbnail item" itemscope="" itemtype="http://schema.org/CreativeWork">
                            <div class="product-thumbnail-frame">
                                <div class="sale-flash">SALE</div>
                                <a class="blog-thumb-img" href="#" title="">
                                    <img src="images/product/sp5-min.jpg" class="img-responsive" />
                                </a>
                            </div>
                            <div class="caption">
                                <h5 itemprop="headline" class="product-name">
                                    <a href="#" rel="bookmark">Bánh Kem Bắp</a>
                                </h5>
                                <div class="price-box clearfix">            
                                    <div class="special-price">
                                        <span class="price product-price">Giá: 35.000₫</span>
                                    </div>
                                    
                                    <div class="old-price">                                                   
                                        <span class="price product-price-old">
                                            55.000₫         
                                        </span>
                                    </div>        
                                </div>
                                <div class="product-action clearfix">
                                    <form action="" method="post" class="variants form-nut-grid has-validation-callback" data-id="product-actions-11796111" enctype="multipart/form-data">
                                        <div>
                                            <input type="hidden" name="variantId" value="18765980">
                                            <button class="add2cart btn-buy btn-cart btn btn-gray left-to btn-primary add_to_cart" title="Thêm vào giỏ">
                                                <span>Thêm vào giỏ</span>
                                            </button>
                                            <a href="javascript:void(0)" data-handle="banh-kem-oreo" class="btn-white btn_view btn right-to quick-view">
                                                <i class="fa fa-search"></i>
                                            </a>               
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </article>
                        
                        <article class="thumbnail item" itemscope="" itemtype="http://schema.org/CreativeWork">
                            <div class="product-thumbnail-frame">
                                <div class="sale-flash">SALE</div>
                                <a class="blog-thumb-img" href="#" title="">
                                    <img src="images/product/sp7a-min.jpg" class="img-responsive" />
                                </a>
                            </div>
                            <div class="caption">
                                <h5 itemprop="headline" class="product-name">
                                    <a href="#" rel="bookmark">Bánh Kem Oreo</a>
                                </h5>
                                <div class="price-box clearfix">            
                                    <div class="special-price">
                                        <span class="price product-price">Giá: 35.000₫</span>
                                    </div>
                                    
                                    <div class="old-price">                                                   
                                        <span class="price product-price-old">
                                            55.000₫         
                                        </span>
                                    </div>        
                                </div>
                                <div class="product-action clearfix">
                                    <form action="" method="post" class="variants form-nut-grid has-validation-callback" data-id="product-actions-11796111" enctype="multipart/form-data">
                                        <div>
                                            <input type="hidden" name="variantId" value="18765980">
                                            <button class="add2cart btn-buy btn-cart btn btn-gray left-to btn-primary add_to_cart" title="Thêm vào giỏ">
                                                <span>Thêm vào giỏ</span>
                                            </button>
                                            <a href="javascript:void(0)" data-handle="banh-kem-oreo" class="btn-white btn_view btn right-to quick-view">
                                                <i class="fa fa-search"></i>
                                            </a>               
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </article>
                        
                        <article class="thumbnail item" itemscope="" itemtype="http://schema.org/CreativeWork">
                            <div class="product-thumbnail-frame">
                                <div class="sale-flash">SALE</div>
                                <a class="blog-thumb-img" href="#" title="">
                                    <img src="images/product/sp8-min.jpg" class="img-responsive" />
                                </a>
                            </div>
                            <div class="caption">
                                <h5 itemprop="headline" class="product-name">
                                    <a href="#" rel="bookmark">Bánh Kem Phô Mai Dâu</a>
                                </h5>
                                <div class="price-box clearfix">            
                                    <div class="special-price">
                                        <span class="price product-price">Giá: 35.000₫</span>
                                    </div>
                                    
                                    <div class="old-price">                                                          
                                        <span class="price product-price-old">
                                            55.000₫         
                                        </span>
                                    </div>       
                                </div>
                                <div class="product-action clearfix">
                                    <form action="" method="post" class="variants form-nut-grid has-validation-callback" data-id="product-actions-11796111" enctype="multipart/form-data">
                                        <div>
                                            <input type="hidden" name="variantId" value="18765980">
                                            <button class="add2cart btn-buy btn-cart btn btn-gray left-to btn-primary add_to_cart" title="Thêm vào giỏ">
                                                <span>Thêm vào giỏ</span>
                                            </button>
                                            <a href="javascript:void(0)" data-handle="banh-kem-oreo" class="btn-white btn_view btn right-to quick-view">
                                                <i class="fa fa-search"></i>
                                            </a>               
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </article>
                    </div><!-- tab-1 -->  
                    <div id="owl-demo" class="owl-carousel owl-theme tab-2 tab-content">
                        <article class="thumbnail item" itemscope="" itemtype="http://schema.org/CreativeWork">
                            <div class="product-thumbnail-frame">
                                <a class="blog-thumb-img" href="#" title="">
                                    <img src="images/product/sp1a-min.jpg" class="img-responsive" />
                                </a>
                            </div>
                            <div class="caption">
                                <h5 itemprop="headline" class="product-name">
                                    <a href="#" rel="bookmark">Tiramisu</a>
                                </h5>
                                <div class="price-box clearfix">            
                                    <div class="special-price">
                                        <span class="price product-price">Giá: 35.000₫</span>
                                    </div>
                                    
                                    <div class="old-price">                                
                                        <span class="price product-price-old">
                                            55.000₫         
                                        </span>
                                    </div>    
                                </div>
                                <div class="product-action clearfix">
                                    <form action="" method="post" class="variants form-nut-grid has-validation-callback" data-id="product-actions-11796111" enctype="multipart/form-data">
                                        <div>
                                            <input type="hidden" name="variantId" value="18765980">
                                            <button class="add2cart btn-buy btn-cart btn btn-gray left-to btn-primary add_to_cart" title="Thêm vào giỏ">
                                                <span>Thêm vào giỏ</span>
                                            </button>
                                            <a href="javascript:void(0)" data-handle="banh-kem-oreo" class="btn-white btn_view btn right-to quick-view">
                                                <i class="fa fa-search"></i>
                                            </a>               
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </article>

                        <article class="thumbnail item" itemscope="" itemtype="http://schema.org/CreativeWork">
                            <div class="product-thumbnail-frame">
                                <a class="blog-thumb-img" href="#" title="">
                                    <img src="images/product/sp2a-min.jpg" class="img-responsive" />
                                </a>
                            </div>
                            <div class="caption">
                                <h5 itemprop="headline" class="product-name">
                                    <a href="#" rel="bookmark">Mousse Socola</a>
                                </h5>
                                <div class="price-box clearfix">            
                                    <div class="special-price">
                                        <span class="price product-price">Giá: 35.000₫</span>
                                    </div>
                                    
                                    <div class="old-price">                                  
                                        <span class="price product-price-old">
                                            55.000₫         
                                        </span>
                                    </div>          
                                </div>
                                <div class="product-action clearfix">
                                    <form action="" method="post" class="variants form-nut-grid has-validation-callback" data-id="product-actions-11796111" enctype="multipart/form-data">
                                        <div>
                                            <input type="hidden" name="variantId" value="18765980">
                                            <button class="add2cart btn-buy btn-cart btn btn-gray left-to btn-primary add_to_cart" title="Thêm vào giỏ">
                                                <span>Thêm vào giỏ</span>
                                            </button>
                                            <a href="javascript:void(0)" data-handle="banh-kem-oreo" class="btn-white btn_view btn right-to quick-view">
                                                <i class="fa fa-search"></i>
                                            </a>               
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </article>
                        
                        <article class="thumbnail item" itemscope="" itemtype="http://schema.org/CreativeWork">
                            <div class="product-thumbnail-frame">
                                <a class="blog-thumb-img" href="#" title="">
                                    <img src="images/product/sp3-min.jpg" class="img-responsive" />
                                </a>
                            </div>
                            <div class="caption">
                                <h5 itemprop="headline" class="product-name">
                                    <a href="#" rel="bookmark">Cheesecake</a>
                                </h5>
                                <div class="price-box clearfix">            
                                    <div class="special-price">
                                        <span class="price product-price">Giá: 35.000₫</span>
                                    </div>
                                    
                                    <div class="old-price">                                                   
                                        <span class="price product-price-old">
                                            55.000₫         
                                        </span>
                                    </div>          
                                </div>
                                <div class="product-action clearfix">
                                    <form action="" method="post" class="variants form-nut-grid has-validation-callback" data-id="product-actions-11796111" enctype="multipart/form-data">
                                        <div>
                                            <input type="hidden" name="variantId" value="18765980">
                                            <button class="add2cart btn-buy btn-cart btn btn-gray left-to btn-primary add_to_cart" title="Thêm vào giỏ">
                                                <span>Thêm vào giỏ</span>
                                            </button>
                                            <a href="javascript:void(0)" data-handle="banh-kem-oreo" class="btn-white btn_view btn right-to quick-view">
                                                <i class="fa fa-search"></i>
                                            </a>               
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </article>
                        
                        <article class="thumbnail item" itemscope="" itemtype="http://schema.org/CreativeWork">
                            <div class="product-thumbnail-frame">
                                <a class="blog-thumb-img" href="#" title="">
                                    <img src="images/product/sp5-min.jpg" class="img-responsive" />
                                </a>
                            </div>
                            <div class="caption">
                                <h5 itemprop="headline" class="product-name">
                                    <a href="#" rel="bookmark">Bánh Kem Bắp</a>
                                </h5>
                                <div class="price-box clearfix">            
                                    <div class="special-price">
                                        <span class="price product-price">Giá: 35.000₫</span>
                                    </div>
                                    
                                    <div class="old-price">                                                   
                                        <span class="price product-price-old">
                                            55.000₫         
                                        </span>
                                    </div>        
                                </div>
                                <div class="product-action clearfix">
                                    <form action="" method="post" class="variants form-nut-grid has-validation-callback" data-id="product-actions-11796111" enctype="multipart/form-data">
                                        <div>
                                            <input type="hidden" name="variantId" value="18765980">
                                            <button class="add2cart btn-buy btn-cart btn btn-gray left-to btn-primary add_to_cart" title="Thêm vào giỏ">
                                                <span>Thêm vào giỏ</span>
                                            </button>
                                            <a href="javascript:void(0)" data-handle="banh-kem-oreo" class="btn-white btn_view btn right-to quick-view">
                                                <i class="fa fa-search"></i>
                                            </a>               
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </article>
                        
                        <article class="thumbnail item" itemscope="" itemtype="http://schema.org/CreativeWork">
                            <div class="product-thumbnail-frame">
                                <a class="blog-thumb-img" href="#" title="">
                                    <img src="images/product/sp7a-min.jpg" class="img-responsive" />
                                </a>
                            </div>
                            <div class="caption">
                                <h5 itemprop="headline" class="product-name">
                                    <a href="#" rel="bookmark">Bánh Kem Oreo</a>
                                </h5>
                                <div class="price-box clearfix">            
                                    <div class="special-price">
                                        <span class="price product-price">Giá: 35.000₫</span>
                                    </div>
                                    
                                    <div class="old-price">                                                   
                                        <span class="price product-price-old">
                                            55.000₫         
                                        </span>
                                    </div>        
                                </div>
                                <div class="product-action clearfix">
                                    <form action="" method="post" class="variants form-nut-grid has-validation-callback" data-id="product-actions-11796111" enctype="multipart/form-data">
                                        <div>
                                            <input type="hidden" name="variantId" value="18765980">
                                            <button class="add2cart btn-buy btn-cart btn btn-gray left-to btn-primary add_to_cart" title="Thêm vào giỏ">
                                                <span>Thêm vào giỏ</span>
                                            </button>
                                            <a href="javascript:void(0)" data-handle="banh-kem-oreo" class="btn-white btn_view btn right-to quick-view">
                                                <i class="fa fa-search"></i>
                                            </a>               
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </article>
                        
                        <article class="thumbnail item" itemscope="" itemtype="http://schema.org/CreativeWork">
                            <div class="product-thumbnail-frame">
                                <a class="blog-thumb-img" href="#" title="">
                                    <img src="images/product/sp8-min.jpg" class="img-responsive" />
                                </a>
                            </div>
                            <div class="caption">
                                <h5 itemprop="headline" class="product-name">
                                    <a href="#" rel="bookmark">Bánh Kem Phô Mai Dâu</a>
                                </h5>
                                <div class="price-box clearfix">            
                                    <div class="special-price">
                                        <span class="price product-price">Giá: 35.000₫</span>
                                    </div>
                                    
                                    <div class="old-price">                                                          
                                        <span class="price product-price-old">
                                            55.000₫         
                                        </span>
                                    </div>       
                                </div>
                                <div class="product-action clearfix">
                                    <form action="" method="post" class="variants form-nut-grid has-validation-callback" data-id="product-actions-11796111" enctype="multipart/form-data">
                                        <div>
                                            <input type="hidden" name="variantId" value="18765980">
                                            <button class="add2cart btn-buy btn-cart btn btn-gray left-to btn-primary add_to_cart" title="Thêm vào giỏ">
                                                <span>Thêm vào giỏ</span>
                                            </button>
                                            <a href="javascript:void(0)" data-handle="banh-kem-oreo" class="btn-white btn_view btn right-to quick-view">
                                                <i class="fa fa-search"></i>
                                            </a>               
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </article>
                    </div><!-- tab-2 --> 
                    <div id="owl-demo" class="owl-carousel owl-theme tab-3 tab-content">
                        <article class="thumbnail item" itemscope="" itemtype="http://schema.org/CreativeWork">
                            <div class="product-thumbnail-frame">
                                <a class="blog-thumb-img" href="#" title="">
                                    <img src="images/product/sp1a-min.jpg" class="img-responsive" />
                                </a>
                            </div>
                            <div class="caption">
                                <h5 itemprop="headline" class="product-name">
                                    <a href="#" rel="bookmark">Tiramisu</a>
                                </h5>
                                <div class="price-box clearfix">            
                                    <div class="special-price">
                                        <span class="price product-price">Giá: 35.000₫</span>
                                    </div>
                                    
                                    <div class="old-price">                                
                                        <span class="price product-price-old">
                                            55.000₫         
                                        </span>
                                    </div>    
                                </div>
                                <div class="product-action clearfix">
                                    <form action="" method="post" class="variants form-nut-grid has-validation-callback" data-id="product-actions-11796111" enctype="multipart/form-data">
                                        <div>
                                            <input type="hidden" name="variantId" value="18765980">
                                            <button class="add2cart btn-buy btn-cart btn btn-gray left-to btn-primary add_to_cart" title="Thêm vào giỏ">
                                                <span>Thêm vào giỏ</span>
                                            </button>
                                            <a href="javascript:void(0)" data-handle="banh-kem-oreo" class="btn-white btn_view btn right-to quick-view">
                                                <i class="fa fa-search"></i>
                                            </a>               
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </article>

                        <article class="thumbnail item" itemscope="" itemtype="http://schema.org/CreativeWork">
                            <div class="product-thumbnail-frame">
                                <a class="blog-thumb-img" href="#" title="">
                                    <img src="images/product/sp2a-min.jpg" class="img-responsive" />
                                </a>
                            </div>
                            <div class="caption">
                                <h5 itemprop="headline" class="product-name">
                                    <a href="#" rel="bookmark">Mousse Socola</a>
                                </h5>
                                <div class="price-box clearfix">            
                                    <div class="special-price">
                                        <span class="price product-price">Giá: 35.000₫</span>
                                    </div>
                                    
                                    <div class="old-price">                                  
                                        <span class="price product-price-old">
                                            55.000₫         
                                        </span>
                                    </div>          
                                </div>
                                <div class="product-action clearfix">
                                    <form action="" method="post" class="variants form-nut-grid has-validation-callback" data-id="product-actions-11796111" enctype="multipart/form-data">
                                        <div>
                                            <input type="hidden" name="variantId" value="18765980">
                                            <button class="add2cart btn-buy btn-cart btn btn-gray left-to btn-primary add_to_cart" title="Thêm vào giỏ">
                                                <span>Thêm vào giỏ</span>
                                            </button>
                                            <a href="javascript:void(0)" data-handle="banh-kem-oreo" class="btn-white btn_view btn right-to quick-view">
                                                <i class="fa fa-search"></i>
                                            </a>               
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </article>
                        
                        <article class="thumbnail item" itemscope="" itemtype="http://schema.org/CreativeWork">
                            <div class="product-thumbnail-frame">
                                <a class="blog-thumb-img" href="#" title="">
                                    <img src="images/product/sp3-min.jpg" class="img-responsive" />
                                </a>
                            </div>
                            <div class="caption">
                                <h5 itemprop="headline" class="product-name">
                                    <a href="#" rel="bookmark">Cheesecake</a>
                                </h5>
                                <div class="price-box clearfix">            
                                    <div class="special-price">
                                        <span class="price product-price">Giá: 35.000₫</span>
                                    </div>
                                    
                                    <div class="old-price">                                                   
                                        <span class="price product-price-old">
                                            55.000₫         
                                        </span>
                                    </div>          
                                </div>
                                <div class="product-action clearfix">
                                    <form action="" method="post" class="variants form-nut-grid has-validation-callback" data-id="product-actions-11796111" enctype="multipart/form-data">
                                        <div>
                                            <input type="hidden" name="variantId" value="18765980">
                                            <button class="add2cart btn-buy btn-cart btn btn-gray left-to btn-primary add_to_cart" title="Thêm vào giỏ">
                                                <span>Thêm vào giỏ</span>
                                            </button>
                                            <a href="javascript:void(0)" data-handle="banh-kem-oreo" class="btn-white btn_view btn right-to quick-view">
                                                <i class="fa fa-search"></i>
                                            </a>               
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </article>
                        
                        <article class="thumbnail item" itemscope="" itemtype="http://schema.org/CreativeWork">
                            <div class="product-thumbnail-frame">
                                <a class="blog-thumb-img" href="#" title="">
                                    <img src="images/product/sp5-min.jpg" class="img-responsive" />
                                </a>
                            </div>
                            <div class="caption">
                                <h5 itemprop="headline" class="product-name">
                                    <a href="#" rel="bookmark">Bánh Kem Bắp</a>
                                </h5>
                                <div class="price-box clearfix">            
                                    <div class="special-price">
                                        <span class="price product-price">Giá: 35.000₫</span>
                                    </div>
                                    
                                    <div class="old-price">                                                   
                                        <span class="price product-price-old">
                                            55.000₫         
                                        </span>
                                    </div>        
                                </div>
                                <div class="product-action clearfix">
                                    <form action="" method="post" class="variants form-nut-grid has-validation-callback" data-id="product-actions-11796111" enctype="multipart/form-data">
                                        <div>
                                            <input type="hidden" name="variantId" value="18765980">
                                            <button class="add2cart btn-buy btn-cart btn btn-gray left-to btn-primary add_to_cart" title="Thêm vào giỏ">
                                                <span>Thêm vào giỏ</span>
                                            </button>
                                            <a href="javascript:void(0)" data-handle="banh-kem-oreo" class="btn-white btn_view btn right-to quick-view">
                                                <i class="fa fa-search"></i>
                                            </a>               
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </article>
                        
                        <article class="thumbnail item" itemscope="" itemtype="http://schema.org/CreativeWork">
                            <div class="product-thumbnail-frame">
                                <a class="blog-thumb-img" href="#" title="">
                                    <img src="images/product/sp7a-min.jpg" class="img-responsive" />
                                </a>
                            </div>
                            <div class="caption">
                                <h5 itemprop="headline" class="product-name">
                                    <a href="#" rel="bookmark">Bánh Kem Oreo</a>
                                </h5>
                                <div class="price-box clearfix">            
                                    <div class="special-price">
                                        <span class="price product-price">Giá: 35.000₫</span>
                                    </div>
                                    
                                    <div class="old-price">                                                   
                                        <span class="price product-price-old">
                                            55.000₫         
                                        </span>
                                    </div>        
                                </div>
                                <div class="product-action clearfix">
                                    <form action="" method="post" class="variants form-nut-grid has-validation-callback" data-id="product-actions-11796111" enctype="multipart/form-data">
                                        <div>
                                            <input type="hidden" name="variantId" value="18765980">
                                            <button class="add2cart btn-buy btn-cart btn btn-gray left-to btn-primary add_to_cart" title="Thêm vào giỏ">
                                                <span>Thêm vào giỏ</span>
                                            </button>
                                            <a href="javascript:void(0)" data-handle="banh-kem-oreo" class="btn-white btn_view btn right-to quick-view">
                                                <i class="fa fa-search"></i>
                                            </a>               
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </article>
                        
                        <article class="thumbnail item" itemscope="" itemtype="http://schema.org/CreativeWork">
                            <div class="product-thumbnail-frame">
                                <a class="blog-thumb-img" href="#" title="">
                                    <img src="images/product/sp8-min.jpg" class="img-responsive" />
                                </a>
                            </div>
                            <div class="caption">
                                <h5 itemprop="headline" class="product-name">
                                    <a href="#" rel="bookmark">Bánh Kem Phô Mai Dâu</a>
                                </h5>
                                <div class="price-box clearfix">            
                                    <div class="special-price">
                                        <span class="price product-price">Giá: 35.000₫</span>
                                    </div>
                                    
                                    <div class="old-price">                                                          
                                        <span class="price product-price-old">
                                            55.000₫         
                                        </span>
                                    </div>       
                                </div>
                                <div class="product-action clearfix">
                                    <form action="" method="post" class="variants form-nut-grid has-validation-callback" data-id="product-actions-11796111" enctype="multipart/form-data">
                                        <div>
                                            <input type="hidden" name="variantId" value="18765980">
                                            <button class="add2cart btn-buy btn-cart btn btn-gray left-to btn-primary add_to_cart" title="Thêm vào giỏ">
                                                <span>Thêm vào giỏ</span>
                                            </button>
                                            <a href="javascript:void(0)" data-handle="banh-kem-oreo" class="btn-white btn_view btn right-to quick-view">
                                                <i class="fa fa-search"></i>
                                            </a>               
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </article>
                    </div><!-- tab-3 -->   
                </div>
            </div>
        </div>
    </section>
</section>

<!-- banner -->

<section class="awe-section-6"> 
    <section class="section-service margin-bottom-20">
        <div class="container">
            <div class="row section-service-frame">
                <div class="section-service-bg"></div>
                <div class="col-sm-4 col-xs-12">
                    <h3>Bảo đảm chất lượng</h3>
                    <p>Nguyên liệu được chọn lọc và xử lý trong quy trình khép kín, đảm bảo chất lượng và an toàn vệ sinh thực phẩm</p>
                </div>
                <div class="col-sm-4 col-xs-12">
                    <h3>Hương vị tự nhiên</h3>
                    <p>Chỉ sử dụng nguyên liệu tự nhiên, mang đến một sản phẩm thơm ngon mà không mất đi bản sắc tự nhiên vốn có</p>
                </div>
                <div class="col-sm-4 col-xs-12">
                    <h3>Công thức đặc biệt</h3>
                    <p>Sản phẩm được làm từ những công thức đặc biệt đã trải qua nhiều năm nghiên cứu và phát triển</p>
                </div>             
            </div>
        </div>
    </section>
</section>