<!DOCTYPE html>
<html lang="vi">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <title>Cake</title>
        <meta name="description" content="">
        <meta name="keywords" content="Luz Bakery, cake">
        <meta name="revisit-after" content="1 days">
        <meta name="robots" content="noodp,index,follow">

        <?php include('style.php') ?>
        
    </head>

    <body>

        <?php include('header.php') ?>

        <?php include('slide.php') ?>

        <?php include('productSelling.php') ?>

        <?php include('banner.php') ?>

        <?php include('product.php') ?>

        <?php include('recipe.php') ?>

        <?php include('banner.php') ?>

        <?php include('about.php') ?>

        <?php include('footer.php') ?>

    </body>

        <?php include('script.php') ?>

</html>