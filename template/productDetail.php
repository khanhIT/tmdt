<!DOCTYPE html>
<html lang="vi">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <title>Cake</title>
        <meta name="description" content="">
        <meta name="keywords" content="Luz Bakery, cake">
        <meta name="revisit-after" content="1 days">
        <meta name="robots" content="noodp,index,follow">

        <?php include('style.php') ?>
        
    </head>

    <body>

    	<?php include('header.php') ?>

    	<div class="module-title">
			<div class="bg-frame"></div>
			<h2>
				<a href="javascript:void(0);">
					Bánh Bông Lan Phô Mai Nhật
				</a>
			</h2>
			<section class="bread-crumb margin-bottom-10">
				<div class="container">
					<div class="row">
						<div class="col-xs-12" style="width: 100%">
							<ul class="breadcrumb" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">					
								<li class="home">
									<a itemprop="url" href="/" title="Trang chủ">
										<span itemprop="title">Trang chủ</span>
									</a>						
									<span><i class="fa fa-angle-right"></i></span>
								</li>
								<li class="home">
									<a itemprop="url" href="/" title="Tất cả sản phẩm">
										<span itemprop="title">Tất cả sản phẩm</span>
									</a>						
									<span><i class="fa fa-angle-right"></i></span>
								</li>
								<li>Bánh Bông Lan Phô Mai Nhật</li>
							</ul>
						</div>
					</div>
				</div>
			</section>
		</div>

		<section class="product" itemscope="" itemtype="http://schema.org/Product">	
			<meta itemprop="url" content="//luz-bakery.bizwebvietnam.net/banh-bong-lan-pho-mai-nhat">
			<meta itemprop="image" content="//bizweb.dktcdn.net/thumb/grande/100/312/791/products/sp9-min.jpg?v=1532245583477">
			<meta itemprop="description" content="  Nguyên liệu:  
			 130 ml sữa 
			 100 gram kem phô mai 
			 100 gram bơ 
			 8 lòng đỏ trứng 
			 60 gram bột mì 
			 60 gram bột bắp 
			 13 lòng trắng trứng 
			 130 gram đường cát 
			 Giấy nến 
			 Dâu tây 
			 Đường bột  
			   
			  Cách làm:   
			 1. Làm nóng lò nướng tới 160 độ C. 
			 2. Bắc một nồi nhỏ lên bếp, để lửa vừa. T...">
			<meta itemprop="name" content="Bánh Bông Lan Phô Mai Nhật">
			<div class="container">
				<div class="row">
					<div class="col-lg-12 details-product">
						<div class="product-essential">
							<div class="row">
								<div class="col-12 col-sm-12 col-md-9 col-lg-9" style="padding: 0">
									<div class="col-12 col-sm-6 col-md-5 col-lg-5">
										<div class="large-image">
											<img src="images/product/sp5-min.jpg" alt="">
										</div>
									</div>
									<div class="col-12 col-sm-6 col-md-7 col-lg-7 details-pro">
										<h1 class="title-head">Bánh Bông Lan Phô Mai Nhật</h1>	
										<div itemprop="offers" itemscope="" itemtype="http://schema.org/Offer">
											<div class="price-box clearfix">
												<span class="special-price">
													<span class="price product-price">33.000₫</span>
													<meta itemprop="price" content="33000">
													<meta itemprop="priceCurrency" content="VND">
												</span> <!-- Giá Khuyến mại -->
												<span class="old-price" itemprop="priceSpecification" itemscope="" itemtype="http://schema.org/priceSpecification" style="display: inline;">
													<del class="price product-price-old" style="display: inline;">60.000₫</del>
													<meta itemprop="price" content="60000">
													<meta itemprop="priceCurrency" content="VND">
												</span> <!-- Giá gốca -->
											</div>
										</div>		
										<div class="product-summary product_description margin-bottom-15">
											<div class="rte description">
												  Nguyên liệu:  							
												  130 ml sữa 
												 100 gram kem phô mai 
												 100 gram bơ 
												 8 lòng đỏ trứng 
												 60 gram bột mì 
												 60 gram bột bắp 
												 13 lòng trắng trứng...
											</div>
										</div>					
										<div class="form-product">
											<form enctype="multipart/form-data" id="add-to-cart-form" action="" method="post" class="form-inline has-validation-callback">
												<div class="select-swatch">
													<div class="form-group">
														<div class="custom custom-btn-number form-control">	
															<button onclick="var result = document.getElementById('qty'); var qty = result.value; if( !isNaN(qty) &amp; qty > 1 ) result.value--;return false;" class="btn-minus btn-cts" type="button">–</button>
															<input type="text" class="qty input-text" id="qty" name="quantity" size="4" value="1">
															<button onclick="var result = document.getElementById('qty'); var qty = result.value; if( !isNaN(qty)) result.value++;return false;" class="btn-plus btn-cts" type="button">+</button>
														</div>							
														<button type="submit" class="btn btn-lg btn-gray btn-cart add_to_cart btn_buy add_to_cart" title="Thêm vào giỏ">
															<span class="txt-main">Thêm vào giỏ</span>
														</button>							
													</div>	
												</div>
											</form>
										</div>
									</div>
									<div class="col-12 col-sm-12 col-md-12 col-lg-12 margin-top-15 margin-bottom-10">
										<!-- Nav tabs -->
										<div class="product-tab e-tabs">
											<ul class="tabs tabs-title clearfix">	
												<li class="tab-link current">
													<h3>
														<span>Mô tả</span>
													</h3>
												</li>
											</ul>		
											<div id="tab-1" class="tab-content current">
												<div class="product-well">
													<div class="ba-text-fpt" id="fancy-image-view">
														<p><strong>Nguyên liệu:</strong></p>
														<ul>
															<li>130 ml sữa</li>
															<li>100 gram kem phô mai</li>
															<li>100 gram bơ</li>
															<li>8 lòng đỏ trứng</li>
															<li>60 gram bột mì</li>
															<li>60 gram bột bắp</li>
															<li>13 lòng trắng trứng</li>
															<li>130 gram đường cát</li>
															<li>Giấy nến</li>
															<li>Dâu tây</li>
															<li>Đường bột</li>
														</ul>
														<p>
															<img alt="Bánh Bông Lan Phô Mai Nhật" data-thumb="original" original-height="342" original-width="640" src="//bizweb.dktcdn.net/100/312/791/files/diadiemanuong-com-cach-lam-banh-bong-lan-pho-mai-nhat-ban-ngon-me-man12708c1a635767200186974397-min.jpg?v=1527443071340">
														</p>
														<p>
															<strong>Cách làm:</strong>&nbsp;
														</p>
														<p>1. Làm nóng lò nướng tới 160 độ C.</p>
														<p>2. Bắc một nồi nhỏ lên bếp, để lửa vừa. Trộn sữa, kem phô mai và bơ đến khi hỗn hợp mịn, sau đó bắc xuống và làm mát.</p>
														<p>3. Đánh đều lòng đỏ trứng trong tô lớn đến khi mịn, rồi từ từ đổ hỗn hợp kem vào, khuấy thật đều.</p>
														<p>4. Rây bột và bột bắp vào tô, khuấy nhanh để bột không vón cục.</p>
														<p>5. Lấy một tô lớn khác để đánh lòng trắng trứng bằng máy trộn, đến khi bề mặt bông lên.</p>
														<p>6. Từ từ thêm đường trong lúc tiếp tục đánh trứng để hỗn hợp đặc lại.</p>
														<p>7. Lấy ra khoảng 1/4 hỗn hợp lòng trắng trứng, trộn đều với hỗn hợp lòng đỏ trứng. Thực hiện tương tự với số lòng trắng trứng còn lại.</p>
														<p>8. Đặt một dải giấy nến dài khoảng 10 cm quanh viền chảo bánh kích thước khoảng 22,8 x 7,6 cm. Đáy chảo cũng phải lót giấy nến trước đó. Nếu dùng khuôn springform, hãy nhớ bọc đáy và cạnh khuôn bằng hai lớp giấy bạc để bột không chảy ra ngoài.</p>
														<p>9. Đổ hỗn hợp bột vào chảo đã lót giấy nến và lắc nhẹ.</p>
														<p>10. Đặt chảo bánh vào một chảo nướng hoặc khay lớn hơn, đặt sẵn hai tờ khăn giấy dưới đáy. Khăn giấy giúp phân bố đều lượng nhiệt dưới đáy chảo. Đổ khoảng 2,5 cm nước nóng vào chảo lớn hơn.</p>
														<p>11. Nướng trong vòng 25 phút, sau đó giảm nhiệt độ xuống 135 độ C và nướng thêm 55 phút, đến khi bánh nở cao gấp đôi.</p>
														<p>12. Lấy bánh ra khỏi lò nướng, cẩn thận lật ngược bánh lại trên tay và bóc giấy nến. Cần đặc biệt cẩn thận vì bánh sẽ rất nóng. Bạn cũng có thể lật ngược bánh trên đĩa, nhưng điều này sẽ khiến bánh bị xẹp hơn.</p>
														<p>13. Rắc đường bột lên trên bánh, cắt miếng và ăn kèm với dâu tây khi bánh còn nóng.</p>
													</div>
													<div class="show-more">
														<a class="btn btn-default btn--view-more">
															<span class="more-text">Xem thêm <i class="fa fa-chevron-down"></i>
															</span>
														</a>
													</div>
												</div>
											</div>	
										</div>
									</div>
								</div>
								<div class="sidebar sidebar-aside col--12 col-sm-12 col-md-3 col-lg-3">
									<aside class="aside-item clearfix">
										<div class="right_module margin-top-10">
											<div class="module_service_details">
												<div class="wrap_module_service">
													<div class="item_service">
														<div class="wrap_item_">
															<div class="content_service">
																<p>Giao hàng tận nơi</p>
																<span>Miễn phí vận chuyển với đơn hàng trên 300.000 đ</span>
															</div>
														</div>
													</div>										
													<div class="item_service">
														<div class="wrap_item_">
															<div class="content_service">
																<p>Thanh toán khi nhận</p>
																<span>Bạn thoải mái nhận &amp; kiểm tra hàng trước khi trả tiền.</span>
															</div>
														</div>
													</div>
													<div class="item_service">
														<div class="wrap_item_">
															<div class="content_service">
																<p>Bảo đảm chất lượng</p>
																<span>Chất lượng sản phẩm luôn được đặt lên hàng đầu</span>
															</div>
														</div>
													</div>	
												</div>
											</div>
										</div>
									</aside>
									<aside class="aside-item collection-category">
										<div class="aside-title">
											<h3 class="title-head">
												<span>Danh mục sản phẩm</span>
											</h3>
										</div>
										<div class="aside-content">
											<nav class="nav-category navbar-toggleable-md">
												<ul class="nav navbar-pills">
													<li class="nav-item ">
														<a class="nav-link" href="/banh-cookies">Bánh Cookies</a>
													</li>
													<li class="nav-item ">
														<a class="nav-link" href="/banh-lanh">Bánh Lạnh</a>
													</li>
													<li class="nav-item ">
														<a class="nav-link" href="/banh-nuong">Bánh Nướng</a>
													</li>
													<li class="nav-item ">
														<a class="nav-link" href="/banh-kem">Bánh Kem</a>
													</li>
												</ul>
											</nav>
										</div>
									</aside>
									<div class="pro-hotline-frame">
										<h3>Hotline:</h3>&nbsp;<a href="tel:0973 605 319">0973 605 319</a>
										<p>Liên hệ ngay cho chúng tôi để thưởng thức những chiếc bánh thơm ngon nhất</p>								
									</div>
									<div class="pro-banner-frame d-none d-sm-none d-md-block d-lg-block">
										<a href="#">
											<img src="//bizweb.dktcdn.net/100/312/791/themes/667098/assets/aside_bannerpro.png?1535770651792" alt="Banner">
										</a>							
									</div>
								</div>
							</div>
						</div>
						<?php include('productSelling.php') ?>
					</div>
				</div>
			</div>
		</section>

    	<?php include('footer.php') ?>

	</body>

        <?php include('script.php') ?>

</html>