<!DOCTYPE html>
<html lang="vi">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <title>Cake</title>
        <meta name="description" content="">
        <meta name="keywords" content="Luz Bakery, cake">
        <meta name="revisit-after" content="1 days">
        <meta name="robots" content="noodp,index,follow">

        <?php include('style.php') ?>
        
    </head>

    <body>

    	<?php include('header.php') ?>

    	<div class="module-title">
			<div class="bg-frame"></div>
			<h2>
				<a href="javascript:void(0);">
					Giỏ Hàng
				</a>
			</h2>
			<section class="bread-crumb margin-bottom-10">
				<div class="container">
					<div class="row">
						<div class="col-xs-12" style="width: 100%">
							<ul class="breadcrumb" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">					
								<li class="home">
									<a itemprop="url" href="/" title="Trang chủ"><span itemprop="title">Trang chủ</span></a>						
									<span><i class="fa fa-angle-right"></i></span>
								</li>
								
								<li>Giỏ Hàng</li>
								
							</ul>
						</div>
					</div>
				</div>
			</section>
		</div>
		<div id="luz-cart" class="container white collections-container margin-bottom-20">
			<div class="white-background">
				<div class="row">
					<div class="col-md-12">
						<div class="shopping-cart">
							<div class="d-sm-none d-none d-md-block d-lg-block">
								<div class="shopping-cart-table">
									<div class="row">
										<div class="col-md-12">
											<h1 class="lbl-shopping-cart lbl-shopping-cart-gio-hang">Giỏ hàng <span>(<span class="count_item_pr">0</span> sản phẩm)</span></h1>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6 offset-md-3">
											<div class="cart-empty">
												<img src="images/empty_cart.png" alt="Giỏ hàng trống">
												<div class="btn-cart-empty">
													<a class="btn btn-default" href="/" title="Tiếp tục mua sắm">Tiếp tục mua sắm</a>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-main cart_desktop_page cart-page">
											<form id="shopping-cart" action="/cart" method="post" novalidate="" class="has-validation-callback">
												<div class="cart page_cart cart_des_page hidden-xs-down">    <div class="col-9">
														<div class="cart-tbody">
															<div class="row shopping-cart-item productid-18765980">
																<div class="col-3 img-thumnail-custom">
																	<p class="image">
																		<img class="img-responsive" src="//bizweb.dktcdn.net/thumb/medium/100/312/791/products/sp7a-min.jpg" alt="Bánh Kem Oreo">
																	</p>
																</div>
																<div class="col-right col-9">
																	<div class="box-info-product">
																		<p class="name">
																			<a href="/banh-kem-oreo" target="_blank">Bánh Kem Oreo</a>
																		</p>
																		<p class="action">
																			<a href="javascript:;" class="btn btn-link btn-item-delete remove-item-cart" data-id="18765980">Xóa</a>
																		</p>
																	</div>
																	<div class="box-price">
																		<p class="price">32.000₫</p>
																	</div>
																	<div class="quantity-block">
																		<div class="input-group bootstrap-touchspin">
																			<div class="input-group-btn">
																				<input class="variantID" type="hidden" name="variantId" value="18765980">
																				<button class="increase_pop items-count btn-plus btn btn-default bootstrap-touchspin-up" type="button">+</button>
																				<input type="text" maxlength="12" min="1" class="form-control quantity-r2 quantity js-quantity-product input-text number-sidebar input_pop input_pop qtyItem18765980" id="qtyItem18765980" name="Lines" size="4" value="1">
																				<button disabled="" class="reduced_pop items-count btn-minus btn btn-default bootstrap-touchspin-down" type="button">–</button>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div class="col-3 cart-collaterals cart_submit">
														<div id="right-affix">
															<div class="each-row">
																<div class="box-style fee">
																	<p class="list-info-price">
																		<span>Tạm tính: </span>
																		<strong class="totals_price price _text-right text_color_right1">32.000₫</strong>
																	</p>
																</div>
																<div class="box-style fee">
																	<div class="total2 clearfix">
																		<span class="text-label">Thành tiền: </span>
																		<div class="amount">
																			<p>
																				<strong class="totals_price">32.000₫</strong>
																			</p>
																		</div>
																	</div>
																</div>
																<button class="button btn-proceed-checkout btn btn-large btn-block btn-danger btn-checkout" title="Thanh toán ngay" type="button" onclick="window.location.href='checkout.php'">Thanh toán ngay</button>
																<button class="button btn-proceed-checkout btn btn-large btn-block btn-danger btn-checkouts" title="Tiếp tục mua hàng" type="button" onclick="window.location.href='/collections/all'">Tiếp tục mua hàng
																</button>
															</div>
														</div>
													</div>
												</div>
											</form>
										</div>
									</div>
								</div>
							</div>
							<div class="d-md-none d-lg-none">
								<div class="cart-empty">
									<img src="images/empty_cart.png" class="img-responsive center-block" alt="Giỏ hàng trống">
									<div class="btn-cart-empty">
										<a class="btn btn-default" href="/" title="Tiếp tục mua sắm">Tiếp tục mua hàng</a>
									</div>
								</div>
								<div class="cart-mobile">
									<form action="/cart" method="post" class="margin-bottom-0 has-validation-callback">
										<div class="header-cart">
											<div class="title-cart">
												<h3>Giỏ hàng của bạn</h3>
											</div>
										</div>
										<div class="header-cart-content">
											<div class="cart_page_mobile content-product-list">
												<div class="item-product item productid-18765980 ">
													<div class="item-product-cart-mobile">
														<a class="product-images1" href="" title="Bánh Kem Oreo">
															<img width="80" height="150" alt="" src="images/product/sp7a-min.jpg">
														</a>
													</div>
													<div class="title-product-cart-mobile">
														<h3>
															<a href="/banh-kem-oreo" title="Bánh Kem Oreo">Bánh Kem Oreo</a>
														</h3>
														<p>Giá: <span>32.000₫</span></p>
													</div>
													<div class="select-item-qty-mobile">
														<div class="txt_center">
															<input class="variantID" type="hidden" name="variantId" value="18765980">
															<button class="reduced items-count btn-minus" type="button">–
															</button>
															<input type="text" maxlength="12" min="0" class="input-text number-sidebar qtyMobile18765980" id="qtyMobile18765980" name="Lines" size="4" value="1">
															<button class="increase items-count btn-plus" type="button">+</button>
														</div>
														<a class="button remove-item remove-item-cart" href="javascript:;" data-id="18765980">Xoá</a>
													</div>
												</div>
											</div>
											<div class="header-cart-price" style="">
												<div class="title-cart ">
													<h3 class="text-xs-left">Tổng tiền</h3>
													<a class="text-xs-right totals_price_mobile">32.000₫</a>
												</div>
												<div class="checkout">
													<button class="btn-proceed-checkout-mobile" title="Thanh toán ngay" type="button" onclick="window.location.href='checkout.php'">
														<span>Thanh toán ngay</span>
													</button>
												</div>
												<button class="btn btn-proceed-continues-mobile" title="Tiếp tục mua hàng" type="button" onclick="window.location.href='/collections/all'">Tiếp tục mua hàng</button></div>
											</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

    	<?php include('footer.php') ?>

	</body>

        <?php include('script.php') ?>

</html>