<section class="awe-section-1">
    <section class="section-slide">
        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img class="d-block w-100" src="images/slide/slider_1.png" alt="First slide">
                    <div class="slide-info d-none d-md-block d-lg-block">
                        <h4 class="title-slide" data-in-effect="rollIn" data-out-effect="fadeOut">Luz Bakery</h4>
                        <p data-animation-in="rollIn" data-animation-out="animate-out rollOut">Hương vị đặc trưng đến từ Pháp</p>
                        <p>
                            <a href="#" class="btn" data-animation-in="fadeInLeft" data-animation-out="animate-out fadeOutRight">Xem thêm</a>
                        </p>
                    </div>
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100" src="images/slide/slider_1.png" alt="Second slide">
                    <div class="slide-info d-none d-md-block d-lg-block">
                        <h4 class="title-slide" data-in-effect="rollIn" data-out-effect="fadeOut">Luz Bakery</h4>
                        <p data-animation-in="rollIn" data-animation-out="animate-out rollOut">Hương vị đặc trưng đến từ Pháp</p>
                        <p>
                            <a href="#" class="btn" data-animation-in="fadeInLeft" data-animation-out="animate-out fadeOutRight">Xem thêm</a>
                        </p>
                    </div>
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100" src="images/slide/slider_1.png" alt="Third slide">
                    <div class="slide-info d-none d-md-block d-lg-block">
                        <h4 class="title-slide" data-in-effect="rollIn" data-out-effect="fadeOut">Luz Bakery</h4>
                        <p data-animation-in="rollIn" data-animation-out="animate-out rollOut">Hương vị đặc trưng đến từ Pháp</p>
                        <p>
                            <a href="#" class="btn" data-animation-in="fadeInLeft" data-animation-out="animate-out fadeOutRight">Xem thêm</a>
                        </p>
                    </div>
                </div>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </section>
</section>

<!-- contact -->
<section class="awe-section-2"> 
    <section class="section-contact">
        <div class="container">
            <ul>
                <li class="sec-ct-hotline">
                    <a href="tel:0123456789">
                        <i class="fa fa-phone"></i> 0973 605 319
                    </a>         
                </li>
                <li class="sec-ct-email">
                    <a href="mailto:luz.elioteam@gmail.com">
                        <i class="fa fa-envelope"></i> khanhhoang220596@gmail.com
                    </a> 
                </li>
                <li>
                    <a href="#" target="_blank">
                        <i class="fa fa-facebook"></i>
                    </a>  
                </li>
                <li>
                    <a href="#" target="_blank">
                        <i class="fa fa-instagram"></i>
                    </a> 
                </li>
                <li>
                    <a href="#" target="_blank">
                        <i class="fa fa-google"></i>
                    </a>    
                </li>
            </ul>
        </div>
    </section>
</section>