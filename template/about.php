<section class="awe-section-9">	
	<section class="section-about margin-bottom-20">
		<div class="container">
			<div class="row">
				<div class="col-lg-6 col-md-12">
					<div class="section-about-frame">
						<h3>Luz Bakery</h3>
						<a href="/">						
							<img src="images/sec_about_page_logo.png" data-lazyload="images/sec_about_page_logo.png" alt="Luz Bakery">
						</a>
						<div class="about-content">
							
							<p>Đắm mình trong không gian ồn ào nhộn nhịp của thành phố mang tên Bác, bạn không những được tận hưởng cảm xúc thi vị của Sài Gòn, mà còn được thưởng thức hương vị Pháp ngay trong lòng Sài Gòn.</p>
							<p>Nằm trên con phố đông đúc và cổ kính, Luz Bakery từ lâu đã trở thành điểm đến của những người yêu thích bánh ngọt Pháp. Với niềm đam mê về chất Pháp, Luz Bakery đã mang hương vị pháp “nguyên chất” đến với những thực khách Việt Nam.</p>
							<p>Luz Bakery là một thương hiệu nổi tiếng cho những ai yêu thích văn hóa ẩm thực Pháp. Nằm trên cái “chất” của Sài Gòn, chất của người tìm về cội nguồn văn hóa, du khách sẽ bắt gặp Luz Bakery tại Số 70 Lữ Gia, Quận 10. Vừa tìm hiểu về văn hóa Sài Gòn, du khách có thể thưởng thức hương vị Pháp tại đây, với một không gian rất nhộn nhịp của Sài Thành.</p>
							<p>Luz Bakery được sản xuất trên dây chuyền hiện đại, với những nguyên liệu được nhập khẩu trực tiếp từ các nước có truyền thống làm bánh lâu đời trên thế giới. Thực khách tới đây có thể thưởng thức rất nhiều loại bánh : Bánh Sinh Nhật, Bánh Cưới, Bánh Valentine, Bánh Giáng sinh… Barkery, Bánh mỳ Pháp, Pizza, Hotdog, Patechaux, Cookies, và cả Bánh Trung thu ….</p>
							<p>Tất cả những yếu tố đó đã tạo lên một thương hiệu Luz Bakery.</p>
							
						</div>
					</div>
				</div>
				<div class="col-lg-6 col-md-12">
					<a href="">					
						<img class="section-about-img" src="images/sec_about_page.png" data-lazyload="images/sec_about_page.png" alt="Luz Bakery">
					</a>
				</div>
			</div>
		</div>
	</section>
</section>