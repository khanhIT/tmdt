<!DOCTYPE html>
<html lang="vi">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <title>Cake</title>
        <meta name="description" content="">
        <meta name="keywords" content="Luz Bakery, cake">
        <meta name="revisit-after" content="1 days">
        <meta name="robots" content="noodp,index,follow">

        <?php include('style.php') ?>
        
    </head>

    <body>

    	<?php include('header.php') ?>

    	<div class="module-title">
			<div class="bg-frame"></div>
			<h2>
				<a href="javascript:void(0);">
					Cách Làm Bánh Matcha Delight
				</a>
			</h2>
			<section class="bread-crumb margin-bottom-10">
				<div class="container">
					<div class="row">
						<div class="col-xs-12" style="width: 100%">
							<ul class="breadcrumb" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">					
								<li class="home">
									<a itemprop="url" href="/" title="Trang chủ"><span itemprop="title">Trang chủ</span></a>						
									<span><i class="fa fa-angle-right"></i></span>
								</li>
								<li class="home">
									<a itemprop="url" href="/" title="Trang chủ">
										<span itemprop="title">Công thức</span>
									</a>						
									<span><i class="fa fa-angle-right"></i></span>
								</li>
								<li>Cách Làm Bánh Matcha Delight</li>
							</ul>
						</div>
					</div>
				</div>
			</section>
		</div>
		<div class="luz-blog container" itemscope="" itemtype="http://schema.org/Blog">
			<meta itemprop="name" content="Công thức"> 
			<meta itemprop="description" content="Chủ đề không có mô tả"> 
			<div class="row">
				<aside class="sidebar left left-content col-md-3 col-md-pull-9">
					<div class="border-bg">
						<aside class="aside-item collection-category blog-category">	
							<div class="heading">
								<h2 class="title-head">
									<span>Danh mục</span>
								</h2>
							</div>	
							<div class="aside-content">
								<nav class="nav-category  navbar-toggleable-md">
									<ul class="nav navbar-pills">
										<li class="nav-item ">
											<a class="nav-link" href="/">Trang chủ</a>
										</li>
										<li class="nav-item ">
											<a class="nav-link" href="/gioi-thieu">Giới thiệu</a>
										</li>
										<li class="nav-item ">
											<a href="/collections/all" class="nav-link">Sản phẩm</a>
											<i class="fa fa-angle-down"></i>
											<ul class="dropdown-menu">
												<li class="nav-item">
													<a class="nav-link" href="/banh-cookies">Bánh Cookies</a>
												</li>
												<li class="nav-item">
													<a class="nav-link" href="/banh-lanh">Bánh Lạnh</a>
												</li>
												<li class="nav-item">
													<a class="nav-link" href="/banh-nuong">Bánh Nướng</a>
												</li>
												<li class="nav-item">
													<a class="nav-link" href="/banh-kem">Bánh Kem</a>
												</li>
											</ul>
										</li>
										<li class="nav-item active">
											<a class="nav-link" href="/cong-thuc">Công thức</a>
										</li>
										<li class="nav-item ">
											<a class="nav-link" href="/lien-he">Liên hệ</a>
										</li>
									</ul>
								</nav>
							</div>
						</aside>

						<div class="aside-item">
							<div>
								<div class="heading">
									<h2 class="title-head"><a href="#">Bài viết nổi bật</a></h2>
								</div>
								<div class="list-blogs">
									<div class="row">										
										<article class="col-md-12 col-sm-12 col-xs-12 clearfix">
											<div class="blog-item">
												<div class="blog-item-thumbnail">						
													<a href="/cach-lam-banh-matcha-delight">
														<picture>
															<source media="(min-width: 1200px)" srcset="//bizweb.dktcdn.net/thumb/medium/100/312/791/articles/blog4-min.jpg?v=1527699885557">
															<source media="(min-width: 992px)" srcset="//bizweb.dktcdn.net/thumb/medium/100/312/791/articles/blog4-min.jpg?v=1527699885557">
															<source media="(min-width: 767px)" srcset="//bizweb.dktcdn.net/thumb/grande/100/312/791/articles/blog4-min.jpg?v=1527699885557">
															<source media="(min-width: 666px)" srcset="//bizweb.dktcdn.net/thumb/grande/100/312/791/articles/blog4-min.jpg?v=1527699885557">
															<source media="(min-width: 569px)" srcset="//bizweb.dktcdn.net/thumb/medium/100/312/791/articles/blog4-min.jpg?v=1527699885557">
															<source media="(min-width: 480px)" srcset="//bizweb.dktcdn.net/thumb/grande/100/312/791/articles/blog4-min.jpg?v=1527699885557">
															<img src="//bizweb.dktcdn.net/thumb/large/100/312/791/articles/blog4-min.jpg?v=1527699885557" alt="Cách làm bánh Matcha Delight" class="img-responsive center-block">
														</picture>
													</a>						
												</div>
												<div class="blog-item-mains">
													<h3 class="blog-item-name">
														<a href="/cach-lam-banh-matcha-delight" title="Cách làm bánh Matcha Delight">Cách làm bánh Matcha Delight</a>
													</h3>	
												</div>
											</div>
										</article>
																							
										<article class="col-md-12 col-sm-12 col-xs-12 clearfix">
											<div class="blog-item">
												<div class="blog-item-thumbnail">						
													<a href="/cach-lam-mousse-chanh-leo">
														<picture>
															<source media="(min-width: 1200px)" srcset="//bizweb.dktcdn.net/thumb/medium/100/312/791/articles/blog3-min.jpg?v=1527699488943">
															<source media="(min-width: 992px)" srcset="//bizweb.dktcdn.net/thumb/medium/100/312/791/articles/blog3-min.jpg?v=1527699488943">
															<source media="(min-width: 767px)" srcset="//bizweb.dktcdn.net/thumb/grande/100/312/791/articles/blog3-min.jpg?v=1527699488943">
															<source media="(min-width: 666px)" srcset="//bizweb.dktcdn.net/thumb/grande/100/312/791/articles/blog3-min.jpg?v=1527699488943">
															<source media="(min-width: 569px)" srcset="//bizweb.dktcdn.net/thumb/medium/100/312/791/articles/blog3-min.jpg?v=1527699488943">
															<source media="(min-width: 480px)" srcset="//bizweb.dktcdn.net/thumb/grande/100/312/791/articles/blog3-min.jpg?v=1527699488943">
															<img src="//bizweb.dktcdn.net/thumb/large/100/312/791/articles/blog3-min.jpg?v=1527699488943" alt="Cách làm mousse chanh leo" class="img-responsive center-block">
														</picture>
													</a>						
												</div>
												<div class="blog-item-mains">
													<h3 class="blog-item-name">
														<a href="/cach-lam-mousse-chanh-leo" title="Cách làm mousse chanh leo">Cách làm mousse chanh leo</a>
													</h3>										
												</div>
											</div>
										</article>
																							
										<article class="col-md-12 col-sm-12 col-xs-12 clearfix">
											<div class="blog-item">
												<div class="blog-item-thumbnail">						
													<a href="/cach-lam-banh-black-forest-me-hoac">
														<picture>
															<source media="(min-width: 1200px)" srcset="//bizweb.dktcdn.net/thumb/medium/100/312/791/articles/blog2-min.jpg?v=1527698391267">
															<source media="(min-width: 992px)" srcset="//bizweb.dktcdn.net/thumb/medium/100/312/791/articles/blog2-min.jpg?v=1527698391267">
															<source media="(min-width: 767px)" srcset="//bizweb.dktcdn.net/thumb/grande/100/312/791/articles/blog2-min.jpg?v=1527698391267">
															<source media="(min-width: 666px)" srcset="//bizweb.dktcdn.net/thumb/grande/100/312/791/articles/blog2-min.jpg?v=1527698391267">
															<source media="(min-width: 569px)" srcset="//bizweb.dktcdn.net/thumb/medium/100/312/791/articles/blog2-min.jpg?v=1527698391267">
															<source media="(min-width: 480px)" srcset="//bizweb.dktcdn.net/thumb/grande/100/312/791/articles/blog2-min.jpg?v=1527698391267">
															<img src="//bizweb.dktcdn.net/thumb/large/100/312/791/articles/blog2-min.jpg?v=1527698391267" alt="Cách làm bánh Black Forest mê hoặc" class="img-responsive center-block">
														</picture>
													</a>						
												</div>
												<div class="blog-item-mains">
													<h3 class="blog-item-name">
														<a href="/cach-lam-banh-black-forest-me-hoac" title="Cách làm bánh Black Forest mê hoặc">Cách làm bánh Black Forest mê hoặc</a>
													</h3>									
												</div>
											</div>
										</article>
																							
										<article class="col-md-12 col-sm-12 col-xs-12 clearfix">
											<div class="blog-item">
												<div class="blog-item-thumbnail">						
													<a href="/cach-lam-banh-panna-cotta-ta-xanh-thom-mat">
														<picture>
															<source media="(min-width: 1200px)" srcset="//bizweb.dktcdn.net/thumb/medium/100/312/791/articles/blog1-min.jpg?v=1527697793133">
															<source media="(min-width: 992px)" srcset="//bizweb.dktcdn.net/thumb/medium/100/312/791/articles/blog1-min.jpg?v=1527697793133">
															<source media="(min-width: 767px)" srcset="//bizweb.dktcdn.net/thumb/grande/100/312/791/articles/blog1-min.jpg?v=1527697793133">
															<source media="(min-width: 666px)" srcset="//bizweb.dktcdn.net/thumb/grande/100/312/791/articles/blog1-min.jpg?v=1527697793133">
															<source media="(min-width: 569px)" srcset="//bizweb.dktcdn.net/thumb/medium/100/312/791/articles/blog1-min.jpg?v=1527697793133">
															<source media="(min-width: 480px)" srcset="//bizweb.dktcdn.net/thumb/grande/100/312/791/articles/blog1-min.jpg?v=1527697793133">
															<img src="//bizweb.dktcdn.net/thumb/large/100/312/791/articles/blog1-min.jpg?v=1527697793133" alt="Cách làm bánh Panna Cotta trà xanh thơm mát" class="img-responsive center-block">
														</picture>
													</a>						
												</div>
												<div class="blog-item-mains">
													<h3 class="blog-item-name">
														<a href="/cach-lam-banh-panna-cotta-ta-xanh-thom-mat" title="Cách làm bánh Panna Cotta trà xanh thơm mát">Cách làm bánh Panna Cotta trà xanh thơm mát</a>
													</h3>										
												</div>
											</div>
										</article>
									</div>
								</div>
							</div>
						</div>
					</div>
				</aside>
				<section class="right-content col-md-9 col-md-push-3">	
					<div class="row">
						<div class="col-lg-12">
							<div class="postby">
								<span>
									<i class="fa fa-user"></i> Nguyễn Huỳnh Quốc Việt - 
									<i class="fa fa-calendar"></i> 31/05/2018
								</span>
							</div>
							<div>
								<div class="article-details">
									<div class="article-content">
										<div class="rte">
											<p>
												<strong>
													<em>Tuy có cách làm khá cầu kỳ với nhiều lớp bánh khác nhau, nhưng thành quả thì lại rất “ngọt ngào” với vị matcha thanh ngọt cùng vị beo béo của kem tươi. Matcha Delight chắc chắn sẽ làm bạn “đổ” ngay lập tức luôn đấy! Cùng Luz Bakery&nbsp;học cách làm bánh Matcha Delight này ngay thôi nào!&nbsp;</em>
												</strong>
											</p>
											<p><strong><em><img alt="Cách làm bánh Matcha Delight" data-thumb="original" original-height="400" original-width="600" src="//bizweb.dktcdn.net/100/312/791/files/blog4-min.jpg?v=1527699874266"></em></strong></p>
											<p><u><strong>Nguyên liệu làm bánh Matcha Delight</strong></u></p>
											<p><strong>Phần 1:</strong></p>
											<p>– 2 lòng trắng trứng.</p>
											<p>– ½ quả chanh.</p>
											<p>– 50gr đường.</p>
											<p>– 2 lòng đỏ trứng.</p>
											<p>– 15ml sữa tươi.</p>
											<p>– 15ml dầu ăn.</p>
											<p>– 20gr bột ngô.</p>
											<p>– 20gr bột mì.</p>
											<p>– 2 tsp bột trà xanh.</p>
											<p><strong>Phần 2:</strong></p>
											<p>– 2 hộp sữa chua.</p>
											<p>– 7gr gelatin đun chảy.</p>
											<p>– 150gr kem tươi.</p>
											<p>– 25gr đường xay.</p>
											<p><strong>Phần 3:</strong></p>
											<p>– 125ml sữa tươi đun nóng.</p>
											<p>– 2 lòng đỏ trứng.</p>
											<p>– 2 tsp bột trà xanh.</p>
											<p>– 6gr gelatin đun chảy.</p>
											<p>– 190ml kem tươi.</p>
											<p><strong>Phần 4:</strong></p>
											<p>– 45ml sữa tươi.</p>
											<p>– 45ml kem tươi.</p>
											<p>– 30gr bơ nhạt.</p>
											<p>– 100gr chocolate trắng.</p>
											<p>– 2 tsp bột trà xanh.</p>
											<p>– 5gr gelatin đun chảy.</p>
											<p><u><strong>Dụng cụ làm bánh Matcha Delight</strong></u></p>
											<p>– Tô.</p>
											<p>– Nồi.</p>
											<p>– Máy đánh trứng.</p>
											<p>– Khuôn bánh.</p>
											<p>– Giấy nến.</p>
											<p>– Lò nướng.</p>
											<p><u><strong>Cách làm bánh Matcha Delight</strong></u></p>
											<p><strong>Bước 1:</strong></p>
											<p>– Để làm lớp bánh đầu tiên, bạn dùng máy đánh trứng để đánh bông hỗn hợp lòng trắng trứng, chanh và đường (trong nguyên liệu phần 1) trong 1 cái tô lớn.</p>
											<p><strong>Bước 2:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strong></p>
											<p>– Cho thêm 2 lòng đỏ trứng, 15ml sữa tươi và 15ml dầu ăn vào hỗn hợp lòng trắng trứng ở bước 1 rồi tiếp tục dùng máy đánh trứng để đánh đều hỗn hợp.</p>
											<p><strong>Bước 3:</strong></p>
											<p>– Thêm 20gr bột ngô, 20gr bột mì và 2tsp bột trà xanh vào tô hỗn hợp ở bước 3.</p>
											<p>– Sau đó dùng máy để đánh cho các phần nguyên liệu quyện với nhau và dùng phới để trộn đều lại 1 lần nữa.</p>
											<p><strong>Bước 4:</strong></p>
											<p>– Trút hỗn hợp bột ở bước 3 vào khuôn bánh đã lót sẵn giấy nến, dàn đều rồi mang đi nướng ở nhiệt độ 170 độ C trong vòng 20 phút.</p>
											<p><strong>Bước 5:</strong></p>
											<p>– Với lớp bánh thứ 2, bạn trộn đều gelatin đã đun chảy cùng với sữa chua trong 1 cái tô nhỏ.</p>
											<p><strong>Bước 6:</strong></p>
											<p>– Lấy 1 cái tô lớn khác, cho vào tô 150gr kem tười cùng 25gr đường xay rồi dùng máy để đánh cho hỗn hợp bông mịn.</p>
											<p><strong>Bước 7:</strong></p>
											<p>– Cho hỗn hợp sữa chua đã làm ở bước 5 vào tô hỗn hợp kem tươi ở bước 6 rồi dùng phới trộn đều 2 phần hỗn hợp.</p>
											<p><strong>Bước 8:</strong></p>
											<p>– Đổ hỗn hợp ở bước 7 vào lớp bánh thứ nhất đã nướng chín rồi để khuôn bánh vào trong ngăn lạnh khoảng 1 tiếng.</p>
											<p><strong>Bước 9:</strong></p>
											<p>– Để làm lớp bánh thứ 3, bạn hãy hòa đều 2 lòng đỏ trứng với 125ml sữa tươi đun nóng (chỉ đun cho sữa nóng lên chứ không đun sôi sữa nhé!) và 2 tsp bột trà xanh.</p>
											<p>– Sau đó cho thêm gelatin đã đun chảy vào tô hỗn hợp trên, khuấy đều.</p>
											<p><strong>Bước 10:</strong></p>
											<p>– Đánh bông 190ml kem tươi trong 1 cái tô sạch, lớn. Sau đó đổ hỗn hợp sữa ở bước 9 vào khuấy đều cùng cho đến khi 2 phần hỗn hợp quyện với nhau.</p>
											<p><strong>Bước 11:</strong></p>
											<p>– Đổ tiếp hỗn hợp kem tươi ở bước 10 vào khuôn bánh để tạo thành lớp bánh thứ 3 rồi để lại vào trong ngăn lạnh cho đến khi lớp bánh này đông lại.</p>
											<p><strong>Bước 12:</strong></p>
											<p>– Với lớp bánh cuối cùng, bạn đun cách thủy 45ml sữa tươi, 45ml kem tươi, 30gr bơ nhạt, 100gr chocolate trắng, 2 tsp bột trà xanh và 5gr gelatin đun chảy cho đến khi tất cả các nguyên liệu tan chảy và quyện đều với nhau thì tắt bếp.</p>
											<p><strong>Bước 13:</strong></p>
											<p>– Đổ hỗn hợp đã đun ở bước 12 vào khuôn bánh để tạo thành lớp bánh cuối cùng rồi để trong ngăn lạnh khoảng 5 tiếng để lớp bánh thứ 4 đông lại và chiếc bánh được định hình.</p>
											<p><strong>Bước 14:</strong></p>
											<p>– Sau khi bánh đã để lạnh đủ thời gian thì lấy bánh ra khỏi khuôn, cắt thành những miếng vừa ăn và thưởng thức thôi!</p>
											<p>Chúc các bạn thành công với&nbsp;<strong>cách làm bánh Matcha Delight</strong>&nbsp;này nhé!</p>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-12">
							<div class="blog_related">
								<h2>Bài viết liên quan</h2>
								<article class="blog_entry clearfix">
									<h3 class="blog_entry-title">
										<a rel="bookmark" href="/cach-lam-mousse-chanh-leo" title="Cách làm mousse chanh leo">
											<i class="fa fa-caret-right" aria-hidden="true"></i> Cách làm mousse chanh leo
										</a>
									</h3>
								</article>
								<article class="blog_entry clearfix">
									<h3 class="blog_entry-title">
										<a rel="bookmark" href="/cach-lam-banh-black-forest-me-hoac" title="Cách làm bánh Black Forest mê hoặc">
											<i class="fa fa-caret-right" aria-hidden="true"></i> Cách làm bánh Black Forest mê hoặc
										</a>
									</h3>
								</article>
								<article class="blog_entry clearfix">
									<h3 class="blog_entry-title">
										<a rel="bookmark" href="/cach-lam-banh-panna-cotta-ta-xanh-thom-mat" title="Cách làm bánh Panna Cotta trà xanh thơm mát">
											<i class="fa fa-caret-right" aria-hidden="true"></i> Cách làm bánh Panna Cotta trà xanh thơm mát
										</a>
									</h3>
								</article>
							</div>
						</div>
						<div class="col-12">
							<div id="article-comments">
								<h5 class="title-form-coment">Bình luận (2)</h5>
								<div class="article-comment clearfix">
									<figure class="article-comment-user-image">
										<img src="https://www.gravatar.com/avatar/c9b25479cdcfed34dab4c4599e0163d9?s=110&amp;d=identicon" alt="binh-luan" class="block">
									</figure>
									<div class="article-comment-user-comment">
										<p class="user-name-comment"><strong>An Nguyen</strong>
											<a href="#article_comments" class="btn-link pull-xs-right d-none">Trả lời</a></p>
										<span class="article-comment-date-bull">21/08/2018</span>
										<p>a</p>
									</div>
								</div> 						
								<div class="article-comment clearfix">
									<figure class="article-comment-user-image">
										<img src="https://www.gravatar.com/avatar/c9b25479cdcfed34dab4c4599e0163d9?s=110&amp;d=identicon" alt="binh-luan" class="block">
									</figure>
									<div class="article-comment-user-comment">
										<p class="user-name-comment"><strong>An Nguyen</strong>
											<a href="#article_comments" class="btn-link pull-xs-right d-none">Trả lời</a></p>
										<span class="article-comment-date-bull">27/07/2018</span>
										<p>Đổ hỗn hợp đã đun ở bước 12 vào khuôn bánh để tạo thành lớp bánh cuối cùng rồi để trong ngăn lạnh khoảng 5 tiếng để lớp bánh thứ 4 đông lại và chiếc bánh được định hình.</p>
									</div>
								</div> 
							</div>
							<div class="text-xs-right"></div>
							<form accept-charset="UTF-8" action="/posts/cach-lam-banh-matcha-delight/comments" id="article_comments" method="post" class="has-validation-callback">
								<input name="FormType" type="hidden" value="article_comments">
								<input name="utf8" type="hidden" value="true"> 
								<div class="col-lg-12">
									<div class="form-coment margin-bottom-30" style="margin-left: -15px">
										<h5 class="title-form-coment">VIẾT BÌNH LUẬN CỦA BẠN:</h5>
										<fieldset class="art-comment-name form-group col-12 col-sm-6 col-md-6">	
											<input placeholder="Họ tên" type="text" class="form-control form-control-lg" value="" id="full-name" name="Author" required="">
										</fieldset>
										<fieldset class="form-group col-12 col-sm-6 col-md-6">										
											<input placeholder="Email" type="email" class="form-control form-control-lg" value="" id="email" name="Email" required="">
										</fieldset>
										<fieldset class="form-group col-12 col-sm-12 col-md-12">										
											<textarea placeholder="Nội dung" class="form-control form-control-lg" id="comment" name="Body" rows="6" required=""></textarea>
										</fieldset>
										<div>
											<button type="submit" class="btn btn-white">Gửi bình luận</button>
										</div>
									</div> <!-- End form mail -->
								</div>
							</form>
						</div>
					</div>
				</section>
			</div>
		</div>

    	<?php include('footer.php') ?>

	</body>

        <?php include('script.php') ?>

</html>