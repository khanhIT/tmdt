<?php

return [
    0 => [
        'module_name' => 'Quản lý ứng viên',
        'permission_code' => 'customer',
        'screen' => [
            0 => [
                'screen_name' => 'Danh sách khách hàng',
                'permission_code' => 'customer_list',
            ],
            1 => [
                'screen_name' => 'Xem khách hàng',
                'permission_code' => 'customer_view',
            ],
        ]
    ],
    1 => [
        'module_name' => 'Quản lý sản phẩm',
        'permission_code' => 'product',
        'screen' => [
            0 => [
                'screen_name' => 'Danh sách sản phẩm',
                'permission_code' => 'product_list',
            ],
            1 => [
                'screen_name' => 'Thêm mới sản phẩm',
                'permission_code' => 'product_create',
            ],
            2 => [
                'screen_name' => 'Chỉnh sửa sản phẩm',
                'permission_code' => 'product_edit',
            ],
            3 => [
                'screen_name' => 'Xoá sản phẩm',
                'permission_code' => 'product_delete',
            ],
            4 => [
            	'screen_name' => 'Xem sản phẩm',
            	'permission_code' => 'recipe_view',
            ],
        ]
    ],
    2 => [
        'module_name' => 'Quản lý công thức',
        'permission_code' => 'recipe',
        'screen' => [
            0 => [
                'screen_name' => 'Danh sách công thức',
                'permission_code' => 'recipe_list',
            ],
            1 => [
                'screen_name' => 'Thêm mới công thức',
                'permission_code' => 'recipe_create',
            ],
            2 => [
                'screen_name' => 'Chỉnh sửa công thức',
                'permission_code' => 'recipe_edit',
            ],
            3 => [
                'screen_name' => 'Xoá công thức',
                'permission_code' => 'recipe_delete',
            ],
            4 => [
                'screen_name' => 'Xem công thức',
                'permission_code' => 'recipe_view',
            ],
        ]
    ],
    3 => [
        'module_name' => 'Quản lý đơn hàng',
        'permission_code' => 'order',
        'screen' => [
            0 => [
                'screen_name' => 'Danh sách đơn hàng',
                'permission_code' => 'order_list',
            ],
            1 => [
                'screen_name' => 'Xem đơn hàng',
                'permission_code' => 'order_view',
            ],
            2 => [
                'screen_name' => 'In đơn hàng',
                'permission_code' => 'order_printf',
            ],
            3 => [
                'screen_name' => 'Xoá đơn hàng',
                'permission_code' => 'order_delete',
            ],
        ]
    ],
    4 => [
        'module_name' => 'Quản lý nhóm quyền',
        'permission_code' => 'role',
        'screen' => [
            0 => [
                'screen_name' => 'Danh sách nhóm quyền',
                'permission_code' => 'role_list',
            ],
            1 => [
                'screen_name' => 'Thêm mới nhóm quyền',
                'permission_code' => 'role_create',
            ],
            2 => [
                'screen_name' => 'Chỉnh sửa nhóm quyền',
                'permission_code' => 'role_edit',
            ],
            3 => [
                'screen_name' => 'Xoá nhóm quyền',
                'permission_code' => 'role_delete',
            ],
        ]
    ],
    5 => [
    	'module_name' => 'Quản lý liên hệ',
    	'permission_code' => 'contact',
    	'screen' => [
    		0 => [
    			'screen_name' => 'Danh sách liên hệ',
    			'permission_code' => 'contact_list',
    		],
    		1 => [
    			'screen_name' => 'Xem liên hệ',
    			'permission_code' => 'contact_view',
    		],
    		2 => [
    			'screen_name' => 'Xóa liên hệ',
    			'permission_code' => 'contact_delete',
    		],
    		3 => [
    			'screen_name' => 'Sửa liên hệ',
    			'permission_code' => 'contact_edit',
    		],
    	]
    ],
    6 => [
    	'module_name' => 'Quản lý danh mục sản phẩm',
    	'permission_code' => 'category',
    	'screen' => [
    		0 => [
    			'screen_name' => 'Danh sách danh mục sản phẩm',
    			'permission_code' => 'category_list'
    		],
    		1 => [
    			'screen_name' => 'Thêm mới danh mục sản phẩm',
    			'permission_code' => 'category_create'
    		],
    		2 => [
    			'screen_name' => 'Sửa danh mục sản phẩm',
    			'permission_code' => 'category_edit',
    		],
    		3 => [
    			'screen_name' => 'Xóa danh mục sản phẩm',
    			'permission_code' => 'category_delete',
    		],
    	]
    ],
];

