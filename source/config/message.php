<?php

return [
    'create_success' => '%s đã được tạo thành công.',
    'create_failed' => 'Đã xảy ra lỗi khi thêm mới %s.',
    'update_failed' => 'Đã xảy ra lỗi khi cập nhật %s.',
    'update_success' => '%s đã được cập nhật thành công.',
    'delete_permission_denied' => 'Bạn không có quyền thực hiện tác vụ này!',
    'delete_success' => 'Dữ liệu đã được xoá thành công.',
    'internal_server_error' => 'Oops! Something went wrong!',
    'login_wrong' => 'Địa chỉ email hoặc mật khẩu không đúng.',
    'change_password' => 'Mật khẩu đã được thay đổi thành công',
    'success_add_to_cart' => '%s đã thêm vào giỏ hàng của bạn',
    'active_register' => 'Bạn đã kích hoạt tài khoản thành công. Bây giờ bạn có thể đăng nhập ngay'
];
