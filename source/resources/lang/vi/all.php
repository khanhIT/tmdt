<?php

return [
  'profile' => 'Thông tin cá nhân',
  'logout'  => 'Đăng xuất',
  'dashboard' => 'Bảng điều khiển',
  'customer'  => 'Khách hàng',
  'product'  => 'Sản phẩm',
  'category' => 'Danh mục sản phẩm',
  'repice'  => 'Công thức',
  'contact' => 'Liên hệ',
  'order_manage'   => 'Quản lý đơn hàng',
  'order'   => 'Đơn hàng',
  'user' => 'Quản trị hệ thống',
  'role' => 'Nhóm và phân quyền',
  'user_manage' => 'Quản lý người dùng hệ thống',
  'system' => 'Thông tin hệ thống',
  'view_website' => 'Trang web',
  'users' => 'Người dùng',
  'edit' => 'Sửa',
  'view' => 'Xem',
  'delete' => 'Xóa',
  'add' => 'Thêm mới',
  'export' => 'Xuất'
];
