<?php

return [
  'profile' => 'Profile',
  'logout' => 'Logout',
  'dashboard' => 'Dashboard',
  'customer'  => 'Customer',
  'product'  => 'Product',
  'category' => 'Category Product',
  'repice'  => 'Repice',
  'contact' => 'Contact',
  'order_manage'   => 'Order Management',
  'order'   => 'Order',
  'user' => 'System management',
  'role' => 'Role and Permissions',
  'user_manage' => 'User Management',
  'system' => 'System information',
  'View website' => 'View website',
  'users' => 'Users',
  'edit' => 'Edit',
  'view' => 'View',
  'delete' => 'Delete',
  'add' => 'Add',
  'export' => 'Export'
];
