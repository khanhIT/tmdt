<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" type="image/x-icon" href="{{ URL::to('/') }}/frontend/images/favicon.png">
        {{-- <link rel="stylesheet" href="{{ asset('css/all.css') }}"> --}}

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}"/>

        <!-- fanpage facebook -->
        <script async defer crossorigin="anonymous" src="https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v3.3&appId=280819369010997&autoLogAppEvents=1"></script>
        
        @yield('meta')
        @toastr_css
        @include('Elements.Layout.mainStyle')
        @yield('stylesheetAddon')

    </head>
    <body>
        @include('Elements.Layout.header')

        @yield('content')
        
        @include('Elements.Layout.footer')
        @include('Elements.Layout.mainJs')
        @toastr_js
        @toastr_render

        @yield('scriptAddon')
    </body>
</html>
