@extends('Layouts.main')

@section('meta')
    <title>LuzBakery</title>
@stop()

@section('content')
	
    @include('Frontend.Slide.index')

    @include('Frontend.ProductSelling.listProduct')

    @include('Frontend.Banner.index')

    @include('Frontend.Product.listProduct')

    @include('Frontend.Recipes.listItems')

    @include('Frontend.Banner.index')

    @include('Elements.Layout.about')
@stop()
