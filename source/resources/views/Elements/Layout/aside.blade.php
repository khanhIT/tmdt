<aside class="sidebar left-content col-lg-3 col-md-3 col-lg-pull-9 col-md-pull-9">
	<aside class="aside-item collection-category">
		<div class="aside-title">
			<h3 class="title-head"><span>Danh mục sản phẩm</span></h3>
		</div>
		<div class="aside-content">
			<nav class="nav-category navbar-toggleable-md">
				<ul class="nav navbar-pills">
					@foreach($category as $cate)
						<li class="nav-item">
							<a class="nav-link" href="{{ route('listCake', $cate->id) }}">{{ $cate->name }}</a>
						</li>
					@endforeach
				</ul>
			</nav>
		</div>
	</aside>
	<div class="aside-item aside-filter collection-category">	
		<div class="aside-hidden-mobile">
			<div class="filter-container">
				<aside class="aside-item filter-price">
					<div class="aside-title">
						<h2 class="title-head margin-top-0"><span>Giá sản phẩm</span></h2>
					</div>
					<div class="aside-content filter-group">
						<ul class="filter">
							<li class="filter-item filter-item--check-box filter-item--green">
								<span>
									<label for="filter-duoi-100-000d">
										<input type="checkbox" id="filter-duoi-100-000d" data-group="Khoảng giá" data-field="price_min" data-text="Dưới 100.000đ" value="(<100000)" data-operator="OR">
										<i class="fa"></i>
										Giá dưới 100.000đ
									</label>
								</span>
							</li>
							<li class="filter-item filter-item--check-box filter-item--green">
								<span>
									<label for="filter-100-000d-200-000d">
										<input type="checkbox" id="filter-100-000d-200-000d" onchange="toggleFilter(this)" data-group="Khoảng giá" data-field="price_min" data-text="100.000đ - 200.000đ" value="(>100000 AND <200000)" data-operator="OR">
										<i class="fa"></i>
										100.000đ - 200.000đ							
									</label>
								</span>
							</li>	
							<li class="filter-item filter-item--check-box filter-item--green">
								<span>
									<label for="filter-200-000d-300-000d">
										<input type="checkbox" id="filter-200-000d-300-000d" onchange="toggleFilter(this)" data-group="Khoảng giá" data-field="price_min" data-text="200.000đ - 300.000đ" value="(>200000 AND <300000)" data-operator="OR">
										<i class="fa"></i>
										200.000đ - 300.000đ							
									</label>
								</span>
							</li>	
							<li class="filter-item filter-item--check-box filter-item--green">
								<span>
									<label for="filter-300-000d-500-000d">
										<input type="checkbox" id="filter-300-000d-500-000d" onchange="toggleFilter(this)" data-group="Khoảng giá" data-field="price_min" data-text="300.000đ - 500.000đ" value="(>300000 AND <500000)" data-operator="OR">
										<i class="fa"></i>
										300.000đ - 500.000đ							
									</label>
								</span>
							</li>	
							<li class="filter-item filter-item--check-box filter-item--green">
								<span>
									<label for="filter-500-000d-1-000-000d">
										<input type="checkbox" id="filter-500-000d-1-000-000d" onchange="toggleFilter(this)" data-group="Khoảng giá" data-field="price_min" data-text="500.000đ - 1.000.000đ" value="(>500000 AND <1000000)" data-operator="OR">
										<i class="fa"></i>
										500.000đ - 1.000.000đ							
									</label>
								</span>
							</li>	
							<li class="filter-item filter-item--check-box filter-item--green">
								<span>
									<label for="filter-tren1-000-000d">
										<input type="checkbox" id="filter-tren1-000-000d" onchange="toggleFilter(this)" data-group="Khoảng giá" data-field="price_min" data-text="Trên 1.000.000đ" value="(>1000000)" data-operator="OR">
										<i class="fa"></i>
										Giá trên 1.000.000đ
									</label>
								</span>
							</li>							
						</ul>
					</div>
				</aside>

				<!-- fanpage facebook -->
				<aside class="aside-item filter-type">
					<div class="aside-title">
						<h2 class="title-head margin-top-0"><span>Fanpage</span></h2>
					</div>
					<div class="aside-content filter-group">
					<div class="fb-page" data-href="https://www.facebook.com/khanhLaravel/" data-tabs="timeline" data-width="" data-height="" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true">
						<blockquote cite="https://www.facebook.com/khanhLaravel/" class="fb-xfbml-parse-ignore">
							<a href="https://www.facebook.com/khanhLaravel/">Khánh Hoàng</a>
						</blockquote>
					</div>
					</div>
				</aside>	
			</div>
		</div>
	</div>
</aside>