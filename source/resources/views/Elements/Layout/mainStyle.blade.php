<link rel="stylesheet" href="{{ asset('frontend/lib/bootstrap/css/bootstrap.min.css') }}?{{ time() }}">
<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css?{{ time() }}" rel="stylesheet">
<link rel="stylesheet" href="{{ asset('frontend/lib/textillate/animate.css') }}?{{ time() }}">
<link rel="stylesheet" href="{{ asset('frontend/lib/OwlCarousel2-2.3.4/owl.carousel.min.css') }}?{{ time() }}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css?{{ time() }}">
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css?{{ time() }}" rel="stylesheet" />
<link rel="stylesheet" href="{{ asset('frontend/font/font.css') }}?{{ time() }}">
<link rel="stylesheet" href="{{ asset('frontend/css/base.css') }}?{{ time() }}">
<link rel="stylesheet" href="{{ asset('frontend/css/home/header.css') }}?{{ time() }}">
<link rel="stylesheet" href="{{ asset('frontend/css/home/slide.css') }}?{{ time() }}">
<link rel="stylesheet" href="{{ asset('frontend/css/home/productSelling.css') }}?{{ time() }}">
<link rel="stylesheet" href="{{ asset('frontend/css/home/banner.css') }}?{{ time() }}">
<link rel="stylesheet" href="{{ asset('frontend/css/home/product.css') }}?{{ time() }}">
<link rel="stylesheet" href="{{ asset('frontend/css/home/recipe.css') }}?{{ time() }}">
<link rel="stylesheet" href="{{ asset('frontend/css/home/about.css') }}?{{ time() }}">
<link rel="stylesheet" href="{{ asset('frontend/css/home/footer.css') }}?{{ time() }}">
<link rel="stylesheet" href="{{ asset('frontend/css/intro/intro.css') }}?{{ time() }}">
<link rel="stylesheet" href="{{ asset('frontend/css/product/products.css') }}?{{ time() }}">
<link rel="stylesheet" href="{{ asset('frontend/css/product/productDetail.css') }}?{{ time() }}">
<link rel="stylesheet" href="{{ asset('frontend/css/recipes/recipes.css') }}?{{ time() }}">
<link rel="stylesheet" href="{{ asset('frontend/css/recipes/recipesDetail.css') }}?{{ time() }}">
<link rel="stylesheet" href="{{ asset('frontend/css/cart/cart.css') }}?{{ time() }}">
<link rel="stylesheet" href="{{ asset('frontend/css/cart/checkout.css') }}?{{ time() }}">
<link rel="stylesheet" href="{{ asset('frontend/css/contact/contact.css') }}?{{ time() }}">
<link rel="stylesheet" href="{{ asset('frontend/css/phone_ring.css') }}?{{ time() }}">
