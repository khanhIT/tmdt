<!-- phone ring -->
<div class="phonering-alo-phone phonering-alo-green phonering-alo-show" id="phonering-alo-phoneIcon">
	<div class="phonering-alo-ph-circle"></div>
 	<div class="phonering-alo-ph-circle-fill"></div>
	<a href="tel:+84973605319" class="pps-btn-img" title="Liên hệ">
		<div class="phonering-alo-ph-img-circle"></div>
	</a>
</div>

<!-- end -->
<div class="bf-fot">
	<div class="container">
		<div class="row">
			<div class="col-xs-12" style="width: 100%">
				<p>Chúng tôi luôn mang đến những chiếc bánh tươi ngon nhất</p>
			</div>
		</div>
	</div>
</div>
<footer class="footer">
	<div class="bg-fot-frame"></div>
	<div class="site-footer">

		<div class="footer-inner padding-top-25 padding-bottom-10">
			<div class="fot-contact-info">
				<a href="/">
					<img src="{{ asset('frontend/images/logo/logo.png') }}" data-lazyload="images/logo/logo.png" alt="logo Luz Bakery">
				</a>
				<h3>Luz Bakery</h3>
				<p>Số 280 Cổ Nhuế - Bắc Từ Liêm - Hà Nội</p>
				<a href="tel:0973 605 319">0973 605 319</a><span class="hidden-xs">&nbsp;-&nbsp;</span>
				<a href="mailto:luz.elioteam@gmail.com">khanhhoang220596@gmail.com</a>
			</div>
			<div class="back-to-top show">
				<p class="holder">
					<a href="javascript:void(0);" id="back-to-top" class="animBtn themeD">Về đầu trang</a>
				</p>
			</div>
		</div>
	</div>

	<div class="copyright clearfix">
		<div class="inner clearfix">
			<div class=" a-center">
				<span>© Bản quyền thuộc về <b>Khánh Team</b>
					<span class="s480-f">|</span> Cung cấp bởi
					<a href="#" title="Khánh" target="_blank" rel="nofollow">Khánh Hoàng</a>
				</span>
			</div>
		</div>
	</div>
</footer>
