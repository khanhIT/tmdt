<div class="col-xs-12 col-sm-6 text-xs-left text-sm-right">
    <div id="sort-by">
        <label class="left hidden-xs">Sắp xếp: </label>
        <ul>
            <li>
                <span>Sắp xếp</span>
                <ul>								
                    <li>
                        <a href="javascript:void(0);">A → Z</a>
                    </li>
                    <li>
                        <a href="javascript:void(0);">Z → A</a>
                    </li>
                    <li>
                        <a href="javascript:void(0);">Giá tăng dần</a>
                    </li>
                    <li>
                        <a href="javascript:void(0);">Giá giảm dần</a>
                    </li>
                    <li>
                        <a href="javascript:void(0);">Hàng mới nhất</a>
                    </li>
                    <li>
                        <a href="javascript:void(0);">Hàng cũ nhất</a>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</div>