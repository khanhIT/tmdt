<aside class="sidebar left left-content col-md-3 col-md-pull-9">
	<div class="border-bg">
		<aside class="aside-item collection-category blog-category">	
			<div class="heading">
				<h2 class="title-head">
					<span>Danh mục</span>
				</h2>
			</div>	
			<div class="aside-content">
				<nav class="nav-category  navbar-toggleable-md">
					<ul class="nav navbar-pills">
						<li class="nav-item ">
							<a class="nav-link" href="/">Trang chủ</a>
						</li>
						<li class="nav-item ">
							<a class="nav-link" href="{{ route('intro') }}">Giới thiệu</a>
						</li>
						<li class="nav-item ">
							<a href="{{ route('product') }}" class="nav-link">Sản phẩm</a>
							<i class="fa fa-angle-down"></i>
							<ul class="dropdown-menu">
								@foreach($category as $cate)
									<li class="nav-item">
										<a class="nav-link" href="{{ route('listCake', $cate->id) }}">{{ $cate->name }}</a>
									</li>
								@endforeach
							</ul>
						</li>
						<li class="nav-item active">
							<a class="nav-link" href="{{ route('recipe') }}">Công thức</a>
						</li>
						<li class="nav-item ">
							<a class="nav-link" href="{{ route('contact') }}">Liên hệ</a>
						</li>
					</ul>
				</nav>
			</div>
		</aside>

		<div class="aside-item">
			<div>
				<div class="heading">
					<h2 class="title-head"><a href="#">Bài viết nổi bật</a></h2>
				</div>
				<div class="list-blogs">
					<div class="row">
						@foreach($hotRecipe as $hot)
						<article class="col-md-12 col-sm-12 col-xs-12 clearfix">
							<div class="blog-item">
								<div class="blog-item-thumbnail">						
									<a href="{{ route('detailRecipe', $hot->slug) }}">
										<picture>
											<source media="(min-width: 1200px)" srcset="{{ $hot->thumbnail }}">
											<source media="(min-width: 992px)" srcset="{{ $hot->thumbnail }}">
											<source media="(min-width: 767px)" srcset="{{ $hot->thumbnail }}">
											<source media="(min-width: 666px)" srcset="{{ $hot->thumbnail }}">
											<source media="(min-width: 569px)" srcset="{{ $hot->thumbnail }}">
											<source media="(min-width: 480px)" srcset="{{ $hot->thumbnail }}">
											<img src="{{ $hot->thumbnail }}" alt="{{ $hot->title }}" class="img-responsive center-block">
										</picture>
									</a>						
								</div>
								<div class="blog-item-mains">
									<h3 class="blog-item-name">
										<a href="{{ route('detailRecipe', $hot->slug) }}" title="{{ $hot->title }}">{{ $hot->title }}</a>
									</h3>	
								</div>
							</div>
						</article>
						@endforeach()
					</div>
				</div>
			</div>
		</div>
	</div>
</aside>