@extends('Backend.Layouts.master')

@section('title')
	<title>Xem liên hệ | LuzBakery</title>
@stop()

@section('styleAdd')
	<style>
		.p-d-15 h5{
			font-weight: 700
		}
	</style>
@stop()

@section('content')
	<section class="content">
        <ul class="breadcrumb">
		  <li>
		  	<a href="{{ route('dashboard') }}">
		  		<i class="fa fa-home" aria-hidden="true"></i>
		  	 	Bảng điểu khiển
		  	</a>
		  </li>
		  <li>
		  	<a href="{{ route('contact.index') }}">Liên hệ</a>
		  </li>
		  <li>Xem liên hệ</li>
		</ul>
		<div class="row">
			<div class="col-md-8">
				<div class="box p-d-15">
					<h5>Thông tin liên hệ</h5>
					<hr>
					<div>
						<p>Họ tên : Hoàng Minh Khánh</p>
						<p>Email : khanhhoang220596@gmail.com</p>
						<p>Số điện thoại : 0973.605.319</p>
						<p>Nội dung : Hà Nội</p>
						<p></p>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="box p-d-15">
					<h5>Xuất bản</h5>
					<hr>
					<button class="btn btn-sm btn-info"><i class="fa fa-save"></i> Lưu</button>
				</div>
			</div>
		</div>
	</section>
@stop()