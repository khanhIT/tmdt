@extends('Backend.Layouts.master')

@section('title')
	<title>Sửa liên hệ | LuzBakery</title>
@stop()

@section('styleAdd')
	<style>
		.p-d-15 h5{
			font-weight: 700
		}
	</style>
@stop()

@section('content')
	<section class="content">
        <ul class="breadcrumb">
		  <li>
		  	<a href="{{ route('dashboard') }}">
		  		<i class="fa fa-home" aria-hidden="true"></i>
		  	 	Bảng điểu khiển
		  	</a>
		  </li>
		  <li>
		  	<a href="{{ route('contact.index') }}">Liên hệ</a>
		  </li>
		  <li>Xem liên hệ</li>
		</ul>
		<div class="row">
			<form action="{{ route('contact.update', $contact->id) }}" method="post">
			@csrf
			@method('PUT')
				<div class="col-md-8">
					<div class="box p-d-15">
						<h5>Thông tin liên hệ</h5>
						<hr>
						<div class="form-group">
							<label>Tên liên hệ</label>
							<input type="text" name="full_name" class="form-control" placeholder="Tên liên hệ" value="{{ $contact->full_name }}" readonly>
						</div>
						<div class="form-group">
							<label>Email</label>
							<input type="email" name="email" class="form-control" placeholder="Email" value="{{ $contact->email }}" readonly>
						</div>
						<div class="form-group">
							<label>Số điện thoại</label>
							<input type="number" name="phone" class="form-control" placeholder="Số điện thoại" value="{{ $contact->phone }}" readonly>
						</div>
						<div class="form-group">
							<label>Nội dung</label>
							<textarea class="form-control" name="content" cols="5" rows="3" readonly>{{ $contact->content }}</textarea>
						</div>
						<div class="form-group {{ !empty($errors->first('content_reply')) ? 'has-warning' : '' }}">
							<label>Nội dung trả lời <span class="star">*</span></label>
							<textarea class="form-control" name="content_reply" id="content_reply" cols="10" rows="5">{!! $contact->content_reply !!}</textarea>
							<span class="help-block text-center">{{ !empty($errors->first('content_reply')) ? $errors->first('content_reply') : '' }}</span>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="box p-d-15">
						<h5>Xuất bản</h5>
						<hr>
						<button type="submit" class="btn btn-sm btn-info"><i class="fa fa-save"></i> Gửi</button>
					</div>
				</div>
			</form>
		</div>
	</section>
@stop()

@section('scriptAdd')
	<script src="https://cdn.ckeditor.com/ckeditor5/11.0.1/classic/ckeditor.js"></script>
	<script>
		 ClassicEditor
            .create( document.querySelector( '#content_reply' ) )
            .catch( error => {
                console.error( error );
            } );
	</script>
@stop()