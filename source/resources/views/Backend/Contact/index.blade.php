@extends('Backend.Layouts.master')

@section('title')
	<title>Liên hệ | LuzBakery</title>
@stop()

@section('content')
	<section class="content">
        <ul class="breadcrumb">
		  <li>
		  	<a href="{{ route('dashboard') }}">
		  		<i class="fa fa-home" aria-hidden="true"></i>
		  	 	Bảng điểu khiển
		  	</a>
		  </li>
		  <li>
		  	Liên hệ
		  </li>
		</ul>
		@include('Backend.Errors.sessionSuccess')
		<div class="row">
			<div class="col-md-12 dataTable">
				<div class="box">
					<table id="table" class="table table-hover">
						<thead>
				            <tr>
				                <th>ID</th>
				                <th>Tên</th>
				                <th>Số điện thoại</th>
				                <th>Email</th>
				                <th>Ngày tạo</th>
				                <th class="action">Tác vụ</th>
				            </tr>
				        </thead>
				        <tbody>
						@php $i = 1; @endphp
						@foreach($contacts as $contact)
				            <tr>
				                <td class="vertical">{{ $i }}</td>
				                <td class="vertical">
				                	<a href="{{ route('contact.show', $contact->id) }}">{{ $contact->full_name }}</a>
				                </td>
				                <td>
								{{ $contact->phone }}
				                </td>
				                <td class="vertical">{{ $contact->email }}</td>
				                <td class="vertical">{{ \Carbon\Carbon::parse( $contact->created_at)->format('Y/m/d H:i') }}</td>
				                <td class="vertical">
				                	<a href="{{ route('contact.edit', $contact->id) }}" class="btn btn-sm waves-effect waves-light btn-warning">Sửa</a>
				                	<a class="btn btn-sm btn-outline-danger btn-delete" data-id="{{ $contact->id }}" data-url="{{ route('contact.destroy', $contact->id) }}" data-url-reload="{{ route('contact.index', $contact->id) }}">Xóa</a>
									@if(!empty($contact->content_reply))
									<button class="btn btn-sm waves-effect waves-light btn-rounded btn-outline-primary">Đã gửi mail</button>
									@endif
				                </td>
				            </tr>
						@php $i++; @endphp	
						@endforeach	
				        </tbody>
					</table>
				</div>
			</div>
		</div>
	</section>
@stop()