@extends('Backend.Layouts.master')

@section('title')
	<title>Nhóm và phân quyền | LuzBakery</title>
@stop()

@section('content')
	<section class="content">
        <ul class="breadcrumb">
		  <li>
		  	<a href="{{ route('dashboard') }}">
		  		<i class="fa fa-home" aria-hidden="true"></i>
		  	 	Bảng điểu khiển
		  	</a>
		  </li>
		  <li>
		  	Quản trị hệ thống
		  </li>
		  <li>
		  	Nhóm và phân quyền
		  </li>
		</ul>
		<div class="pull-right" style="margin-top: -50px">
			<a href="{{ route('role.create') }}" class="btn btn-block btn-success">
				<i class="fa fa-plus"></i>
				Thêm mới
			</a>
		</div>
		@include('Backend.Errors.sessionSuccess')
		<div class="row">
			<div class="col-md-12 dataTable">
				<div class="box">
					@include('Backend.Role.Component.list.listRoleItem')
				</div>
			</div>
		</div>
	</section>
@stop()