<table id="table" class="table table-hover">
	<thead>
        <tr>
            <th>TT</th>
            <th>Tên</th>
            <th>Ngày tạo</th>
            <th class="action">Tác vụ</th>
        </tr>
    </thead>
		@php $i=1; @endphp
    @foreach($role as $roleItem)
    <tbody>
        <tr>
            <td class="vertical">{{ $i }}</td>
            <td class="vertical">
            	<a href="{{ route('role.edit', $roleItem->id) }}">{{ $roleItem->name }}</a>
            </td>
            <td class="vertical">{{\Carbon\Carbon::parse($roleItem->created_at)->format('Y/m/d H:i')  }}</td>
            <td class="vertical">
            	<a href="{{ route('role.edit', $roleItem->id) }}" class="btn btn-sm waves-effect waves-light btn-warning">Sửa</a>
            	<a href="javascript:void(0)" class="btn btn-sm btn-outline-danger btn-delete" data-url="{{ route('role.destroy', $roleItem->id) }}" data-url-reload="{{ route('role.index', $roleItem->id) }}">Xóa</a>
            </td>
        </tr>
    </tbody>
		@php $i++; @endphp
    @endforeach()
</table>
