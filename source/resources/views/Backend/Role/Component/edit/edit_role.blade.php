<div class="col-sm-9 box form-group {{ !empty($errors->first('role_name')) ? 'has-warning' : '' }}">
    <label style="padding-top: 10px">Tên <span class="star">*</span></label>
    <hr>
    <input type="text" name="role_name" class="form-control" placeholder="Tên danh mục" value="{{ old('role_name', $role->name) }}">
    <span class="help-block">{{ !empty($errors->first('role_name')) ? $errors->first('role_name') : '' }}</span>
</div>
