<div class="box form-group col-md-9 {{ !empty($errors->first('name')) ? 'has-warning' : '' }}">
	<label style="padding-top: 10px">Tên <span class="star">*</span></label>
	<hr>
	<input type="text" name="name" class="form-control" placeholder="Nhập tên" value="{{ old('name') }}">
	<span class="help-block">{{ !empty($errors->first('name')) ? $errors->first('name') : '' }}</span>
</div>
