@extends('Backend.Layouts.master')

@section('title')
	<title>Sửa danh mục | LuzBakery</title>
@stop()

@section('styleAdd')
  <link rel="stylesheet" href="{{ asset('backend/pages/role/role.css') }}">
@stop()

@section('content')
	<section class="content">
        <ul class="breadcrumb">
			<li>
				<a href="{{ route('dashboard') }}">
					<i class="fa fa-home" aria-hidden="true"></i>
					Bảng điểu khiển
				</a>
		    </li>
		    <li> Quản trị hệ thống </li>
		    <li>
		  		<a href="{{ route('role.index') }}">Nhóm và phân quyền</a>
		    </li>
		  	<li> Sửa quyền </li>
		</ul>
		<div class="pull-right" style="margin-top: -50px">
			<a href="{{ route('role.index') }}" class="btn btn-block btn-success">
				<i class="fa fa-list"></i>
				Danh sách
			</a>
		</div>
		@if(!empty($role))
		<div class="row">
            <div class="col-md-12">
				<form action="{{ route('role.update', $role->id) }}" method="post" role="form" enctype="multipart/form-data" style="padding: 20px">
					@csrf
					@method('PUT')
					<div class="row">
						<div class="col-sm-9">
							@include('Backend.Role.Component.edit.edit_role')
						</div>
						<div class="col-sm-3">
							@include('Backend.Role.Component.edit.action')
						</div>
					</div>
				</form>
            </div>
        </div>
        @endif
	</section>
@stop()
@section('scriptAdd')
   <script type="text/javascript" src="{{ asset('backend/pages/role/role.js') }}"></script>
@stop()
