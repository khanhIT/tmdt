@extends('Backend.Layouts.master')

@section('title')
	<title>Thêm mới quyền | LuzBakery</title>
@stop()

@section('styleAdd')
		<link rel="stylesheet" href="{{ asset('backend/pages/role/role.css') }}">
@stop()

@section('content')
	<section class="content">
    <ul class="breadcrumb">
        <li>
          <a href="{{ route('dashboard') }}">
              <i class="fa fa-home" aria-hidden="true"></i>
              Bảng điểu khiển
          </a>
        </li>
        <li>Quản trị hệ thống</li>
        <li>
             <a href="{{ route('role.index') }}">Nhóm và phân quyền</a>
        </li>
        <li>Thêm mới quyền </li>
		</ul>
		<div class="pull-right" style="margin-top: -50px">
			<a href="{{ route('role.index') }}" class="btn btn-block btn-success">
				<i class="fa fa-list"></i>
				Danh sách
			</a>
		</div>
		<div class="row">
        <div class="col-md-12">
            <form action="{{ route('role.store') }}" method="post" class="role-permission" style="padding: 20px">
                @csrf
                <div class="row">
                      <div class="col-md-9">
                          @include('Backend.Role.Component.add.add_role')
                      </div>
                      <div class="col-md-3">
                          @include('Backend.Role.Component.add.action')
                      </div>
                </div>
            </form>
        </div>
    </div>
	</section>
@stop()
@section('scriptAdd')
  <script type="text/javascript" src="{{ asset('backend/pages/role/role.js') }}"></script>
@stop()
