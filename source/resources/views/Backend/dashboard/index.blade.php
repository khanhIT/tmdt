@extends('Backend.Layouts.master')

@section('title')
	<title>Dashboard | LuzBakery</title>
@stop()

@section('content')
 	<!-- Main content -->
    <section class="content">
      	<h4>
      		<i class="fa fa-home" aria-hidden="true"></i>
  	        <small>@lang('all.dashboard')</small>
        </h4>
      	<div class="no-print">
  	      <div class="callout callout-info">
  	        Chào mừng <em style="color: red">{{ Auth::user()->name }}</em> đến với trang quản trị
  	      </div>
  	    </div>
        <!-- Info boxes -->
        <div class="row">
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-aqua">
                    	<i class="fa fa-money"></i>
                    </span>

                    <div class="info-box-content">
                        <a href="" class="title"><span class="info-box-text">@lang('all.customer')</span></a>
                        <span class="info-box-number">10</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-red"><i class="fa fa-graduation-cap"></i></span>

                    <div class="info-box-content">
                        <a href="" class="title"><span class="info-box-text">@lang('all.users')</span></a>
                        <span class="info-box-number">10</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->

            <!-- fix for small devices only -->
            <div class="clearfix visible-sm-block"></div>

            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-green"><i class="fa fa-paper-plane-o"></i></span>

                    <div class="info-box-content">
                        <a href="" class="title"><span class="info-box-text">@lang('all.order')</span></a>
                        <span class="info-box-number">20</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-yellow">
                    	<i class="fa fa-user-plus"></i>
                    </span>

                    <div class="info-box-content">
                        <a href="" class="title"><span class="info-box-text">NHÂN VIÊN</span></a>
                        <span class="info-box-number">5</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->

            <!-- chart -->
            <div class="col-xs-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Chart Demo</div>

                    <div class="panel-body">
                        {!! $chart->html() !!}
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->

    </section>
    <!-- /.content -->
    {!! Charts::scripts() !!}
    {!! $chart->script() !!}
@stop()
