@extends('Backend.Layouts.master')

@section('title')
	<title>Vận chuyển | LuzBakery</title>
@stop()

@section('content')
	<section class="content">
        <ul class="breadcrumb">
		  <li>
		  	<a href="{{ route('dashboard') }}">
		  		<i class="fa fa-home" aria-hidden="true"></i>
		  	 	Bảng điểu khiển
		  	</a>
		  </li>
		  <li>
		  	Vận chuyển
		  </li>
		</ul>
		
		<div class="row">
			<div class="col-md-12 dataTable">
				<div class="box">
					<table id="table" class="table table-hover">
						<thead>
				            <tr>
				                <th>ID</th>
				                <th>Tên</th>
				                <th class="action">Tác vụ</th>
				            </tr>
				        </thead>
				        <tbody>
				            <tr>
				                <td class="vertical">1</td>
				                <td>Đã thanh toán</td>
				                <td class="vertical">
				                	<a href="{{ route('shipper.edit', 1) }}" class="btn btn-sm waves-effect waves-light btn-warning">Sửa</a>
				                	<a class="btn btn-sm btn-outline-danger btn-delete">Xóa</a>
				                </td>
				            </tr>
				        </tbody>
					</table>
				</div>
			</div>
		</div>
	</section>
@stop()