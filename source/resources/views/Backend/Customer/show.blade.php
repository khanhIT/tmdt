@extends('Backend.Layouts.master')

@section('title')
	<title>Khách hàng | LuzBakery</title>
@stop()

@section('styleAdd')
	<style>
		.p-d-15 h5{
			font-weight: 700
		}
	</style>
@stop()

@section('content')
	<section class="content">
        <ul class="breadcrumb">
		  <li>
		  	<a href="{{ route('dashboard') }}">
		  		<i class="fa fa-home" aria-hidden="true"></i>
		  	 	Bảng điểu khiển
		  	</a>
		  </li>
		  <li>
		  	<a href="{{ route('customer.index') }}">Khách hàng</a>
		  </li>
		  <li>Chi tiết khách hàng</li>
		</ul>
		<div class="row">
			<div class="col-md-8">
				<div class="box p-d-15">
					<h5>Thông tin khách hàng</h5>
					<hr>
					<div>
						<p>Tên khách hàng : {{ $customer->first_name }} {{ $customer->last_name }}</p>
						<p>Email : {{ $customer->email }}</p>
						<p>Số điện thoại : {{ $customer->phone }}</p>
						<p>Địa chỉ : {{ $customer->province_name }}</p>
						<p></p>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="box p-d-15">
					<h5>Xuất bản</h5>
					<hr>
					<a href="{{ route('customer.index') }}" class="btn btn-sm btn-info"><i class="fa fa-save"></i> Lưu</a>
				</div>
			</div>
		</div>
	</section>
@stop()