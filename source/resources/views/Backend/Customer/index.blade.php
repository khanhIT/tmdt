@extends('Backend.Layouts.master')

@section('title')
	<title>Khách hàng | LuzBakery</title>
@stop()

@section('content')
	<section class="content">
        <ul class="breadcrumb">
		  <li>
		  	<a href="{{ route('dashboard') }}">
		  		<i class="fa fa-home" aria-hidden="true"></i>
		  	 	@lang('all.dashboard')
		  	</a>
		  </li>
		  <li>
		  		@lang('all.customer')
		  </li>
		</ul>
		<div class="row">
			<div class="col-md-12 dataTable">
				<div class="box">
					<table id="table" class="table table-hover">
						<thead>
				            <tr>
				                <th>ID</th>
				                <th>Tên</th>
				                <th>Số điện thoại</th>
				                <th>Email</th>
				                <th>Thành phố</th>
				                <th class="action">Tác vụ</th>
				            </tr>
				        </thead>
				        <tbody>
						@php $i = 1; @endphp
						@foreach($customer as $cus)
				            <tr>
				                <td>{{ $i }}</td>
				                <td>
				                	<a href="{{ route('customer.show', $cus->id) }}">{{ $cus->first_name }} {{ $cus->last_name }}</a>
				                </td>
				                <td>{{ $cus->phone }}</td>
				                <td>{{ $cus->email }}</td>
				                <td>{{ $cus->province_name }}</td>
				                <td>
				                	<button class="btn btn-sm btn-info">
				                		<i class="fa fa-check"></i> 
										@if($cus->status == 20)
											Actived
										@else 
										    Unactive	
										@endif
				                	</button>
				                </td>
				            </tr>
						@php $i++; @endphp	
						@endforeach	
				        </tbody>
					</table>
				</div>
			</div>
		</div>
	</section>
@stop()