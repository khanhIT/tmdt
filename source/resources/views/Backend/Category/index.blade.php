@extends('Backend.Layouts.master')

@section('title')
	<title>Danh mục sản phẩm | LuzBakery</title>
@stop()

@section('content')
	<section class="content">
        <ul class="breadcrumb">
		  <li>
		  	<a href="{{ route('dashboard') }}">
		  		<i class="fa fa-home" aria-hidden="true"></i>
		  	 	Bảng điểu khiển
		  	</a>
		  </li>
		  <li>
		  	Danh mục
		  </li>
		</ul>

		<div class="pull-right" style="margin-top: -50px">
			<a href="{{ route('category.create') }}" class="btn btn-block btn-success">
				<i class="fa fa-plus"></i>
				Thêm mới
			</a>
		</div>
		
		@include('Backend.Errors.sessionSuccess')

		<div class="row">
			<div class="col-md-12 dataTable">
				<div class="box">
					@include('Backend.Category.Component.list.listCategoryItem')
				</div>
			</div>
		</div>
	</section>
@stop()
