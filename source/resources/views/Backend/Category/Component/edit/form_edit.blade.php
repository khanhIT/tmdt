<div class="form-group {{ !empty($errors->first('category_name')) ? 'has-warning' : '' }}">
    <label>Tên danh mục <span class="star">*</span></label>
    <input type="text" name="category_name" class="form-control" placeholder="Tên danh mục" value="{{ old('category_name', $category->name) }}">
    <span class="help-block">{{ !empty($errors->first('category_name')) ? $errors->first('category_name') : '' }}</span>
</div>