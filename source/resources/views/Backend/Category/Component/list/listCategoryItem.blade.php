<table id="table" class="table table-hover">
	<thead>
        <tr>
            <th class="action">ID</th>
            <th>Tên danh mục</th>
            <th>Ngày tạo</th>
            <th class="action">Tác vụ</th>
        </tr>
    </thead>
    <tbody>
    	@foreach($category as $listCategory)
        <tr>
            <td class="vertical">{{ $listCategory->id }}</td>
            <td class="vertical">
            	<a href="{{ route('category.edit', $listCategory->id) }}">
            		{{ $listCategory->name }}
            	</a>
            </td>
            <td class="vertical">
            	{{\Carbon\Carbon::parse($listCategory->created_at)->format('Y/m/d H:i')  }}
            </td>
            <td class="vertical">
            	<a href="{{ route('category.edit', $listCategory->id) }}" class="btn btn-sm waves-effect waves-light btn-warning">Sửa</a>
            	&nbsp;
            	<a class="btn btn-sm btn-outline-danger btn-delete" data-id="{{ $listCategory->id }}" data-url="{{ route('category.destroy', $listCategory->id) }}" data-url-reload="{{ route('category.index', $listCategory->id) }}">Xóa</a>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>