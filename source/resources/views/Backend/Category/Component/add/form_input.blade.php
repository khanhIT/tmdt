<div class="form-group {{ !empty($errors->first('category_name')) ? 'has-warning' : '' }}">
    <label>Tên danh mục <span class="star">*</span></label>
    <input type="text" name="category_name" class="form-control" placeholder="Tên danh mục">
    <span class="help-block">{{ !empty($errors->first('category_name')) ? $errors->first('category_name') : '' }}</span>
</div>