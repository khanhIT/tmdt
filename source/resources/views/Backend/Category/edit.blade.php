@extends('Backend.Layouts.master')

@section('title')
	<title>Sửa danh mục | LuzBakery</title>
@stop()

@section('content')
	<section class="content">
        <ul class="breadcrumb">
		  <li>
		  	<a href="{{ route('dashboard') }}">
		  		<i class="fa fa-home" aria-hidden="true"></i>
		  	 	Bảng điểu khiển
		  	</a>
		  </li>
		  <li>
		  	<a href="{{ route('category.index') }}">Danh mục</a>
		  </li>
		  <li>
		  	Sửa danh mục
		  </li>
		</ul>
		<div class="pull-right" style="margin-top: -50px">
			<a href="{{ route('category.index') }}" class="btn btn-block btn-success">
				<i class="fa fa-list"></i>
				Danh sách
			</a>
		</div>
		@if(!empty($category))
		<div class="row">
            <div class="col-md-12">
            	<div class="box">
               		<form action="{{ route('category.update', $category->id) }}" method="post" role="form" enctype="multipart/form-data" style="padding: 20px">
               			@csrf
               			@method('PUT')
               			
               			@include('Backend.Category.Component.edit.form_edit')

					    <button type="submit" class="btn waves-effect waves-light btn-rounded btn-outline-success">Cập nhật</button>
					    &nbsp;
					    <button type="reset" class="btn waves-effect waves-light btn-rounded btn-outline-danger">Hủy</button>
               		</form>
            	</div>
            </div>
        </div>
        @endif
	</section>
@stop()
