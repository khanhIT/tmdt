@extends('Backend.Layouts.master')

@section('title')
	<title>Thêm mới danh mục | LuzBakery</title>
@stop()

@section('content')
	<section class="content">
        <ul class="breadcrumb">
		  <li>
		  	<a href="{{ route('dashboard') }}">
		  		<i class="fa fa-home" aria-hidden="true"></i>
		  	 	Bảng điểu khiển
		  	</a>
		  </li>
		  <li>
		  	<a href="{{ route('category.index') }}">Danh mục</a>
		  </li>
		  <li>
		  	Thêm mới danh mục
		  </li>
		</ul>
		<div class="pull-right" style="margin-top: -50px">
			<a href="{{ route('category.index') }}" class="btn btn-block btn-success">
				<i class="fa fa-list"></i>
				Danh sách
			</a>
		</div>
		<div class="row">
            <div class="col-md-12">
            	<div class="box">
               		<form action="{{ route('category.store') }}" method="post" enctype="multipart/form-data" style="padding: 20px">
               			@csrf

               			@include('Backend.Category.Component.add.form_input')
               			
					    <button type="submit" class="btn waves-effect waves-light btn-rounded btn-outline-success">Lưu</button>
					    &nbsp;
					    <button type="reset" class="btn waves-effect waves-light btn-rounded btn-outline-danger">Hủy</button>
               		</form>
            	</div>
            </div>
        </div>
	</section>
@stop()
