<div style="width: 100%; float: left; font-family: arial; background: #eee; ">
    <div style="width: 500px; margin:auto; box-shadow: 5px 5px 3px 4px;">
        <h2>
            <a href="#">
                <img style="margin-left: 170px; float: none; background: #eee; margin-top: 17px"
                     src="https://bizweb.dktcdn.net/100/312/791/themes/689770/assets/logo.png?1545363339913">
            </a>
        </h2>
        <div style=" font-family: Arial,Verdana,sans-serif;width: 100%; float: left; background: #fff;padding: 10px 19px; margin-bottom:30px; border-radius: 0px 0px 5px 5px; border-top: 2px #bc002d solid">

            <h1 style="font-size: 18px; width: 100%; float: left; text-align: center; line-height: 30px">
                Tài khoản đăng nhập admin của bạn trên LuzBakery
            </h1>
            <p><em>Bạn vui lòng active tài khoản</em></p>
            <table style="width: 100%; float: left; background:#b6ecff; font-family: Arial,Verdana,sans-serif; padding:30px; line-height: 40px; border-radius: 5px">
                <tr>
                    <td style="border-bottom: 1px dashed #a2cede; width: 35%">Tên đăng nhập:</td>
                </tr>
                <tr>
                    <td style="border-bottom: 1px dashed #a2cede;">
                        {{$email}}
                    </td>
                </tr>
                <tr>
                    <td style="border-bottom: 1px dashed #a2cede; width: 35%">Click link để active:</td>
                </tr>
                <tr>
                    <td style="border-bottom: 1px dashed #a2cede;">
                        <a href="{{$url_active_user}}">{{$token}}</a>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>
