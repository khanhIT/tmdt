@extends('Backend.Layouts.master')

@section('title')
	<title>Thêm mới người dùng hệ thống | LuzBakery</title>
@stop()

@section('content')
	<section class="content">
        <ul class="breadcrumb">
		  <li>
		  	<a href="{{ route('dashboard') }}">
		  		<i class="fa fa-home" aria-hidden="true"></i>
		  	 	Bảng điểu khiển
		  	</a>
		  </li>
		  <li>
		  	Quản trị hệ thống
		  </li>
		  <li>
		  	<a href="{{ route('user.index') }}">Quản lý người dùng hệ thống</a>
		  </li>
		  <li>
		  	Thêm mới người dùng hệ thống
		  </li>
		</ul>
		<div class="pull-right" style="margin-top: -50px">
			<a href="{{ route('user.index') }}" class="btn btn-block btn-success">
				<i class="fa fa-list"></i>
				Danh sách
			</a>
		</div>
		<div class="row">
            <div class="col-md-12">
            	<div class="box">
               		<form action="{{ route('user.store') }}" method="post" enctype="multipart/form-data" autocomplete="off" style="padding: 20px">
               			@csrf

               			@include('Backend.User.Component.add.add_user')
               			
					    <div class="text-align">
					    	<button type="submit" class="btn waves-effect waves-light btn-rounded btn-outline-success">
					    	   <i class="fa fa-save"></i> Lưu
						    </button>
						    &nbsp;
						    <button type="reset" class="btn waves-effect waves-light btn-rounded btn-outline-danger">
						    	<i class="fa fa-ban"></i> Hủy
						    </button>
					    </div>
               		</form>
            	</div>
            </div>
        </div>
	</section>
@stop()
