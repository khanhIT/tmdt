<div class="">
    <div class="col-md-8">
        <div class="row">
            <div class="row">
                <div class="form-group col-md-6 {{ !empty($errors->first('name')) ? 'has-warning' : '' }}">
                    <label>Họ tên <span class="star">*</span></label>
                    <input type="text" autofocus name="name" class="form-control" value="{{ old('name') }}" placeholder="Nhập họ tên">
                    <span class="help-block">{{ !empty($errors->first('name')) ? $errors->first('name') : '' }}</span>
                </div>

                <div class="form-group col-md-6 {{ !empty($errors->first('email')) ? 'has-warning' : '' }}">
                    <label>Email <span class="star">*</span></label>
                    <input type="email" name="email" class="form-control" value="{{ old('email') }}" placeholder="Nhập email" autocomplete="off">
                    <span class="help-block">{{ !empty($errors->first('email')) ? $errors->first('email') : '' }}</span>
                </div>

                <div class="form-group col-md-6">
                    <label>Số điện thoại</label>
                    <input type="number" name="phone" class="form-control" value="{{ old('phone') }}" placeholder="Nhập số điện thoại">
                </div>

                <div class="form-group col-md-6">
                    <label>Địa chỉ</label>
                    <input type="text" name="address" class="form-control" value="{{ old('address') }}" placeholder="Nhập địa chỉ">
                </div>

                <div class="form-group col-md-6">
                    <label>Ngày sinh</label>
                    <input type="date" name="birthday" class="form-control" value="{{ old('birthday') }}" placeholder="Y/m/d">
                </div>

                <div class="form-group col-md-6 {{ !empty($errors->first('password')) ? 'has-warning' : '' }}">
                    <label>Mật khẩu <span class="star">*</span></label>
                    <input type="password" name="password" class="form-control" value="{{ old('birthday') }}" placeholder="" autocomplete="new-password">
                    <span class="help-block">{{ !empty($errors->first('password')) ? $errors->first('password') : '' }}</span>
                </div>

                <div class="form-group col-md-12 {{ !empty($errors->first('role_id')) ? 'has-warning' : '' }}">
                    <label>Phân quyền <span class="star">*</span></label>
                    <select name="role_id" class="form-control">
                        <option>-- Chọn quyền --</option>
                        @foreach($role as $roleItem)
                        <option value="{{ $roleItem->id }}">{{ $roleItem->name }}</option>
                        @endforeach()
                    </select>
                    <span class="help-block">{{ !empty($errors->first('role_id')) ? $errors->first('role_id') : '' }}</span>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-4">
        <div class="">
            <div class="form-group">
                <label>Hình ảnh </label>
                <input type="file" accept="image/*" name="avatar" id="avatar" class="form-control" placeholder="" onchange="readURL(this)"> 
                <div class="item-image">
                    <img id="preview-image" src="{{ asset('backend/image/bg.jpg') }}">
                </div>
            </div>
        </div>
    </div>
</div>