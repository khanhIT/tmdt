<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
  <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="shortcut icon" type="image/x-icon" href="{{ URL::to('/') }}/frontend/images/favicon.png">
      <link rel="stylesheet" href="{{ asset('frontend/lib/bootstrap/css/bootstrap.min.css') }}">

      <!-- CSRF Token -->
      <meta name="csrf-token" content="{{ csrf_token() }}">

      <title>Login</title>

      <!-- Fonts -->
      <link rel="dns-prefetch" href="https://fonts.gstatic.com">
      <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

      <!-- Styles -->
      <link href="{{ asset('backend/css/login.css') }}" rel="stylesheet">
      <!-- script -->
      <script src="{{ asset('frontend/lib/jquery/jquery.js') }}"></script>
      <script src="{{ asset('backend/js/bootstrap.min.js') }}"></script>
  </head>
  <body>
    <div class="cont">
      <div class="demo">
        <div class="login">
          <div class="login__check"></div>
            <form action="" method="post">
              @csrf()
              <div class="login__form">
                @if(session('error'))
                    <div class="alert alert-warning alert-dismissible" style="font-size: 10px">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4><i class="icon fa fa-warning"></i> Oops!</h4>
                        {{ session('error') }}
                    </div>
                @endif
                <div class="login__row">
                  <svg class="login__icon pass svg-icon" viewBox="0 0 20 20">
                    <path d="M0,20 20,20 20,8 0,8z M10,13 10,16z M4,8 a6,8 0 0,1 12,0" />
                  </svg>
                  <input type="password" name="password" id="txtPassword" class="login__input name" value="{{ old('password') }}" placeholder="Nhập mật khẩu"/>
                </div>
                <div class="error">
                	<span class=" help-block check-password text-yellow"></span>
                </div>
                <button type="submit" class="login__submit">Cập nhật</button>
              </div>
            </form>
        </div>
      </div>
    </div>
  </body>
</html>
