<div class="form-group col-md-6">
    <label for="">
        Tên đăng nhập
    </label>
    <input type="text" class="form-control" name="name" value="{{ old('name', $detailUser->name) }}" placeholder="Họ tên">
</div>
<div class="form-group col-md-6">
    <label for="">
        Email
    </label>
    <input type="email" class="form-control" name="email" value="{{ old('email', $detailUser->email) }}" placeholder="Email">
</div>
<div class="form-group col-md-6">
    <label for="">
        Số điện thoại
    </label>
    <input type="number" class="form-control" name="phone" value="{{ old('phone', $detailUser->phone) }}">
</div>
<div class="form-group col-md-6">
    <label for="">Địa chỉ</label>
    <input type="text" class="form-control" name="address" value="{{ old('address', $detailUser->address) }}" placeholder="Địa chỉ">
</div>
<div class="form-group col-md-6">
    <label for="">Ngày sinh</label>
    <input type="date" class="form-control" name="birthday" value="{{ old('birthday', $detailUser->birthday) }}">
</div>
<div class="form-group col-md-6">
    <label>Phân quyền</label>
    <select name="role_id" class="form-control">
        <option>-- Chọn quyền --</option>
        @foreach($role as $roleItem)
        <option value="{{ $roleItem->id }}" {!! $roleItem->id == old('role_id',$detailUser->role_id)  ? "selected" : "" !!}>{{ $roleItem->name }}</option>
        @endforeach()
    </select>
</div>