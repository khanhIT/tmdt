<table id="table" class="table table-hover">
	<thead>
        <tr>
            <th>ID</th>
            <th>Tên đăng nhập</th>
            <th>Email</th>
            <th>Phân quyền</th>
            <th>Ngày tạo</th>
            <th>Kích hoạt</th>
            <th class="action">Tác vụ</th>
        </tr>
    </thead>
    @if(!empty($user))
		  @php $i=1; @endphp
	    @foreach($user as $userItem)
		    <tbody>
		        <tr>
		            <td class="vertical">{{ $i }}</td>
		            <td class="vertical">
		            	<a href="{{ route('user.show', $userItem->id) }}">{{
		            		$userItem->name }}</a>
		            </td>
		            <td class="vertical">{{ $userItem->email }}</td>
		            <td class="vertical">
						<buton class="btn btn-sm {{ $userItem->role_id == 1 ? 'btn-outline-success' : 'btn-outline-danger' }}">
							{{ $userItem->role_name }}
						</buton>
					</td>
		            <td class="vertical">{{\Carbon\Carbon::parse($userItem->created_at)->format('Y/m/d H:i')  }}</td>
		            <td class="vertical">
		            	<button class="btn btn-sm btn-outline-success">
		            		{{ $userItem->active == 1 ? 'Kích hoạt' : 'Chưa kích hoạt' }}
		            	</button>
		            </td>
		            <td class="vertical">
		            	<a href="{{ route('user.show', $userItem->id) }}"class="btn btn-sm waves-effect waves-light btn-rounded btn-outline-primary">Xem</a>
		            	<a href="{{ route('user.edit', $userItem->id) }}" class="btn btn-sm waves-effect waves-light btn-warning">Sửa</a>
		            	<a href="javascript:void(0)" class="btn btn-sm btn-outline-danger btn-delete"  data-url="{{ route('user.destroy', $userItem->id) }}" data-url-reload="{{ route('user.index', $userItem->id) }}">Xóa</a>
		            </td>
		        </tr>
		    </tbody>
				@php $i++; @endphp
	    @endforeach()
    @endif()
</table>
