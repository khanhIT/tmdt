<form action="{{ route('user.update', $user->id) }}" method="post" role="form" enctype="multipart/form-data">
    @csrf
    @method('PUT')
    <div class="form-group col-md-6 {{ !empty($errors->first('name')) ? 'has-warning' : '' }}">
        <label for="">
            Tên đăng nhập <span class="star">*</span>
        </label>
        <input type="text" class="form-control" name="name" value="{{ old('name', $user->name) }}" placeholder="Họ tên">
        <span class="help-block">{{ !empty($errors->first('name')) ? $errors->first('name') : '' }}</span>
    </div>
    <div class="form-group col-md-6 {{ !empty($errors->first('email')) ? 'has-warning' : '' }}">
        <label for="">
            Email <span class="star">*</span>
        </label>
        <input type="email" class="form-control" name="email" value="{{ old('email', $user->email) }}" placeholder="Email">
         <span class="help-block">{{ !empty($errors->first('email')) ? $errors->first('email') : '' }}</span>
    </div>
    <div class="form-group col-md-6">
        <label for="">
            Số điện thoại
        </label>
        <input type="number" class="form-control" name="phone" value="{{ old('phone', $user->phone) }}">
    </div>
    <div class="form-group col-md-6">
        <label for="">Địa chỉ</label>
        <input type="text" class="form-control" name="address" value="{{ old('address', $user->address) }}" placeholder="Địa chỉ">
    </div>
    <div class="form-group col-md-6">
        <label for="">Ngày sinh</label>
        <input type="date" class="form-control" name="birthday" value="{{ old('birthday', $user->birthday) }}">
    </div>
    <div class="form-group col-md-6 {{ !empty($errors->first('role_id')) ? 'has-warning' : '' }}">
        <label>Phân quyền <span class="star">*</span></label>
        <select name="role_id" class="form-control">
            <option>-- Chọn quyền --</option>
            @foreach($role as $roleItem)
            <option value="{{ $roleItem->id }}" {!! $roleItem->id == old('role_id',$user->role_id)  ? "selected" : "" !!}>{{ $roleItem->name }}</option>
            @endforeach()
        </select>
        <span class="help-block">{{ !empty($errors->first('role_id')) ? $errors->first('role_id') : '' }}</span>
    </div>
    <div class="form-group text-center">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-outline-success">
                <i class="fa fa-check"></i>
                Cập nhật
            </button>
        </div>
    </div>
</form>