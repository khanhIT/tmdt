<form method="post" action="{{ route('user.changePass', $user->id) }}" accept-charset="UTF-8" class="form-horizontal">
    @csrf
    <div class="form-group col-md-8 {{ !empty($errors->first('password')) ? 'has-warning' : '' }}">
        <label for="inputName" class="control-label">
            Mật khẩu mới <span class="star">*</span>
        </label>
        <input type="password" name="password" value="{{ old('password') }}" class="form-control" id="txtPassword">
        <span class="help-block">{{ !empty($errors->first('password')) ? $errors->first('password') : '' }}</span>
    </div>
    <div class="form-group col-md-8 {{ !empty($errors->first('confirmPassword')) ? 'has-warning' : ''}}">
        <label for="inputEmail" class="control-label">
            Xác nhận mật khẩu mới <span class="star">*</span>
        </label>
        <input type="password" name="confirmPassword" value="{{ old('confirmPassword') }}" class="form-control" id="txtConfirmPassword">
        <span class="help-block">{{ !empty($errors->first('confirmPassword')) ? $errors->first('confirmPassword') : '' }}</span>
    </div>
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-outline-success">
                <i class="fa fa-check"></i>
                Cập nhật
            </button>
        </div>
    </div>
</form>