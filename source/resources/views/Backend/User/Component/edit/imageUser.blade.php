<div class="box-body box-profile">
    <img class="profile-user-img img-responsive img-circle" src="{{ !empty($user->avatar) ? $user->avatar : asset('frontend/images/man.png') }}" alt="User profile picture" style="width: 150px; height: 150px; border: none">
    <h3 class="profile-username text-center" style="font-weight: 600; font-size: 17px; margin-top: 20px">{{ $user->name }}</h3>
</div>