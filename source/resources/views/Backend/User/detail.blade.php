@extends('Backend.Layouts.master')

@section('title')
    <title>Chi tiết người dùng | LuzBakery</title>
@stop()

@section('content')
    <section class="content">
        <ul class="breadcrumb">
            <li>
                <a href="{{ route('dashboard') }}">
                    <i class="fa fa-home" aria-hidden="true"></i>
                    Bảng điểu khiển
                </a>
            </li>
            <li>
                Quản trị hệ thống
            </li>
            <li>
                <a href="{{ route('user.index') }}">Quản lý người dùng hệ thống</a>
            </li>
            <li>
                Chi tiết người dùng
            </li>
        </ul>
        <div class="pull-right" style="margin-top: -50px">
            <a href="{{ route('user.index') }}" class="btn btn-block btn-success">
                <i class="fa fa-list"></i>
                Danh sách
            </a>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-3">
                    <div class="box box-primary">
                       @include('Backend.User.Component.detail.imageUser')
                        <!-- /.box-body -->
                    </div>
                </div>
                <div class="col-md-9">
                   <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="nav-tabs-custom">
                                    <ul class="nav nav-tabs">
                                        <li class="active">
                                            <a href="#activity" data-toggle="tab" aria-expanded="true">Thông tin người dùng</a>
                                        </li>
                                    </ul>
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="activity">
                                            <div class="post row">
                                                @include('Backend.User.Component.detail.formInformationUser')
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                   </div>
                </div>
            </div>
        </div>
    </section>
@stop()