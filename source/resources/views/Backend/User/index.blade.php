@extends('Backend.Layouts.master')

@section('title')
	<title>Quản lý người dùng hệ thống | LuzBakery</title>
@stop()

@section('content')
	<section class="content">
        <ul class="breadcrumb">
		  <li>
		  	<a href="{{ route('dashboard') }}">
		  		<i class="fa fa-home" aria-hidden="true"></i>
		  	 	Bảng điểu khiển
		  	</a>
		  </li>
		  <li>
		  	Quản trị hệ thống
		  </li>
		  <li>
		  	Quản lý người dùng hệ thống
		  </li>
		</ul>
		<div class="pull-right" style="margin-top: -50px">
			<a href="{{ route('user.create') }}" class="btn btn-block btn-success">
				<i class="fa fa-plus"></i>
				Thêm mới
			</a>
		</div>
		@include('Backend.Errors.sessionSuccess')
		<div class="row">
			<div class="col-md-12 dataTable">
				<div class="box">
					@include('Backend.User.Component.list.listUserItem')
				</div>
			</div>
		</div>
	</section>
@stop()