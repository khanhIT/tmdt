@extends('Backend.Layouts.master')

@section('title')
	<title>Chi tiết sản phẩm | LuzBakery</title>
@stop()

@section('styleAdd')
	<link rel="stylesheet" href="{{ asset('backend/pages/product/product.css') }}">
@stop()

@section('content')
	<section class="content">
        <ul class="breadcrumb">
		  <li>
		  	<a href="{{ route('dashboard') }}">
		  		<i class="fa fa-home" aria-hidden="true"></i>
		  	 	Bảng điểu khiển
		  	</a>
		  </li>
		  <li>
		  	<a href="{{ route('product.index') }}">Sản phẩm</a>
		  </li>
		  <li>
		  	Chi tiết sản phẩm
		  </li>
		</ul>
		<div class="pull-right" style="margin-top: -50px">
			<a href="{{ route('product.index') }}" class="btn btn-block btn-success">
				<i class="fa fa-list"></i>
				Danh sách
			</a>
		</div>
		<div class="row">
        <div class="col-md-12">
        	<div class="box">
           		<div class="" style="padding: 20px">
           			<h2 class="m-b-0 m-t-0">
           				<font>
           					<font>{{ $productdetail->product_name }}</font>
           				</font>
           			</h2>
           			<hr>
           			<div class="row">
           				<div class="col-md-4">
             				<div class="thumbnail">
             					<img src="{{ $productdetail->image }}" alt="{{ $productdetail->product_name }}">
             				</div>
             			</div>
             			<div class="col-md-8">
             				<h4 class="box-title">Mô tả sản phẩm</h4>
             				<p>{!! substr($productdetail->description, 0, 298) !!} ...</p>
             				<h4 class="m-t-40">
											@if(empty($productdetail->sale_price))
             							<font class="price">{{ number_format($productdetail->price, 2, ',', '.') }} VNĐ</font>
											@else
		             					<font>{{ number_format($productdetail->sale_price, 2, ',', '.') }} VNĐ</font>
		             					<small class="text-success">
		             							<font class="price">( {{ number_format($productdetail->price, 2, ',', '.') }} VNĐ )</font>
		             					</small>
											@endif
             				</h4>
             			</div>
             			@include('Backend.Product.Component.detail.inforGenerate')
           			</div>
           		</div>
        	</div>
        </div>
    </div>
	</section>
@stop()
