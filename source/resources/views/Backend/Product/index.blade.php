@extends('Backend.Layouts.master')

@section('title')
	<title>Sản phẩm | LuzBakery</title>
@stop()

@section('content')
	<section class="content">
        <ul class="breadcrumb">
		  <li>
		  	<a href="{{ route('dashboard') }}">
		  		<i class="fa fa-home" aria-hidden="true"></i>
					@lang('all.dashboard')
		  	</a>
		  </li>
		  <li>
			@lang('all.product')
		  </li>
		</ul>
		<div class="pull-right" style="margin-top: -50px">
			<a href="{{ route('product.create') }}" class="btn btn-block btn-success">
				<i class="fa fa-plus"></i>
				@lang('all.add')
			</a>
		</div>
		<div class="pull-right" style="margin-top: -50px; margin-right: 120px">
			<a href="{{ route('product.export') }}" class="btn btn-block btn-success">
				<i class="fa fa-download"></i>
				@lang('all.export')
			</a>
		</div>
		@include('Backend.Errors.sessionSuccess')
		<div class="row">
			<div class="col-md-12 dataTable">
				<div class="box">
					<table id="table" class="table table-hover">
						@include('Backend.Product.Component.list.listIndex')
					</table>
					<!-- paginate -->
			    <div class="text-align">
			      {{ $product->links() }}
			    </div>
				</div>
			</div>
		</div>
	</section>
@stop()
