<thead>
    <tr>
        <th>ID</th>
        <th>Tên sản phẩm</th>
        <th>Hình ảnh</th>
        <th>Giá sản phẩm</th>
        <th>Giá khuyến mại</th>
        <th>Số lượng</th>
        <th>Danh mục</th>
        <th class="action">Tác vụ</th>
    </tr>
</thead>
<tbody>
    @if(!empty($product))
    @php $i=1; @endphp
    @foreach($product as $pro)
    <tr>
        <td class="vertical">{{ $i }}</td>
        <td class="vertical">
          <a href="{{ route('product.show', $pro->id) }}">{{ $pro->product_name }}</a>
        </td>
        <td>
          <div class="product_image">
            <img src="{{ $pro->image }}" alt="{{ $pro->product_name }}">
          </div>
        </td>
        @if(!empty($pro->sale_price))
            <td class="vertical" style="text-decoration: line-through">{{ number_format($pro->price, 2, ',', '.') }} VNĐ</td>
        @else
            <td class="vertical">{{ number_format($pro->price, 2, ',', '.') }} VNĐ</td>
        @endif
        @if(!empty($pro->sale_price))
            <td class="vertical">{{ number_format($pro->sale_price, 2, ',', '.') }} VNĐ</td>
        @else
            <td class="vertical">0 VNĐ</td>
        @endif
        <td class="vertical">{{ $pro->qty }}</td>
        <td class="vertical">
          <button class="btn btn-sm waves-effect waves-light btn-rounded btn-outline-primary">{{ $pro->name }}</button>
        </td>
        <td class="vertical">
          <a href="{{ route('product.edit', $pro->id) }}" class="btn btn-sm waves-effect waves-light btn-warning">@lang('all.edit')</a>
          <a href="javascript:void(0)" class="btn btn-sm btn-outline-danger btn-delete"  data-url="{{ route('product.destroy', $pro->id) }}" data-url-reload="{{ route('product.index', $pro->id) }}">@lang('all.delete')</a>
        </td>
    </tr>
    @php $i++; @endphp
    @endforeach()
    @endif
</tbody>
