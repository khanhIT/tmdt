<div class="col-lg-12 col-md-12 col-sm-12">
  <h4 class="box-title m-t-40">Thông tin chung</h4>
  <div class="table-responsive">
     <table class="table table-hover">
        <tbody>
          <tr>
            <td class="w-350">Danh mục</td>
            <td>
              <button class="btn btn-sm waves-effect waves-light btn-rounded btn-outline-info">{{ $productdetail->name }}</button>
            </td>
          </tr>
          <tr>
            <td class="w-350">Sản phẩm bán chạy</td>
            <td>
              @if($productdetail->hot == 1)
              <span>Có</span>
              @else
              <span>Không</span>
              @endif
            </td>
          </tr>
          <tr>
            <td class="w-350">Sản phẩm nổi bật</td>
            <td>
              @if($productdetail->status == 1)
              <span>Có</span>
              @else
              <span>Không</span>
              @endif
            </td>
          </tr>
          <tr>
            <td class="w-350">Mô tả chi tiết</td>
            <td>
              <p>{!! $productdetail->description !!}</p>
            </td>
          </tr>
        </tbody>
      </table>
  </div>
</div>
