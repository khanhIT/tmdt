<div class="box">
	<div style="padding: 20px">
		<div class="form-group row {{ !empty($errors->first('product_name')) ? 'has-warning' : '' }}">
	    <label class="col-sm-3">Tên sản phẩm <span class="star">*</span></label>
	    <div class="col-sm-9">
	    	<input type="text" class="form-control" name="product_name" autofocus placeholder="Tên sản phẩm" value="{{ old('product_name') }}">
	    </div>
	    <span class="help-block text-center">{{ !empty($errors->first('product_name')) ? $errors->first('product_name') : '' }}</span>
    </div>

    <div class="form-group row {{ !empty($errors->first('category_id')) ? 'has-warning' : '' }}">
	    <label class="col-sm-3">Danh mục sản phẩm <span class="star">*</span></label>
	    <div class="col-sm-9">
	    	<select class="form-control category" name="category_id">
	    		<option value="">Chọn danh mục</option>
	    		@foreach($category as $cate)
		    		<option value="{{ $cate->id }}" {{ $cate->id == old('category_id') ? "selected" : "" }}>{{ $cate->name }}</option>
		    	@endforeach
		    </select>
	    </div>
	    <span class="help-block text-center">{{ !empty($errors->first('category_id')) ? $errors->first('category_id') : '' }}</span>
    </div>

    <div class="form-group row {{ !empty($errors->first('price')) ? 'has-warning' : '' }}">
	    <label class="col-sm-3">Giá sản phẩm <span class="star">*</span></label>
	    <div class="col-sm-9">
	    	<input type="number" min='1000' name="price" class="form-control" placeholder="Giá sản phẩm" value="{{ old('price') }}">
	    </div>
	    <span class="help-block text-center">{{ !empty($errors->first('price')) ? $errors->first('price') : '' }}</span>
    </div>

    <div class="form-group row">
	    <label class="col-sm-3">Giá sản phẩm khuyến mại</label>
	    <div class="col-sm-9">
	    	<input type="number" min='0' name="sale_price" class="form-control" placeholder="Giá sản phẩm khuyến mại" value="{{ old('sale_price') }}">
	    </div>
    </div>

		<div class="form-group row {{ !empty($errors->first('qty')) ? 'has-warning' : '' }}">
	    <label class="col-sm-3">Số lượng <span class="star">*</span></label>
	    <div class="col-sm-9">
	    	<input type="number" min='0' name="qty" class="form-control" placeholder="Số lượng" value="{{ old('qty') }}">
	    </div>
			<span class="help-block text-center">{{ !empty($errors->first('qty')) ? $errors->first('qty') : '' }}</span>
    </div>

    <div class="form-group row">
	    <label class="col-sm-3">Mô tả</label>
	    <div class="col-sm-9">
	    	<textarea class="form-control" id="editor" rows="8" name="description">{{ old('description') }}</textarea>
	    </div>
		</div>

		<div class="form-group row {{ !empty($errors->first('image')) ? 'has-warning' : '' }}">
	    <label class="col-sm-3">Hình ảnh sản phẩm <span class="star">*</span></label>
			<div class="col-sm-9">
				<input type="file" name="image" id="avatar" class="form-control" placeholder="" onchange="readURL(this)">
				<div class="item-image">
						<img id="preview-image" src="{{ asset('backend/image/bg.jpg') }}">
				</div>
			</div>
	    <span class="help-block text-center">{{ !empty($errors->first('image')) ? $errors->first('image') : '' }}</span>
    </div>
	</div>
</div>
