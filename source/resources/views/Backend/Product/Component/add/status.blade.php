<div class="box">
	<div style="padding: 20px">
		<div class="title">
			<h5>
				<label>Trạng thái</label>
			</h5>
		</div><hr>
		<div class="btn-set">
			<select name="status" class="form-control">
				<option value="">-- Chọn trạng thái --</option>
				<option value="1" @if (old('status') == '1') {{ 'selected' }} @endif>Kích hoạt</option>
				<option value="0" @if (old('status') == '0') {{ 'selected' }} @endif>Hủy bỏ</option>
			</select>
		</div>
	</div>
</div>
