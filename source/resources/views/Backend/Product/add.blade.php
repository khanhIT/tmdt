@extends('Backend.Layouts.master')

@section('title')
	<title>Thêm mới sản phẩm | LuzBakery</title>
@stop()

@section('styleAdd')
	<style>
		.ck-editor__editable {
			height: 200px
		}
	</style>
	<link rel="stylesheet" href="{{ asset('backend/pages/product/product.css') }}">
@stop()

@section('content')
	<section class="content">
      <ul class="breadcrumb">
				<li>
					<a href="{{ route('dashboard') }}">
						<i class="fa fa-home" aria-hidden="true"></i>
						@lang('all.dashboard')
					</a>
				</li>
				<li>
					<a href="{{ route('product.index') }}">@lang('all.product')</a>
				</li>
				<li>
					@lang('all.add')
			</ul>
			<div class="pull-right" style="margin-top: -50px">
				<a href="{{ route('product.index') }}" class="btn btn-block btn-success">
					<i class="fa fa-list"></i>
					Danh sách
				</a>
			</div>
			@if(session('error'))
					<div class="col-md-12 alert alert-warning alert-dismissible" style="font-size: 10px">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
							<h4><i class="icon fa fa-warning"></i> Oops!</h4>
							{{ session('error') }}
					</div>
			@endif
			<div class="row">
					<div class="col-md-12">
						<form action="{{ route('product.store') }}" method="post" autocomplete="off" enctype="multipart/form-data">
							@csrf
							<div class="col-sm-9">
								@include('Backend.Product.Component.add.formAddProduct')
							</div>
							<div class="col-sm-3">
								@include('Backend.Product.Component.add.action')
							</div>
							<div class="col-sm-3">
								@include('Backend.Product.Component.add.status')
							</div>
							<div class="col-sm-3">
								@include('Backend.Product.Component.add.hotNew')
							</div>
						</form>
					</div>
			</div>
	</section>
@stop()
@section('scriptAdd')
	<script src="https://cdn.ckeditor.com/ckeditor5/11.0.1/classic/ckeditor.js"></script>
	<script src="{{ asset('backend/pages/product/product.js') }}"></script>
	<script>
		 ClassicEditor
            .create( document.querySelector( '#editor' ) )
            .catch( error => {
                console.error( error );
            } );
	</script>
@stop()
