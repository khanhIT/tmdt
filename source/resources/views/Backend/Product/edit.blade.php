@extends('Backend.Layouts.master')

@section('title')
	<title>Sửa sản phẩm | LuzBakery</title>
@stop()

@section('content')
	<section class="content">
        <ul class="breadcrumb">
		  <li>
		  	<a href="{{ route('dashboard') }}">
		  		<i class="fa fa-home" aria-hidden="true"></i>
		  	 	Bảng điểu khiển
		  	</a>
		  </li>
		  <li>
		  	<a href="{{ route('product.index') }}">Sản phẩm</a>
		  </li>
		  <li>
		  	Sửa sản phẩm
		  </li>
		</ul>
		<div class="pull-right" style="margin-top: -50px">
			<a href="{{ route('product.index') }}" class="btn btn-block btn-success">
				<i class="fa fa-list"></i>
				Danh sách
			</a>
		</div>
		<div class="row">
			<div class="col-md-12">
				<form action="{{ route('product.update', $productEdit->id) }}" method="post" autocomplete="off" enctype="multipart/form-data">
					@csrf
					@method('PUT')
					<div class="col-sm-9">
						@include('Backend.Product.Component.edit.formEditProduct')
					</div>
					<div class="col-sm-3">
						@include('Backend.Product.Component.edit.action')
					</div>
					<div class="col-sm-3">
						@include('Backend.Product.Component.edit.status')
					</div>
					<div class="col-sm-3">
						@include('Backend.Product.Component.edit.hotNew')
					</div>
				</form>
			</div>
    </div>
	</section>
@stop()
@section('scriptAdd')
	<script src="https://cdn.ckeditor.com/ckeditor5/11.0.1/classic/ckeditor.js"></script>
	<script>
		 ClassicEditor
            .create( document.querySelector( '#editor' ) )
            .catch( error => {
                console.error( error );
            } );
	</script>
@stop()
