<header class="main-header">
    <!-- Logo -->
    <a href="{{ route('dashboard') }}" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>L</b>UZ</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Luz</b>Bakery</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="{{ !empty(Auth::user()->avatar) ? asset(Auth::user()->avatar) : asset('frontend/images/man.png') }}" class="user-image" alt="User Image">
              <span class="hidden-xs">{{ Auth::user()->name }}</span>
            </a>
            <ul class="dropdown-menu" style="left: 0">
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="">
                  <a href="#" style="color: #000">
                    <i class="fa fa-user" aria-hidden="true" style="padding-right: 10px;"></i> @lang('all.profile')
                  </a><br><br>
                  <a href="{{ route('logout') }}" style="color: #000">
                    <i class="fa fa-key" aria-hidden="true" style="padding-right: 10px;"></i> @lang('all.logout')
                  </a>
                </div>
              </li>
            </ul>
          </li>
        </ul>
      </div>
      <!-- multiple languages -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <li class="user user-menu">
            <a href="{{ route('lang',['lang' => 'vi']) }}" class="dropdown-toggle">
              <span class="hidden-xs">
                <img src="{{ asset('backend/image/lang/vn.png') }}" alt=""> Tiếng Việt
              </span>
            </a>
          </li>
          <li class="user user-menu">
            <a href="{{ route('lang',['lang' => 'en']) }}" class="dropdown-toggle">
              <span class="hidden-xs">
                <img src="{{ asset('backend/image/lang/en.png') }}" alt=""> English
              </span>
            </a>
          </li>
        </ul>
      </div>
      <!-- end -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <li class="user user-menu">
            <a href="http://luzbakery.local:8080" target="_blank">
              <span class="hidden-xs">
                <i class="fa fa-globe" aria-hidden='true'></i> Trang web
              </span>
            </a>
          </li>
        </ul>
      </div>
    </nav>
</header>
