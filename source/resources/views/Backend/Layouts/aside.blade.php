<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="active">
          <a href="{{ route('dashboard') }}" class="title">
            <i class="fa fa-home" aria-true="hidden"></i>
            <span>@lang('all.dashboard')</span>
          </a>
        </li>

        <li>
          <a href="{{ route('customer.index') }}" class="title">
            <i class="fa fa-user-circle-o" aria-hidden="true"></i>
            <span>@lang('all.customer')</span>
          </a>
        </li>
        <li>
          <a href="{{ route('product.index') }}" class="title">
            <i class="fa fa-product-hunt" aria-hidden="true"></i>
            <span>@lang('all.product')</span>
          </a>
        </li>
        <li>
          <a href="{{ route('category.index') }}" class="title">
            <i class="fa fa-laptop"></i>
            <span>@lang('all.category')</span>
          </a>
        </li>

        <li>
          <a href="{{ route('recipe.index') }}" class="title">
            <i class="fa fa-edit"></i> <span>@lang('all.repice')</span>
          </a>
        </li>
        <li>
          <a href="{{ route('contact.index') }}" class="title">
            <i class="fa fa-envelope-o" aria-hidden="true"></i>
            <span>@lang('all.contact')</span>
          </a>
        </li>

        <li>
          <a href="{{ route('order.index') }}" class="title">
             <i class="fa fa-first-order" aria-hidden="true"></i>
            <span> @lang('all.order')</span>
          </a>
        </li>
        <li class="treeview">
          <a href="#" class="title">
            <i class="fa fa-user" aria-hidden="true"></i>
            <span>@lang('all.user')</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li>
                <a href="{{ route('role.index') }}" class="title">
                     @lang('all.role')
                </a>
            </li>
            <li>
                <a href="{{ route('user.index') }}" class="title">
                  @lang('all.user_manage')
                </a>
            </li>
            <li>
                <a href="{{ route('system.index') }}" class="title">
                     @lang('all.system')
                </a>
            </li>
          </ul>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
</aside>
