<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" type="image/x-icon" href="{{ URL::to('/') }}/frontend/images/favicon.png">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}"/>

        @yield('title')

        @include('Backend.Layouts.mainStyle')
        @yield('styleAdd')

    </head>
    <body class="hold-transition skin-blue sidebar-mini">

        <div class="wrapper">
            @include('Backend.Layouts.header')
            @include('Backend.Layouts.aside')

            <div class="content-wrapper">
                @yield('content')
            </div>

            @include('Backend.Layouts.footer')

        </div>

        @include('Backend.Layouts.mainJS')
        @yield('scriptAdd')
    </body>
</html>
