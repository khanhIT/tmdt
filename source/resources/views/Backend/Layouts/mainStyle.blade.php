<link rel="stylesheet" href="{{ asset('backend/css/bootstrap.min.css') }}">
<link rel="stylesheet" href="{{ asset('backend/lib/dataTable/dataTables.bootstrap.min.css') }}">
<link rel="stylesheet" href="{{ asset('backend/lib/select2/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('backend/css/AdminLTE.min.css') }}">
<link rel="stylesheet" href="{{ asset('backend/css/all-skins.css') }}">
<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
<link rel="stylesheet" href="{{ asset('frontend/font/font.css') }}">
<link rel="stylesheet" href="{{ asset('backend/css/header/header.css') }}">
<link rel="stylesheet" href="{{ asset('backend/css/aside/aside.css') }}">
<link rel="stylesheet" href="{{ asset('backend/pages/common/common.css') }}">
