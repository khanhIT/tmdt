@extends('Backend.Layouts.master')

@section('title')
	<title>Đơn hàng | LuzBakery</title>
@stop()

@section('content')
	<section class="content">
        <ul class="breadcrumb">
		  <li>
		  	<a href="{{ route('dashboard') }}">
		  		<i class="fa fa-home" aria-hidden="true"></i>
		  	 	Bảng điểu khiển
		  	</a>
		  </li>
		  <li>
		  	<a href="{{ route('order.index') }}">Đơn hàng</a>
		  </li>
		  <li>
		  	Chi tiết đơn hàng
		  </li>
		</ul>
		<div class="pad margin no-print">
	      <div class="callout callout-info" style="margin-bottom: 0!important;">
	        <h4><i class="fa fa-info"></i> Note:</h4>
	        Nhấn vào nút in ở cuối hóa đơn để kiểm tra
	      </div>
	    </div>
		<div class="row">
			<div class="col-md-12 dataTable">
				<section class="invoice">
			      <!-- title row -->
			      <div class="row">
			        <div class="col-xs-12">
			          <h2 class="page-header">
			            <i class="fa fa-globe"></i> {{ $detail->first_name }} {{ $detail->last_name }}
			            <small class="pull-right">Date: {{ $detail->created_at }}</small>
			          </h2>
			        </div>
			        <!-- /.col -->
			      </div>
			      <!-- info row -->
			      <div class="row invoice-info">
			        <div class="col-sm-4 invoice-col">
			          Từ
			          <address>
			            <strong>{{ $detail->first_name }} {{ $detail->last_name }}, Inc.</strong><br>
			            {{ $detail->address }}<br>
			            Số điện thoại: {{ $detail->phone }}<br>
			            Email: {{ $detail->email }}
			          </address>
			        </div>
			        <!-- /.col -->
			        <div class="col-sm-4 invoice-col">
			          Tới
			          <address>
			            <strong>{{ $detail->first_name }} {{ $detail->last_name }}</strong><br>
			            {{ $customer->district_name }} {{ $customer->province_name }}<br>
			            Số điện thoại: {{ $detail->phone }}<br>
			            Email: {{ $detail->email }}
			          </address>
			        </div>
			        <!-- /.col -->
			        <div class="col-sm-4 invoice-col">
			          <b>Hóa đơn #OD-00{{$detail->id}}</b><br>
			          <br>
			          <b>ID:</b> {{$detail->id}}<br>
			          <b>Ngày thanh toán:</b>{{$detail->updated_at}}<br>
			        </div>
			        <!-- /.col -->
			      </div>
			      <!-- /.row -->

			      <!-- Table row -->
			      <div class="row">
			        <div class="col-xs-12 table-responsive">
			          <table class="table table-striped">
			            <thead>
							<tr>
								<th>STT</th>
								<th>Tên sản phẩm</th>
								<th>Giá tiền</th>
								<th>Số lượng</th>
								<th>Tổng tiền</th>
							</tr>
			            </thead>
			            <tbody>
							<tr>
								<td>1</td>
								<td>{{ $detail->product_name }}</td>
								<td>{{ number_format(empty($detail->sale_price) ? $detail->price : $detail->sale_price, 3) }} VNĐ</td>
								<td>{{ $detail->qty }}</td>
								<td>{{ $detail->total_mount }} VNĐ</td>
							</tr>
			            </tbody>
			          </table>
			        </div>
			        <!-- /.col -->
			      </div>
			      <!-- /.row -->

			      <div class="row">
			        <!-- accepted payments column -->
			        <div class="col-xs-6">
			          <p class="lead">Phương thức thanh toán:</p>
			          <img src="{{ asset('backend/image/visa.png')}}" alt="Visa">
			        </div>
			        <!-- /.col -->
			        <div class="col-xs-6">
			          <p class="lead">Tổng tiền</p>

			          <div class="table-responsive">
			            <table class="table">
			              <tbody>
			              <tr>
			                <th>Thành tiền:</th>
			                <td>{{ $detail->total_mount }} VNĐ</td>
			              </tr>
			            </tbody></table>
			          </div>
			        </div>
			        <!-- /.col -->
			      </div>
			      <!-- /.row -->

			      <!-- this row will not appear when printing -->
			      <div class="row no-print">
			        <div class="col-xs-12">
			          <a href="javascript:void(0)" target="_blank" class="btn btn-default" onClick="window.print()"><i class="fa fa-print"></i> Print</a>
			        </div>
			      </div>
			    </section>
			</div>
		</div>
	</section>
@stop()
