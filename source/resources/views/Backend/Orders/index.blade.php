@extends('Backend.Layouts.master')

@section('title')
	<title>Đơn hàng | LuzBakery</title>
@stop()

@section('content')
	<section class="content">
        <ul class="breadcrumb">
		  <li>
		  	<a href="{{ route('dashboard') }}">
		  		<i class="fa fa-home" aria-hidden="true"></i>
		  	 	Bảng điểu khiển
		  	</a>
		  </li>
		  <li>
		  	Đơn hàng
		  </li>
		</ul>
		<div class="row">
			<div class="col-md-12 dataTable">
				<div class="box">
					<table id="table" class="table table-hover">
						<thead>
				            <tr>
				                <th>ID</th>
				                <th>Tên sản phẩm</th>
				                <th>Khách hàng</th>
				                <th>Tổng giá</th>
								<th>Trạng thái</th>
				                <th class="action">Tác vụ</th>
				            </tr>
				        </thead>
				        <tbody>
						@php $i=1; @endphp
						@foreach($order as $oder )
				            <tr>
				                <td class="vertical">{{ $i }}</td>
				                <td class="vertical">
				                	<a href="{{ route('order.show', $oder->id) }}">{{ $oder->product_name }}</a>
				                </td>
				                <td class="vertical">
				                	{{ $oder->first_name }} {{ $oder->last_name }}
				                </td>
				                <td class="vertical">
				                	{{ $oder->total_mount }} VNĐ
				                </td>
								<td class="vertical">
								@if($oder->status == 1)
									<button class="btn btn-sm waves-effect waves-light btn-rounded btn-outline-primary">Đã thanh toán</button>
								@else
									<button class="btn btn-sm btn-outline-danger">Pending</button>
								@endif	
								</td>
				                <td class="vertical">
				                	<a href="{{ route('order.show', $oder->id) }}" class="btn btn-sm waves-effect waves-light btn-warning">Xem</a>
				                	<a class="btn btn-sm btn-outline-danger btn-delete">Xóa</a>
				                </td>
				            </tr>
						@php $i++; @endphp	
						@endforeach
				        </tbody>
					</table>
				</div>
			</div>
		</div>
	</section>
@stop()