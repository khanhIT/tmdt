@extends('Backend.Layouts.master')

@section('title')
	<title>404 Not Found | LuzBakery</title>
@stop()

@section('content')
	<section class="content">
        <div class="error-page">
	        <h2 class="headline text-yellow">404</h2>

	        <div class="error-content">
	            <h3><i class="fa fa-warning text-yellow"></i> Oops! Page not found.</h3>
	            <p>
	                Không tìm thấy trang mà bạn yêu cầu. <br />
	                Bạn có muốn quay lại <a href="javascript:history.back()">trang trước</a> không?
	            </p>
	        </div>
	        <!-- /.error-content -->
	    </div>
	    <!-- /.error-page -->
	</section>
@stop()