@extends('Backend.Layouts.master')

@section('title')
	<title>500 Server Error | LuzBakery</title>
@stop()


@section('content')
<section class="content">
    <div class="error-page">
        <h2 class="headline text-yellow">500</h2>

        <div class="error-content">
            <h3><i class="fa fa-warning text-yellow"></i> Oops! Something went wrong.</h3>
            <p>
                Có lỗi xảy ra trong quá trình thực hiện. <br />
                Bạn có muốn quay lại <a href="{{ route('dashboard') }}">trang chủ</a> không?
            </p>
        </div>
        <!-- /.error-content -->
    </div>
    <!-- /.error-page -->
</section>
@stop
