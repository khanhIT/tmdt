@extends('Backend.Layouts.master')

@section('title')
	<title>403 Permission denied | LuzBakery</title>
@stop()

@section('content')
	<section class="content">
        <div class="error-page">
	        <h2 class="headline text-yellow">403</h2>

	        <div class="error-content">
	            <h3><i class="fa fa-warning text-yellow"></i> Oops! Permission denied.</h3>
	            <p>
	                Bạn không có quyền truy cập.<br />
	                Bạn có muốn quay lại <a href="{{ route('dashboard') }}">trang chủ</a> không?
	            </p>
	        </div>
	        <!-- /.error-content -->
	    </div>
	    <!-- /.error-page -->
	</section>
@stop()