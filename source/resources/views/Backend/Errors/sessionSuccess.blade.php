@if(session('success'))
    <div class="alert alert-info alert-dismissible" role="alert" style="border-radius: 0">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        {{ session('success') }}
    </div>
@endif