@extends('Backend.Layouts.master')

@section('title')
	<title>Thông tin hệ thống | LuzBakery</title>
@stop()

@section('content')
	<section class="content">
        <ul class="breadcrumb">
		  <li>
		  	<a href="{{ route('dashboard') }}">
		  		<i class="fa fa-home" aria-hidden="true"></i>
		  	 	Bảng điểu khiển
		  	</a>
		  </li>
		  <li>
		  	Quản trị hệ thống
		  </li>
		  <li>
		  	Thông tin hệ thống
		  </li>
		</ul>
		<div class="row">
			<div class="col-md-12">
				<div class="box">
					@include('Backend.System.Component.inforSystem.index')
				</div>
			</div>
		</div>
	</section>
@stop()