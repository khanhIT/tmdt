<div class="col-sm-12 bs-callout">
    <div class="bs-callout-primary">
        <p>Vui lòng chia sẻ thông tin này nhằm mục đích điều tra nguyên nhân gây lỗi:</p>
        <div id="report-wrapper">
            <textarea name="txt-report" id="txt-report" class="col-sm-12" rows="10" spellcheck="false">                        
                ### Môi trường hệ thống

                - Phiên bản framework: 5.6
                - Múi giờ: UTC
                - Chế độ sửa lỗi: ✘
                - Có thể lưu trữ dữ liệu tạm: ✔
                - Có thể ghi bộ nhớ đệm: ✔
                - Kích thước ứng dụng: 246,29 MB
                
                ### Môi trường máy chủ

                - Phiên bản PHP: 7.1.3
                - Phần mềm: phpStorm
                - Hệ thống dữ liệu: mysql
                - Đã cài đặt chứng chỉ SSL: ✔
                - Loại lưu trữ bộ nhớ đệm: file
                - Loại lưu trữ phiên làm việc: file
                - Mbstring Ext: ✔
                - OpenSSL Ext: ✔
                - PDO Ext: ✔
                - CURL Ext: ✔
                - Exif Ext: ✔
                - File info Ext: ✔
                - Tokenizer Ext: ✔
                
                ### Các gói đã cài đặt và phiên bản

                - laravel/framework : 5.6.*
            </textarea>
        </div>
    </div>
</div>