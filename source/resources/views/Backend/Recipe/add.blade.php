@extends('Backend.Layouts.master')

@section('title')
	<title>Thêm mới công thức làm bánh | LuzBakery</title>
@stop()

@section('content')
	<section class="content">
        <ul class="breadcrumb">
		  <li>
		  	<a href="{{ route('dashboard') }}">
		  		<i class="fa fa-home" aria-hidden="true"></i>
		  	 	Bảng điểu khiển
		  	</a>
		  </li>
		  <li>
		  	<a href="{{ route('recipe.index') }}">Công thức làm bánh</a>
		  </li>
		  <li>
		  	Thêm mới công thức làm bánh
		  </li>
		</ul>
		<div class="pull-right" style="margin-top: -50px">
			<a href="{{ route('recipe.index') }}" class="btn btn-block btn-success">
				<i class="fa fa-list"></i>
				Danh sách
			</a>
		</div>
		<div class="row">
			<div class="col-md-12">
			   <form action="{{ route('recipe.store') }}" method="post" enctype="multipart/form-data">
					@csrf
					<div class="col-sm-9">
						@include('Backend.Recipe.Component.add.formRecipe')
					</div>
					<div class="col-sm-3">
						@include('Backend.Recipe.Component.add.hot')
					</div>
					<div class="col-sm-3">
						@include('Backend.Recipe.Component.add.action')
					</div>
			   </form>
			</div>
		</div>
	</section>
@stop()
@section('scriptAdd')
	<script src="https://cdn.ckeditor.com/ckeditor5/11.0.1/classic/ckeditor.js"></script>
	<script>
		  ClassicEditor
      .create( document.querySelector( '#editor' ) )
      .catch( error => {
          console.error( error );
      } );

      ClassicEditor
      .create( document.querySelector( '#editorContent' ) )
      .catch( error => {
          console.error( error );
      } );
	</script>
@stop()
