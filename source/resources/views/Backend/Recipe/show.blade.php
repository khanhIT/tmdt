@extends('Backend.Layouts.master')

@section('title')
	<title>Thêm mới công thúc làm bánh | LuzBakery</title>
@stop()

@section('styleAdd')
	<link rel="stylesheet" href="{{ asset('backend/pages/product/product.css') }}">
@stop()

@section('content')
	<section class="content">
        <ul class="breadcrumb">
		  <li>
		  	<a href="{{ route('dashboard') }}">
		  		<i class="fa fa-home" aria-hidden="true"></i>
		  	 	Bảng điểu khiển
		  	</a>
		  </li>
		  <li>
		  	<a href="{{ route('recipe.index') }}">Công thức làm bánh</a>
		  </li>
		  <li>
		  	Chi tiết công thức làm bánh
		  </li>
		</ul>
		<div class="pull-right" style="margin-top: -50px">
			<a href="{{ route('recipe.index') }}" class="btn btn-block btn-success">
				<i class="fa fa-list"></i>
				Danh sách
			</a>
		</div>
		<div class="row">
        <div class="col-md-12">
        	<div class="box">
           		@include('Backend.Recipe.Component.detail.informationRecipe')
        	</div>
        </div>
    </div>
	</section>
@stop()
