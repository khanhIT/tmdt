 @if(!empty($recipes))
 <tbody>
 	@foreach($recipes as $recipe)
    <tr>
        <td class="vertical">{{ $recipe->id }}</td>
         <td>
        	<div class="product_image">
        		<img src="{{ $recipe->thumbnail }}" alt="{{ $recipe->title }}">
        	</div>
        </td>
        <td class="vertical">
        	<a href="{{ route('recipe.show', $recipe->id) }}">{{ $recipe->title }}</a>
        </td>
        <td class="vertical">{{ $recipe->author }}</td>
        <td class="vertical">{{\Carbon\Carbon::parse($recipe->created_at)->format('Y/m/d H:i')  }}</td>
        <td class="vertical">
            {{ $recipe->hot == 1 ? 'Có' : 'Không'}}
        </td>
        <td class="vertical">
        	<a href="{{ route('recipe.edit', $recipe->id) }}" class="btn btn-sm waves-effect waves-light btn-warning">Sửa</a>
        	<a class="btn btn-sm btn-outline-danger btn-delete" data-url="{{ route('recipe.destroy', $recipe->id) }}" data-url-reload="{{ route('recipe.index', $recipe->id) }}">Xóa</a>
        </td>
    </tr>
    @endforeach
</tbody>
@endif
