<div class="" style="padding: 20px">
	<h2 class="m-b-0 m-t-0">
		<font>
			<font>{{ $recipe->title }}</font>
		</font>
	</h2>
	<hr>
	<div class="row">
		<div class="col-md-4">
			<div class="thumbnail">
				<img src="{{ $recipe->thumbnail }}" alt="{{ $recipe->title }}">
			</div>
		</div>
		<div class="col-md-8">
			<h4 class="box-title">Mô tả sản phẩm</h4>
			{!! $recipe->description !!}
		</div>
		<div class="col-lg-12 col-md-12 col-sm-12">
			<h4 class="box-title m-t-40">Thông tin chung</h4>
			<div class="table-responsive">
	          <table class="table table-hover">
	          	<tbody>
	          		<tr>
	          			<td class="w-350">Người viết bài</td>
	          			<td> {{ $recipe->author }} </td>
	          		</tr>
	          		<tr>
	          			<td class="w-350">Ngày viết</td>
	          			<td> {{ $recipe->created_at->format('d-m-Y H:i') }} </td>
	          		</tr>
	          		<tr>
	          			<td class="w-350">Mô tả chi tiết</td>
	          			<td>{!! $recipe->content !!}</td>
	          		</tr>
	          	</tbody>
	          </table>
	          <h5>Bình luận bài viết</h5>
		        <table class="table table-bordered">
				  <thead>
				    <tr>
				      <th scope="col">Tên người bình luận</th>
				      <th scope="col">Email</th>
				      <th scope="col">Nội dung bình luận</th>
				    </tr>
				  </thead>
				  <tbody>
				  	@foreach($comment as $com)
				    <tr>
				      <td>{{ $com->full_name }}</td>
				      <td>{{ $com->email }}</td>
				      <td>{{ $com->content }}</td>
				    </tr>
				    @endforeach()
				  </tbody>
				</table>
	       </div>
		</div>
	</div>
</div>
