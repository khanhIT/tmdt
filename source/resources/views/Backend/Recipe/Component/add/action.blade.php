<div class="box">
  <div style="padding: 20px">
    <div class="title">
      <h5>
        <label>Xuất bản</label>
      </h5>
    </div><hr>
    <button type="submit" class="btn waves-effect waves-light btn-rounded btn-outline-success">
      <i class="fa fa-save"> Lưu</i>
    </button>
    &nbsp;
    <button type="reset" class="btn waves-effect waves-light btn-rounded btn-outline-danger">
      <i class="fa fa-ban"> Hủy</i>
    </button>
  </div>
</div>
