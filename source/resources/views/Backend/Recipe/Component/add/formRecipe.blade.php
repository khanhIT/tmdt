<div class="box">
  <div style="padding: 20px">
    <div class="form-group row {{ !empty($errors->first('title')) ? 'has-warning' : '' }}">
        <label class="col-sm-3">Tiêu đề <span class="star">*</span></label>
        <div class="col-sm-9">
          <input type="text" name="title" autofocus class="form-control" value="{{ old('title') }}" placeholder="Tiêu đề">
          <span class="help-block">{{ !empty($errors->first('title')) ? $errors->first('title') : '' }}</span>
        </div>
    </div>

    <div class="form-group row {{ !empty($errors->first('thumbnail')) ? 'has-warning' : '' }}">
        <label class="col-sm-3">Hình ảnh <span class="star">*</span></label>
        <div class="col-sm-9">
          <input type="file" name="thumbnail" id="avatar" onchange="readURL(this)" class="form-control">
          <div class="item-image">
              <img id="preview-image" src="{{ asset('backend/image/bg.jpg') }}">
          </div>
          <span class="help-block">{{ !empty($errors->first('thumbnail')) ? $errors->first('thumbnail') : '' }}</span>
        </div>
    </div>

    <div class="form-group row {{ !empty($errors->first('author')) ? 'has-warning' : '' }}">
        <label class="col-sm-3">Người viết <span class="star">*</span></label>
        <div class="col-sm-9">
          <input type="text" name="author" class="form-control" value="{{ old('author') }}" placeholder="Người viết">
          <span class="help-block">{{ !empty($errors->first('author')) ? $errors->first('author') : '' }}</span>
        </div>
    </div>

    <div class="form-group row">
        <label class="col-sm-3">Mô tả</label>
        <div class="col-sm-9">
          <textarea class="form-control" name="description" id="editor" rows="8">{{ old('description') }}</textarea>
        </div>
    </div>

    <div class="form-group row {{ !empty($errors->first('content')) ? 'has-warning' : '' }}">
        <label class="col-sm-3">Nội dung <span class="star">*</span></label>
        <div class="col-sm-9">
          <textarea class="form-control" name="content" id="editorContent" rows="8">{{ old('content') }}</textarea>
          <span class="help-block">{{ !empty($errors->first('content')) ? $errors->first('content') : '' }}</span>
        </div>
    </div>
  </div>
</div>
