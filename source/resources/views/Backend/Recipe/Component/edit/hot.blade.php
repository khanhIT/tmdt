<div class="box">
	<div style="padding: 20px">
		<div class="title">
			<h5>
				<label>Hot</label>
			</h5>
		</div><hr>
		<div class="btn-set">
			<select name="hot" class="form-control">
				<option value="">-- Chọn hot --</option>
				<option value="1" @if (old('hot', $recipe->hot) == '1') {{ 'selected' }} @endif>Có</option>
				<option value="0" @if (old('hot', $recipe->hot) == '0') {{ 'selected' }} @endif>Không</option>
			</select>
		</div>
	</div>
</div>
