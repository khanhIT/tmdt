@extends('Backend.Layouts.master')

@section('title')
	<title>Công thức làm bánh | LuzBakery</title>
@stop()

@section('content')
	<section class="content">
        <ul class="breadcrumb">
		  <li>
		  	<a href="{{ route('dashboard') }}">
		  		<i class="fa fa-home" aria-hidden="true"></i>
		  	 	Bảng điểu khiển
		  	</a>
		  </li>
		  <li>
		  	Công thức làm bánh
		  </li>
		</ul>
		<div class="pull-right" style="margin-top: -50px">
			<a href="{{ route('recipe.create') }}" class="btn btn-block btn-success">
				<i class="fa fa-plus"></i>
				Thêm mới
			</a>
		</div>
		@include('Backend.Errors.sessionSuccess')
		<div class="row">
			<div class="col-md-12 dataTable">
				<div class="box">
					<table id="table" class="table table-hover">
						<thead>
		            <tr>
		                <th>ID</th>
		                <th>Tiêu đề</th>
		                <th>Hình ảnh</th>
		                <th>Người viết bài</th>
		                <th>Ngày viết</th>
		                <th>Nổi bật</th>
		                <th class="action">Tác vụ</th>
		            </tr>
				     </thead>
				        @include('Backend.Recipe.Component.list.listRecipe')
					</table>
				</div>
			</div>
		</div>
	</section>
@stop()
