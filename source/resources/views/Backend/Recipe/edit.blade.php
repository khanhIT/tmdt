@extends('Backend.Layouts.master')

@section('title')
	<title>Sửa công thức làm bánh | LuzBakery</title>
@stop()

@section('content')
	<section class="content">
        <ul class="breadcrumb">
		  <li>
		  	<a href="{{ route('dashboard') }}">
		  		<i class="fa fa-home" aria-hidden="true"></i>
		  	 	Bảng điểu khiển
		  	</a>
		  </li>
		  <li>
		  	<a href="{{ route('recipe.index') }}">Công thức làm bánh</a>
		  </li>
		  <li>
		  	Sửa công thức làm bánh
		  </li>
		</ul>
		<div class="pull-right" style="margin-top: -50px">
			<a href="{{ route('recipe.index') }}" class="btn btn-block btn-success">
				<i class="fa fa-list"></i>
				Danh sách
			</a>
		</div>
		<div class="row">
			<div class="col-md-12">
				<form action="{{ route('recipe.update', $recipe->id) }}" method="post" enctype="multipart/form-data">
					@csrf
					@method('PUT')
					<div class="col-sm-9">
						@include('Backend.Recipe.Component.edit.formEdit')
					</div>
					<div class="col-sm-3">
						@include('Backend.Recipe.Component.edit.hot')
					</div>
						<div class="col-sm-3">
						@include('Backend.Recipe.Component.edit.action')
					</div>
				</form>
			</div>
		</div>
	</section>
@stop()
@section('scriptAdd')
	<script src="https://cdn.ckeditor.com/ckeditor5/11.0.1/classic/ckeditor.js"></script>
	<script>
		 ClassicEditor
      .create( document.querySelector( '#editor' ) )
      .catch( error => {
          console.error( error );
      } );

			ClassicEditor
       .create( document.querySelector( '#content' ) )
       .catch( error => {
           console.error( error );
       } );
	</script>
@stop()
