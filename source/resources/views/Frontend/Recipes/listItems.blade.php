<section class="awe-section-7">
	<section class="section-news margin-bottom-20">
		<div class="container">
			<div class="blogs-content">
				<div class="heading a-center">
					<h2 class="title-head">
						<a href="/cong-thuc">Công thức mới</a>
					</h2>
				</div>
				<div class="list-blogs-link">
					<div class="row">
						@foreach($recipe as $lsRepice)
						<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
							<div class="blog-item-frame">
								<div class="blog-item-thumbnail">
									<a href="{{ route('detailRecipe', $lsRepice->slug) }}">
										<img class="img-responsive" src="{{ $lsRepice->thumbnail }}" data-lazyload="{{ $lsRepice->thumbnail }}" alt="{{ $lsRepice->title }}">
									</a>
									<div class="post-time">
										{{\Carbon\Carbon::parse($lsRepice->created_at)->format('d/m/Y')  }}
									</div>
								</div>
								<div class="blog-item-info">
									<h3 class="blog-item-name">
										<a href="{{ route('detailRecipe', $lsRepice->slug) }}" title="{{ $lsRepice->title }}">{{ $lsRepice->title }}</a>
									</h3>
									<p class="blog-item-summary margin-bottom-5 line-clamp" style="min-height: 0 !important;">
										<i>{!! substr($lsRepice->description, 0, 150)  !!}</i>
									</p>
								</div>
							</div>
						</div>
						@endforeach()
						<div class="see-more">
							<a href="/cong-thuc">Xem tất cả</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</section>
