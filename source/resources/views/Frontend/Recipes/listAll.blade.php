@extends('Layouts.main')

@section('meta')
	<title>Công thức - LuzBakery</title>
@stop()

@section('content')
	<div class="module-title">
		<div class="bg-frame"></div>
		<h2>
			<a href="javascript:void(0);">
				Công thức
			</a>
		</h2>
		<section class="bread-crumb margin-bottom-10">
			<div class="container">
				<div class="row">
					<div class="col-xs-12" style="width: 100%">
						<ul class="breadcrumb">
							<li class="home">
								<a itemprop="url" href="/" title="Trang chủ">
									<span itemprop="title">Trang chủ</span>
								</a>
								<span><i class="fa fa-angle-right"></i></span>
							</li>
							<li>Công thức</li>
						</ul>
					</div>
				</div>
			</div>
		</section>
	</div>
	<div class="luz-blog container">
		<meta itemprop="name" content="Công thức"> 
		<meta itemprop="description" content="Chủ đề không có mô tả"> 
		<div class="row">
			<!--sidebar-->
			@include('Elements.Layout.sidebar')
			<!--sidebar-->
			<section class="right-content col-md-9 col-md-push-3 list-blog-page">	
				<div class="border-bg clearfix">
					<div class="col-md-12">
						<div class="box-heading d-none">
							<h1 class="title-head">Công thức</h1>
						</div>
						<section class="list-blogs blog-main">
							<div class="row">
								@foreach($recipe as $lsRecipe)
								<article class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
									<div class="blog-item-frame">
										<div class="blog-item-thumbnail">	
											<a href="{{ route('detailRecipe', $lsRecipe->slug) }}">
												<img class="img-responsive" src="{{ $lsRecipe->thumbnail }}" alt="{{ $lsRecipe->title }}">
											</a>
											<div class="post-time">
												{{\Carbon\Carbon::parse($lsRecipe->created_at)->format('d/m/Y')  }}
											</div>
										</div>
										<div class="blog-item-info">
											<h3 class="blog-item-name">
												<a href="{{ route('detailRecipe', $lsRecipe->slug) }}" title="{{ $lsRecipe->title }}">{{ $lsRecipe->title }}</a>
											</h3>
											<p class="blog-item-summary margin-bottom-5 line-clamp" style="min-height: 0 !important;">{!! substr($lsRecipe->description, 0, 150) !!}</p>
										</div>
									</div>
								</article>
								@endforeach()
							</div>
						</section>
					</div>
				</div>
			</section>
		</div>
	</div>
@stop()