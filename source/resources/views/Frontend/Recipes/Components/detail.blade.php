@extends('Layouts.main')

@section('meta')
    <title>Cách làm bánh - LuzBakery</title>
@stop()

@section('content')
    <div class="module-title">
        <div class="bg-frame"></div>
        <h2>
            <a href="javascript:void(0);">
                {{ (!empty($recipe)) ? $recipe->title : '...' }}
            </a>
        </h2>
        <section class="bread-crumb margin-bottom-10">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12" style="width: 100%">
                        <ul class="breadcrumb">
                            <li class="home">
                                <a itemprop="url" href="/" title="Trang chủ"><span itemprop="title">Trang chủ</span></a>
                                <span><i class="fa fa-angle-right"></i></span>
                            </li>
                            <li class="home">
                                <a itemprop="url" href="/" title="Trang chủ">
                                    <span itemprop="title">Công thức</span>
                                </a>
                                <span><i class="fa fa-angle-right"></i></span>
                            </li>
                            <li> {{ (!empty($recipe)) ? $recipe->title : '...' }}</li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <div class="luz-blog container">
        <meta itemprop="name" content="Công thức">
        <meta itemprop="description" content="Chủ đề không có mô tả">
        <div class="row">
            <!--sidebar-->
            @include('Elements.Layout.sidebar')
            <!--sidebar-->
            @if(!empty($recipe))
                <section class="right-content col-md-9 col-md-push-3">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="postby">
                            <span>
                                <i class="fa fa-user"></i> {{ $recipe->author }} -
                                <i class="fa fa-calendar"></i> {{\Carbon\Carbon::parse($recipe->created_at)->format('d/m/Y')  }}
                            </span>
                        </div>
                        <div>
                            <div class="article-details">
                                <div class="article-content">
                                    <div class="rte">
                                        <p>
                                            <strong>
                                                <em>{!! $recipe->description !!}</em>
                                            </strong>
                                        </p>
                                        <p>
                                            <strong>
                                                <em>
                                                    <img alt="Cách làm bánh Matcha Delight" data-thumb="original" original-height="400" original-width="600" src="{{ $recipe->thumbnail }}">
                                                </em>
                                            </strong>
                                        </p>
                                        <p>{!! $recipe->content !!}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        @if(!empty($commentRecipe))
                            <div id="article-comments">
                                <h5 class="title-form-coment">Bình luận ({{ $commentRecipe->count() }})</h5>
                                <div class="article-comment clearfix">
                                    @foreach($commentRecipe as $comment)
                                    <figure class="article-comment-user-image">
                                        <img src="https://www.gravatar.com/avatar/c9b25479cdcfed34dab4c4599e0163d9?s=110&amp;d=identicon" alt="binh-luan" class="block">
                                    </figure>
                                    <div class="article-comment-user-comment">
                                        <p class="user-name-comment"><strong>{{ $comment->full_name }}</strong></p>
                                        <p>{{ $comment->content }}</p>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                        @endif
                        <div class="text-xs-right"></div>
                        <form accept-charset="UTF-8" id="recipe_comments" action="{{ route('commentRecipe') }}" method="post">
                            <input name="recipe_id" type="hidden" value="{{ $recipe->id }}">
                            @csrf()
                            <div class="col-lg-12">
                                <div class="form-coment margin-bottom-30" style="margin-left: -15px">
                                    <h5 class="title-form-coment">VIẾT BÌNH LUẬN CỦA BẠN:</h5>
                                    <div class="invalid-feedback error" style="margin-bottom: 10px; border: none"></div>
                                    <fieldset class="art-comment-name form-group col-12 col-sm-6 col-md-6">
                                        <input placeholder="Họ tên" type="text" class="form-control form-control-lg" name="full_name">
                                        <div class="invalid-feedback errorFullName"></div>
                                    </fieldset>
                                    <fieldset class="form-group col-12 col-sm-6 col-md-6">
                                        <input placeholder="Email" type="email" class="form-control form-control-lg" name="email">
                                        <div class="invalid-feedback errorEmail"></div>
                                    </fieldset>
                                    <fieldset class="form-group col-12 col-sm-12 col-md-12">
                                        <textarea placeholder="Nội dung" class="form-control form-control-lg" name="content" rows="6"></textarea>
                                        <div class="invalid-feedback errorContent"></div>
                                    </fieldset>
                                    <div>
                                        <button type="submit" class="btn btn-white comment">Gửi bình luận</button>
                                    </div>
                                </div> <!-- End form mail -->
                            </div>
                        </form>
                    </div>
                </div>
            </section>
            @else
                <div class="error-page">
                    <h2 class="headline text-warning"> 404</h2>
                    <div class="error-content">
                        <h3><i class="fa fa-warning text-warning"></i> Oops! Page not found.</h3>
                        <p>
                            Chúng tôi không thể tìm thấy trang bạn đang tìm kiếm.
                            Trong khi đó, bạn có thể quay lại <a style="color: #0d6aad; cursor: pointer" onclick="history.back()">trang trước</a>
                        </p>
                    </div>
                    <!-- /.error-content -->
                </div>
            @endif
        </div>
    </div>
@stop()
@section('scriptAddon')
    <script src="{{ asset('frontend/js/comment_recipe/comment_recipe.js') }}"></script>
@stop()