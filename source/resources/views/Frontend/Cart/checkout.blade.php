<!DOCTYPE html>
<html lang="vi">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="shortcut icon" type="image/x-icon" href="{{ URL::to('/') }}/frontend/images/favicon.png">
    <title>Thanh toán - LuzBakery</title>
    <meta name="description" content="">
    <meta name="keywords" content="Luz Bakery, cake">
    <meta name="revisit-after" content="1 days">
    <meta name="robots" content="noodp,index,follow">

    <script src="https://js.stripe.com/v3/"></script>

    @include('Elements.Layout.mainStyle')
    @yield('stylesheetAddon')

    <style>

        .StripeElement {
            box-sizing: border-box;
            height: 40px;
            padding: 10px 12px;
            border: 1px solid transparent;
            border-radius: 4px;
            background-color: white;
            box-shadow: 0 1px 3px 0 #e6ebf1;
            -webkit-transition: box-shadow 150ms ease;
            transition: box-shadow 150ms ease;
        }

        .StripeElement--focus {
            box-shadow: 0 1px 3px 0 #cfd7df;
        }

        .StripeElement--invalid {
            border-color: #fa755a;
        }

        .StripeElement--webkit-autofill {
            background-color: #fefde5 !important;
        }

        #card-errors {
            color: #fa755a
        }

    </style>

</head>

<body class="body--custom-background-color ">
    @include('Frontend.Error.success')    
    @include('Frontend.Error.errors')
    <div class="banner" data-header="">
        <div class="wrap">
            <div class="shop logo logo--left ">
                <h1 class="shop__name">
                    <a href="/" class="color">
                        Luz Bakery
                    </a>
                </h1>
            </div>
        </div>
    </div>

    <button class="order-summary-toggle">
        <div class="wrap">
            <h5>
                <label class="control-label">Đơn hàng</label>
                <label class="control-label d-sm-noneall-device">
                    ({{ \Cart::count() }} sản phẩm)
                </label>
            </h5>
        </div>
    </button>

    <form method="post" action="{{ route('cart.checkout') }}" id="payment-form" data-toggle="validator" class="content stateful-form formCheckout" novalidate="true">
    @csrf
        <div class="wrap" context="checkout">
            <!-- đơn hàng -->
            <div class="sidebar ">
                <div class="sidebar_header">
                    <h5>
                        <label class="control-label">Đơn hàng</label>
                        <label class="control-label">({{ \Cart::count() }} sản phẩm)</label>
                    </h5>
                    <hr class="full_width">
                </div>
                <div class="sidebar__content">
                    <div class="order-summary order-summary--product-list order-summary--is-collapsed">
                        <div class="summary-body summary-section summary-product">
                            <div class="summary-product-list">
                                <table class="product-table">
                                    <tbody>
                                    @foreach(\Cart::content() as $item)
                                        <tr class="product product-has-image clearfix">
                                            <td>
                                                <div class="product-thumbnail">
                                                    <div class="product-thumbnail__wrapper">
                                                        <img src="{{ $item->model->image }}" class="product-thumbnail__image">
                                                    </div>
                                                    <span class="product-thumbnail__quantity" aria-hidden="true">{{ $item->qty }}</span>
                                                </div>
                                            </td>
                                            <td class="product-info">
                                                <span class="product-info-name">
                                                   {{ $item->model->product_name }}
                                                </span>
                                            </td>
                                            <td class="product-price text-right">
                                                {{ $item->price }}₫
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                <div class="order-summary__scroll-indicator">
                                    Cuộn chuột để xem thêm
                                    <i class="fa fa-long-arrow-down" aria-hidden="true"></i>
                                </div>
                            </div>
                        </div>
                        <hr class="m0">
                    </div>
                    <!-- <div class="order-summary order-summary--discount">
                        <div class="summary-section">
                            <div class="form-group m0">
                                <div class="field__input-btn-wrapper">
                                    <div class="field__input-wrapper">
                                        <input bind="code" name="code" type="text" class="form-control discount_code" placeholder="Nhập mã giảm giá" value="" id="checkout_reduction_code">
                                    </div>
                                    <button class="btn btn-primary event-voucher-apply" type="button">Áp dụng</button>
                                </div>
                            </div>

                            <div class="error mt10" bind-show="inValidCode">
                                Mã khuyến mãi không hợp lệ
                            </div>
                        </div>
                        <hr class="m0">
                    </div> -->
                    <div class="order-summary order-summary--total-lines">
                        <div class="summary-section border-top-none--mobile">
                            <div class="total-line total-line-subtotal clearfix">
                                <span class="total-line-name pull-left">
                                    Tạm tính
                                </span>
                                <span class="total-line-subprice pull-right">{{ \Cart::subtotal() }}₫
                                        </span>
                            </div>
                            <div class="total-line total-line-total clearfix">
                                <span class="total-line-name pull-left">
                                    Tổng cộng
                                </span>
                                <span class="total-line-price pull-right">{{ \Cart::total() }}₫</span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group clearfix d-sm-none d-none d-md-block d-lg-block">
                        <div class="field__input-btn-wrapper mt10">
                            <a class="previous-link color" href="{{ route('cart.index') }}">
                                <i class="fa fa-angle-left fa-lg" aria-hidden="true"></i>
                                <span>Quay về giỏ hàng</span>
                            </a>
                            <input type="submit" class="btn btn-primary btn-checkout" id="complete-order" value="ĐẶT HÀNG">
                        </div>
                    </div>
                    <div class="form-group has-error">
                        <div class="help-block ">
                            <ul class="list-unstyled">

                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end -->

            <div class="main" role="main">
                <div class="main_header">
                    <div class="shop logo logo--left ">
                        <h1 class="shop__name">
                            <a href="/">
                                Luz Bakery
                            </a>
                        </h1>
                    </div>
                </div>
                <div class="main_content">
                    <div class="row">
                        <!-- thông tin khách hàng -->
                        <div class="col-md-6 col-lg-6">
                            <div class="section">
                                <div class="section__header">
                                    <div class="layout-flex layout-flex--wrap">
                                        <h2 class="section__title layout-flex__item layout-flex__item--stretch">
                                            <i class="fa fa-id-card-o fa-lg section__title--icon d-md-none d-lg-none" aria-hidden="true"></i>
                                            <label class="control-label">Thông tin mua hàng</label>
                                        </h2>
                                    </div>
                                </div>
                                <div class="section__content">
                                    <div class="form-group">
                                        <div>
                                            <div class="field__input-wrapper" >
                                                <span class="field__label" >
                                                    Email
                                                </span>
                                                <input name="email" id="email" type="email" class="field__input form-control" value="{{ Auth::guard('customer')->user()->email }}" pattern="^([a-zA-Z0-9_\-\.\+]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="billing">
                                        <div>
                                            <div class="form-group">
                                                <div class="field__input-wrapper">
                                                    <span class="field__label">
                                                        Họ và tên
                                                    </span>
                                                    <input name="full_name" id="full_name" type="text" value="{{ Auth::guard('customer')->user()->last_name }}" class="field__input form-control">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="field__input-wrapper">
                                                    <span class="field__label">
                                                        Số điện thoại
                                                    </span>
                                                    <input name="phone" type="tel" id="phone" value="{{ Auth::guard('customer')->user()->phone }}" class="field__input form-control" pattern="^([0-9,\+,\-,\(,\),\.]{8,20})$">
                                                </div>
                                            </div>
                                    
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end -->

                        <!-- Thanh toán -->
                        <div class="col-md-6 col-lg-6">
                            <div class="section payment-methods">
                                <div class="section__header">
                                    <h2 class="section__title">
                                        <i class="fa fa-credit-card fa-lg section__title--icon d-md-none d-lg-none" aria-hidden="true"></i>
                                        <label class="control-label">Thanh toán</label>
                                    </h2>
                                </div>
                                <div class="form-group">
                                    <div class="field__input-wrapper">
                                        <span class="field__label">
                                            Tên thẻ
                                        </span>
                                        <input type="text" name="nameorcreadit" id="name" class="field__input form-control">
                                    </div>
                                    <div class="help-block with-errors"></div>
                                </div>
                                <div class="form-group inline">
                                    <label for="card-element">
                                        Credit or debit card
                                    </label>
                                    <div id="card-element">
                                    <!-- A Stripe Element will be inserted here. -->
                                    </div>

                                    <!-- Used to display form errors. -->
                                    <div id="card-errors" role="alert"></div>
                                </div>
                            </div>
                            <div class="section d-md-none d-lg-none">
                                <div class="form-group clearfix m0">
                                    <input class="btn btn-primary btn-checkout" data-loading-text="Đang xử lý" type="button" value="ĐẶT HÀNG">
                                </div>
                                <div class="text-center mt20">
                                    <a class="previous-link color" href="{{ route('cart.index') }}">
                                        <i class="fa fa-angle-left fa-lg" aria-hidden="true"></i>
                                        <span>Quay về giỏ hàng</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <!-- end -->
                    </div>
                </div>
                <div class="main_footer footer unprint">
                    <div class="mt10"></div>
                </div>
                <div class="modal fade" id="refund-policy" data-width="" tabindex="-1" role="dialog">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                                <h4 class="modal-title">Chính sách hoàn trả</h4>
                            </div>
                            <div class="modal-body">
                                <pre></pre>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal fade" id="privacy-policy" data-width="" tabindex="-1" role="dialog">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                                <h4 class="modal-title">Chính sách bảo mật</h4>
                            </div>
                            <div class="modal-body">
                                <pre></pre>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal fade" id="terms-of-service" data-width="" tabindex="-1" role="dialog">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                                <h4 class="modal-title">Điều khoản sử dụng</h4>
                            </div>
                            <div class="modal-body">
                                <pre></pre>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>

@include('Elements.Layout.mainJs')

@yield('scriptAddon')

<script>
    $(document).ready(function() {
        $("#province").change(function() {
            var province =$(this).val();

            $.get("name/" + province, function (data) {
                $('#district').html(data);
            });
        });


        // Create a Stripe client.
        var stripe = Stripe('pk_test_7oJi579GpY8azlHX2KpFzcEB00KhFbl9pt');

        // Create an instance of Elements.
        var elements = stripe.elements();

        // Custom styling can be passed to options when creating an Element.
        // (Note that this demo uses a wider set of styles than the guide below.)
        var style = {
        base: {
            color: '#32325d',
            fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
            fontSmoothing: 'antialiased',
            fontSize: '16px',
            '::placeholder': {
            color: '#aab7c4'
            }
        },
        invalid: {
            color: '#fa755a',
            iconColor: '#fa755a'
        }
        };

        // Create an instance of the card Element.
        var card = elements.create('card', {
            style: style,
            hidePostalCode: true
        });

        // Add an instance of the card Element into the `card-element` <div>.
        card.mount('#card-element');

        // Handle real-time validation errors from the card Element.
        card.addEventListener('change', function(event) {
        var displayError = document.getElementById('card-errors');
        if (event.error) {
            displayError.textContent = event.error.message;
        } else {
            displayError.textContent = '';
        }
        });

        // Handle form submission.
        var form = document.getElementById('payment-form');
        form.addEventListener('submit', function(event) {
        event.preventDefault();
        // disable submit button click    
        document.getElementById('complete-order').disable = true;

        stripe.createToken(card).then(function(result) {
            if (result.error) {
                // Inform the user if there was an error.
                var errorElement = document.getElementById('card-errors');
                errorElement.textContent = result.error.message;

                // enable the submit button    
                document.getElementById('complete-order').disable = false;
                } else {
                // Send the token to your server.
                stripeTokenHandler(result.token);
                }
            });
        });

        // Submit the form with the token ID.
        function stripeTokenHandler(token) {
            // Insert the token ID into the form so it gets submitted to the server
            var form = document.getElementById('payment-form');
            var hiddenInput = document.createElement('input');
            hiddenInput.setAttribute('type', 'hidden');
            hiddenInput.setAttribute('name', 'stripeToken');
            hiddenInput.setAttribute('value', token.id);
            form.appendChild(hiddenInput);

            // Submit the form
            form.submit();
        }

    });
</script>

</html>