@extends('Layouts.main')

@section('meta')
    <title>Giỏ hàng - LuzBakery</title>
@stop()

@section('content')
    <div class="module-title">
        <div class="bg-frame"></div>
        <h2>
            <a href="javascript:void(0);">
                Giỏ Hàng
            </a>
        </h2>
        <section class="bread-crumb margin-bottom-10">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12" style="width: 100%">
                        <ul class="breadcrumb">
                            <li class="home">
                                <a itemprop="url" href="/" title="Trang chủ">
                                    <span itemprop="title">Trang chủ</span>
                                </a>
                                <span><i class="fa fa-angle-right"></i></span>
                            </li>
                            <li>Giỏ Hàng</li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <div id="luz-cart" class="container white collections-container margin-bottom-20">
        <div class="white-background">
            <div class="row">
                <div class="col-md-12">
                    <div class="shopping-cart">
                        <div class="d-sm-none d-none d-md-block d-lg-block">
                            <div class="shopping-cart-table">
                                <div class="row">
                                    <div class="col-md-12">
                                        <h1 class="lbl-shopping-cart lbl-shopping-cart-gio-hang">Giỏ hàng
                                            <span>(<span class="count_item_pr">{{ \Cart::count() }}</span> sản phẩm)</span>
                                        </h1>
                                    </div>
                                </div>
                                <div class="row">
                                    @if(session('success'))
                                        <div class="alert alert-info alert-dismissible" role="alert" style="border-radius: 0">
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                            {{ session('success') }}
                                        </div>
                                    @endif
                                </div>
                                @if(\Cart::count() > 0)
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="col-main cart_desktop_page cart-page">
                                                <div class="cart page_cart cart_des_page hidden-xs-down">
                                                    <div class="col-9">
                                                        @foreach(\Cart::content() as $item)
                                                        <div class="cart-tbody" id="shopping-cart">
                                                            <div class="row shopping-cart-item">
                                                                <div class="col-3 img-thumnail-custom">
                                                                    <p class="image">
                                                                        <img class="img-responsive" src="{{ $item->model->image }}" alt="Bánh Kem Oreo">
                                                                    </p>
                                                                </div>
                                                                <div class="col-right col-9">
                                                                    <div class="box-info-product">
                                                                        <p class="name">
                                                                            <a href="{{ route('detailProduct', $item->model->product_name) }}" target="_blank">{{ $item->model->product_name }}</a>
                                                                        </p>
                                                                        <p class="action">
                                                                            <form action="{{ route('cart.destroy', $item->rowId ) }}" method="post">
                                                                                @csrf
                                                                                @method('delete')
                                                                                <button type="submit" class="btn btn-link btn-item-delete remove-item-cart">Xóa</button>
                                                                            </form>
                                                                        </p>
                                                                    </div>
                                                                    <div class="box-price">
                                                                        <p class="price">{{ number_format($item->subtotal, 3) }}₫</p>
                                                                    </div>
                                                                    <div class="quantity-block">
                                                                        <div class="input-group bootstrap-touchspin">
                                                                            <div class="input-group-btn">
                                                                                <select class="quantity" data-id="{{ $item->rowId }}">
                                                                                    @for($i=1; $i<=10; $i++)
                                                                                        <option {{ $item->qty == $i ? 'selected' : '' }}>{{ $i }}</option>
                                                                                    @endfor
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        @endforeach
                                                    </div>
                                                    <div class="col-3 cart-collaterals cart_submit">
                                                        <div id="right-affix">
                                                            <div class="each-row">
                                                                <div class="box-style fee">
                                                                    <p class="list-info-price">
                                                                        <span>Tạm tính: </span>
                                                                        <strong class="totals_price price _text-right text_color_right1">{{ \Cart::subtotal() }}₫</strong>
                                                                    </p>
                                                                </div>
                                                                <div class="box-style fee">
                                                                    <div class="total2 clearfix">
                                                                        <span class="text-label">Thành tiền: </span>
                                                                        <div class="amount">
                                                                            <p>
                                                                                <strong class="totals_price">{{ \Cart::total() }}₫</strong>
                                                                            </p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <button class="button btn-proceed-checkout btn btn-large btn-block btn-danger btn-checkout" title="Thanh toán ngay" type="button" onclick="window.location.href='/checkout'">Thanh toán ngay</button>
                                                                <button class="button btn-proceed-checkout btn btn-large btn-block btn-danger btn-checkouts" title="Tiếp tục mua hàng" type="button" onclick="window.location.href='/tat-ca-san-pham'">Tiếp tục mua hàng
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @else
                                    <div class="row">
                                        <div class="col-md-6 offset-md-3">
                                            <div class="cart-empty">
                                                <img src="{{ asset('frontend/images/empty_cart.png') }}" alt="Giỏ hàng trống">
                                                <div class="btn-cart-empty">
                                                    <a class="btn btn-default" onclick="window.location.href='/tat-ca-san-pham'" title="Tiếp tục mua sắm">Tiếp tục mua sắm</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </div>
                        {{--Mobile--}}
                        <div class="d-md-none d-lg-none">
                            @if(\Cart::count() > 0)
                                <div class="cart-mobile">
                                    <div class="header-cart">
                                        <div class="title-cart">
                                            <h3>Giỏ hàng của bạn</h3>
                                        </div>
                                    </div>
                                    <div class="header-cart-content">
                                        <div class="cart_page_mobile content-product-list">
                                            @foreach(\Cart::content() as $item)
                                            <div class="item-product item">
                                                <div class="item-product-cart-mobile">
                                                    <a class="product-images1" href="" title="{{ $item->model->image }}">
                                                        <img width="80" height="150" alt="" src="{{ $item->model->image }}">
                                                    </a>
                                                </div>
                                                <div class="title-product-cart-mobile">
                                                    <h3>
                                                        <a href="{{ route('detailProduct', $item->model->product_name) }}" title="{{ $item->model->product_name }}">{{ $item->model->product_name }}</a>
                                                    </h3>
                                                    <p>Giá: <span>{{ $item->subtotal }}₫</span></p>
                                                </div>
                                                <div class="select-item-qty-mobile">
                                                    <select class="quantity" data-id="{{ $item->rowId }}">
                                                        @for($i=1; $i<=10; $i++)
                                                            <option {{ $item->qty == $i ? 'selected' : '' }}>{{ $i }}</option>
                                                        @endfor
                                                    </select>
                                                    <form action="{{ route('cart.destroy', $item->rowId ) }}" method="post">
                                                        @csrf
                                                        @method('delete')
                                                        <button type="submit" class="btn btn-link btn-item-delete remove-item-cart">Xóa</button>
                                                    </form>
                                                </div>
                                            </div>
                                            @endforeach
                                        </div>
                                        <div class="header-cart-price" style="">
                                            <div class="title-cart ">
                                                <h3 class="text-xs-left">Tạm tính: </h3>
                                                <a class="text-xs-right totals_price_mobile">{{ \Cart::subtotal() }}₫</a>
                                            </div>
                                            <div class="title-cart ">
                                                <h3 class="text-xs-left">Thuế (10%): </h3>
                                                <a class="text-xs-right totals_price_mobile">{{ \Cart::tax() }}₫</a>
                                            </div>
                                            <div class="title-cart ">
                                                <h3 class="text-xs-left">Tổng tiền: </h3>
                                                <a class="text-xs-right totals_price_mobile">{{ \Cart::total() }}₫</a>
                                            </div>
                                            <div class="checkout">
                                                <button class="btn-proceed-checkout-mobile" title="Thanh toán ngay" type="button" onclick="window.location.href='checkout'">
                                                    <span>Thanh toán ngay</span>
                                                </button>
                                            </div>
                                            <button class="btn btn-proceed-continues-mobile" title="Tiếp tục mua hàng" type="button" onclick="window.location.href='/tat-ca-san-pham'">Tiếp tục mua hàng</button></div>
                                    </div>
                                </div>
                            @else
                                <div class="cart-empty">
                                    <img src="{{ asset('frontend/images/empty_cart.png') }}" class="img-responsive center-block" alt="Giỏ hàng trống">
                                    <div class="btn-cart-empty">
                                        <a class="btn btn-default" onclick="window.location.href='/tat-ca-san-pham'" title="Tiếp tục mua sắm">Tiếp tục mua hàng</a>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop()
@section('scriptAddon')
    <script src="{{ asset('js/app.js') }}"></script>
    <script>
        (function(){
            const classname = document.querySelectorAll('.quantity');
            Array.from(classname).forEach(function(element){
               element.addEventListener('change', function (){
                   const id = element.getAttribute('data-id');
                   axios.patch(`gio-hang/${id}`, {
                       quantity: this.value
                   })
                   .then(function (response) {
                       // console.log(response);
                        window.location.href='{{ route('cart.index') }}';
                   })
                   .catch(function (error) {
                       console.log(error);
                       window.location.href='{{ route('cart.index') }}';
                   })
               });
            });
        })();
    </script>
@stop