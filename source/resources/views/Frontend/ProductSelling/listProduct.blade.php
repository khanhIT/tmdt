<section class="awe-section-3">
    <section class="section-selling margin-bottom-20">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 selling-content">
                    <div class="heading a-center">
                        <h2 class="title-head">
                            <a href="#">Sản phẩm bán chạy</a>
                        </h2>
                    </div>
                    <div id="owl-demo-2" class="owl-carousel owl-theme">
                        <!-- list item product -->
                        @include('Frontend.ProductSelling.Components.listItems')
                        <!-- list item product -->
                    </div>
                </div>
            </div>
        </div>
    </section>
</section>
