@foreach($product as $pro)
<article class="thumbnail item" itemscope="">
    <div class="product-thumbnail-frame">
        @if(isset($pro->sale_price))
        <div class="sale-flash">SALE</div>
        @endif
        <a class="blog-thumb-img" href="{{ route('detailProduct', $pro->product_name) }}" title="{{ $pro->product_name }}">
            <img src="{{ $pro->image }}" class="img-responsive" title="{{ $pro->product_name }}" />
        </a>
    </div>
    <div class="caption">
        <h5 itemprop="headline" class="product-name">
            <a href="{{ route('detailProduct', $pro->product_name) }}" rel="bookmark">{{ $pro->product_name }}</a>
        </h5>
        <div class="price-box clearfix">
            @if(!empty($pro->sale_price))
            <div class="special-price">
                <span class="price product-price">Giá: {{ number_format($pro->sale_price, 3) }}₫</span>
            </div>
            <div class="old-price">
                <span class="price product-price-old">
                    {{ number_format($pro->price, 3) }}₫
                </span>
            </div>
            @else
            <div class="old-price">
                <span class="price">
                   Giá: {{ number_format($pro->price, 3) }}₫
                </span>
            </div>
            @endif
        </div>
        <div class="product-action clearfix">
            <form action="{{ route('addToCart') }}" method="post" class="variants form-nut-grid has-validation-callback" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div>
                    <input type="hidden" name="id" value="{{ $pro->id }}">
                    <input type="hidden" name="product_name" value="{{ $pro->product_name }}">
                    <input type="hidden" name="sale_price" value="{{ $pro->sale_price }}">
                    <input type="hidden" name="price" value="{{ $pro->price }}">
                    <input type="hidden" name="image" value="{{ $pro->image }}">
                    <button type="submit" class="add2cart btn-buy btn-cart btn btn-gray left-to btn-primary add_to_cart" title="Thêm vào giỏ">
                        <span>Thêm vào giỏ</span>
                    </button>
                </div>
            </form>
        </div>
    </div>
</article>
@endforeach

