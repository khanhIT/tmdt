@extends('Layouts.main')

@section('meta')
    <title>{{ $proDetail->product_name }} - LuBakery</title>
@stop

@section('content')
    <div class="module-title">
        <div class="bg-frame"></div>
        <h2>
            <a href="javascript:void(0);">
               {{ $proDetail->product_name }}
            </a>
        </h2>
        <section class="bread-crumb margin-bottom-10">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12" style="width: 100%">
                        <ul class="breadcrumb">
                            <li class="home">
                                <a itemprop="url" href="/" title="Trang chủ">
                                    <span itemprop="title">Trang chủ</span>
                                </a>
                                <span><i class="fa fa-angle-right"></i></span>
                            </li>
                            <li class="home">
                                <a itemprop="url" href="/" title="Tất cả sản phẩm">
                                    <span itemprop="title">Tất cả sản phẩm</span>
                                </a>
                                <span><i class="fa fa-angle-right"></i></span>
                            </li>
                            <li>{{ $proDetail->product_name }}</li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <section class="product" style="overflow-x: hidden">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 details-product">
                    <div class="product-essential">
                        <div class="row">
                            <div class="col-12 col-sm-12 col-md-9 col-lg-9" style="padding: 0">
                                <div class="col-12 col-sm-6 col-md-5 col-lg-5">
                                    <div class="large-image">
                                        <img src="{{ $proDetail->image }}" alt="">
                                    </div>
                                </div>
                                <div class="col-12 col-sm-6 col-md-7 col-lg-7 details-pro">
                                    <h1 class="title-head">{!! $proDetail->product_name !!}</h1>
                                    <div itemprop="offers">
                                        <div class="price-box clearfix">
                                            @if(!empty($proDetail->sale_price))
                                            <span class="special-price">
                                                <span class="price product-price">{{ number_format($proDetail->sale_price, 3) }}₫</span>
                                            </span> <!-- Giá Khuyến mại -->
                                            <span class="old-price" itemprop="priceSpecification" style="display: inline;">
                                                <del class="price product-price-old" style="display: inline;">{{ number_format($proDetail->price, 3) }}₫</del>
                                            </span> <!-- Giá gốc -->
                                            @else
                                            <span class="special-price">
                                                <span class="price product-price">{{ number_format($proDetail->price, 3) }}₫</span>
                                            </span> <!-- Giá gốc -->
                                            @endif
                                            @if($proDetail->qty == 0)
                                            <div class="select-swatch">
                                                <span class="txt-main" style="
                                                        border-radius: 25px;
                                                        border: 1px solid #f4ba42;
                                                        background: #1c011f;
                                                        line-height: 30px;
                                                        color: #fff;
                                                        text-transform: uppercase;
                                                        margin-bottom: 20px;
                                                        height: 40px;
                                                        padding: 5px 20px;">
                                                    Hết hàng
                                                </span>
                                            </div>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="product-summary product_description margin-bottom-15">
                                        <div class="rte description">
                                           {!! substr($proDetail->description, 0, 250) !!}
                                        </div>
                                    </div>
                                    <div class="form-product">
                                        <form action="{{ route('addToCart') }}" enctype="multipart/form-data" id="add-to-cart-form" method="post" class="form-inline has-validation-callback">
                                            @csrf
                                            <div class="select-swatch">
                                                <div class="form-group">
                                                    <input type="hidden" name="id" value="{{ $proDetail->id }}">
                                                    <input type="hidden" name="qty" value="{{ $proDetail->qty }}">
                                                    <input type="hidden" name="product_name" value="{{ $proDetail->product_name }}">
                                                    <input type="hidden" name="sale_price" value="{{ $proDetail->sale_price }}">
                                                    <input type="hidden" name="price" value="{{ $proDetail->price }}">
                                                    <input type="hidden" name="image" value="{{ $proDetail->image }}">
                                                    <button type="submit" class="btn btn-lg btn-gray btn-cart add_to_cart btn_buy add_to_cart" title="Thêm vào giỏ">
                                                        <span class="txt-main">Thêm vào giỏ</span>
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-12 col-md-12 col-lg-12 margin-top-15 margin-bottom-10">
                                    <!-- Nav tabs -->
                                    <div class="product-tab e-tabs">
                                        <ul class="tabs tabs-title clearfix">
                                            <li class="tab-link current">
                                                <h3>
                                                    <span>Mô tả</span>
                                                </h3>
                                            </li>
                                        </ul>
                                        <div id="tab-1" class="tab-content current">
                                            <div class="product-well">
                                                <div class="ba-text-fpt" id="fancy-image-view">
                                                    <img alt="{!! $proDetail->product_name !!}" src="{!! $proDetail->image !!}" data-thumb="original" original-height="342" original-width="640">
                                                    {!! $proDetail->description !!}
                                                </div>
                                                <div class="show-more">
                                                    <a class="btn btn-default btn--view-more">
                                                        <span class="more-text">Xem thêm <i class="fa fa-chevron-down"></i>
                                                        </span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="sidebar sidebar-aside col--12 col-sm-12 col-md-3 col-lg-3">
                                <aside class="aside-item clearfix">
                                    <div class="right_module margin-top-10">
                                        <div class="module_service_details">
                                            <div class="wrap_module_service">
                                                <div class="item_service">
                                                    <div class="wrap_item_">
                                                        <div class="content_service">
                                                            <p>Giao hàng tận nơi</p>
                                                            <span>Miễn phí vận chuyển với đơn hàng trên 300.000 đ</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="item_service">
                                                    <div class="wrap_item_">
                                                        <div class="content_service">
                                                            <p>Thanh toán khi nhận</p>
                                                            <span>Bạn thoải mái nhận &amp; kiểm tra hàng trước khi trả tiền.</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="item_service">
                                                    <div class="wrap_item_">
                                                        <div class="content_service">
                                                            <p>Bảo đảm chất lượng</p>
                                                            <span>Chất lượng sản phẩm luôn được đặt lên hàng đầu</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </aside>
                                <aside class="aside-item collection-category">
                                    <div class="aside-title">
                                        <h3 class="title-head">
                                            <span>Danh mục sản phẩm</span>
                                        </h3>
                                    </div>
                                    <div class="aside-content">
                                        <nav class="nav-category navbar-toggleable-md">
                                            <ul class="nav navbar-pills">
                                                @foreach($category as $cate)
                                                    <li class="nav-item">
                                                        <a class="nav-link" href="{{ route('listCake', $cate->id) }}">{{ $cate->name }}</a>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </nav>
                                    </div>
                                </aside>
                                <div class="pro-hotline-frame">
                                    <h3>Hotline:</h3>&nbsp;<a href="tel:0973 605 319">0973 605 319</a>
                                    <p>Liên hệ ngay cho chúng tôi để thưởng thức những chiếc bánh thơm ngon nhất</p>
                                </div>
                                <div class="pro-banner-frame d-none d-sm-none d-md-block d-lg-block">
                                    <a href="#">
                                        <img src="//bizweb.dktcdn.net/100/312/791/themes/667098/assets/aside_bannerpro.png?1535770651792" alt="Banner">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    @include('Frontend.ProductSelling.listProduct')
                </div>
            </div>
        </div>
    </section>
@stop()