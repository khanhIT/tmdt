@foreach($productItem as $item)
<article class="thumbnail item">
    <div class="product-thumbnail-frame">
        @if(isset($item->sale_price))
        <div class="sale-flash">SALE</div>
        @endif
        <a class="blog-thumb-img" href="{{ route('detailProduct', $item->product_name) }}" title="{{ $item->product_name }}">
            <img src="{{ $item->image }}" title="{{ $item->product_name }}" class="img-responsive" />
        </a>
    </div>
    <div class="caption">
        <h5 itemprop="headline" class="product-name">
            <a href="{{ route('detailProduct', $item->product_name) }}" rel="bookmark">{{ $item->product_name }}</a>
        </h5>
        <div class="price-box clearfix">
            @if(!empty($item->sale_price))
            <div class="special-price">
                <span class="price product-price">Giá: {{ number_format($item->sale_price, 3) }}₫</span>
            </div>

            <div class="old-price">
                <span class="price product-price-old">
                    {{ number_format($item->price, 3) }}₫
                </span>
            </div>
            @else
            <div class="old-price">
                <span class="price">
                    {{ number_format($item->price, 3) }}₫
                </span>
            </div>
            @endif
        </div>
        <div class="product-action clearfix">
            <form action="{{ route('addToCart') }}" method="post" class="variants form-nut-grid has-validation-callback" enctype="multipart/form-data">
                @csrf
                <div>
                    <input type="hidden" name="id" value="{{ $item->id }}">
                    <input type="hidden" name="product_name" value="{{ $item->product_name }}">
                    <input type="hidden" name="sale_price" value="{{ $item->sale_price }}">
                    <input type="hidden" name="price" value="{{ $item->price }}">
                    <input type="hidden" name="image" value="{{ $item->image }}">
                    <button class="add2cart btn-buy btn-cart btn btn-gray left-to btn-primary add_to_cart" title="Thêm vào giỏ">
                        <span>Thêm vào giỏ</span>
                    </button>
                </div>
            </form>
        </div>
    </div>
</article>
@endforeach()