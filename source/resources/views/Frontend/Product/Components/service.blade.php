<section class="awe-section-6"> 
    <section class="section-service margin-bottom-20">
        <div class="container">
            <div class="row section-service-frame">
                <div class="section-service-bg"></div>
                <div class="col-sm-4 col-xs-12">
                    <h3>Bảo đảm chất lượng</h3>
                    <p>Nguyên liệu được chọn lọc và xử lý trong quy trình khép kín, đảm bảo chất lượng và an toàn vệ sinh thực phẩm</p>
                </div>
                <div class="col-sm-4 col-xs-12">
                    <h3>Hương vị tự nhiên</h3>
                    <p>Chỉ sử dụng nguyên liệu tự nhiên, mang đến một sản phẩm thơm ngon mà không mất đi bản sắc tự nhiên vốn có</p>
                </div>
                <div class="col-sm-4 col-xs-12">
                    <h3>Công thức đặc biệt</h3>
                    <p>Sản phẩm được làm từ những công thức đặc biệt đã trải qua nhiều năm nghiên cứu và phát triển</p>
                </div>             
            </div>
        </div>
    </section>
</section>