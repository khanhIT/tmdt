<section class="awe-section-5">
    <section class="section-selling margin-bottom-20">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 selling-content">
                    <div class="heading a-center">
                        <h2 class="title-head">
                            <a href="#">Tất Cả Sản phẩm</a>
                        </h2>
                    </div>
                    <div id="owl-demo" class="owl-carousel owl-theme tab-1 tab-content current">
                        <!-- list item product -->
                        @include('Frontend.Product.Components.listItem')
                        <!-- list item product -->
                    </div>
                </div>
            </div>
        </div>
    </section>
</section>

<!-- list service -->
@include('Frontend.Product.Components.service')
<!-- list service -->
