@extends('Layouts.main')

@section('meta')
	<title>Bánh Cookies - LuzBakery</title>
@stop()

@section('content')
	<div class="module-title">
		<div class="bg-frame"></div>
		<h2>
			<a href="javascript:void(0);">
				Bánh Cookies
			</a>
		</h2>
		<section class="bread-crumb margin-bottom-10">
			<div class="container">
				<div class="row">
					<div class="col-xs-12" style="width: 100%">
						<ul class="breadcrumb">
							<li class="home">
								<a itemprop="url" href="/" title="Trang chủ">
									<span itemprop="title">Trang chủ</span>
								</a>
								<span><i class="fa fa-angle-right"></i></span>
							</li>
							<li>Bánh Cookies</li>
						</ul>
					</div>
				</div>
			</div>
		</section>
	</div>
	<div class="container">
		<div class="row">
			<!--aside-->
			@include('Elements.Layout.aside')
			<!--aside-->
			<section class="main_container collection col-lg-9 col-md-9 col-lg-push-3 col-md-push-3">
				<h1 class="title-head margin-top-0">Bánh Cookies</h1>		
				<div class="category-products products">
					<div class="sortPagiBar">
						<div class="row">
							<div class="hidden-xs col-sm-6"></div>
							@include('Elements.Layout.sort')
						</div>
					</div>	
					<section class="products-view products-view-gird">
						<div class="row">
							@foreach($categoryItem as $cate)
							<div class="col-xs-6 col-sm-4 col-lg-4">
								<div class="product-box">
									<div class="product-thumbnail-frame">
										@if(isset($cate->sale_price))
										<div class="sale-flash">SALE</div>
										@endif
										<a href="{{ route('detailProduct', $cate->product_name) }}">
											<img class="img-responsive" src="{{ $cate->image }}" data-lazyload="{{ $cate->image }}" alt="{{ $cate->product_name }}">
										</a>	
									</div>	
									<div class="product-info">
										<h5 class="product-name">
											<a href="{{ route('detailProduct', $cate->product_name) }}" title="{{ $cate->product_name }}">{{ $cate->product_name }}</a>
										</h5>		
										<div class="price-box clearfix">
											@if(!empty($cate->sale_price))
											<div class="special-price">
												<span class="price product-price">Giá: {{ number_format($cate->sale_price, 3) }}₫</span>
											</div>		
											<div class="old-price">                                
		                                        <span class="price product-price-old">
		                                           {{ number_format($cate->price, 3) }}₫
		                                        </span>
		                                    </div>
											@else
											<div class="old-price">
												<span class="price">
												   {{ number_format($cate->price, 3) }}₫
												</span>
											</div>
											@endif
										</div>
										<div class="product-action clearfix">
											<form action="{{ route('addToCart') }}" method="post" class="variants form-nut-grid has-validation-callback" enctype="multipart/form-data">
												@csrf
												<div>
													<input type="hidden" name="id" value="{{ $cate->id }}">
													<input type="hidden" name="product_name" value="{{ $cate->product_name }}">
													<input type="hidden" name="sale_price" value="{{ $cate->sale_price }}">
													<input type="hidden" name="price" value="{{ $cate->price }}">
													<input type="hidden" name="image" value="{{ $cate->image }}">
													<button class="add2cart btn-buy btn-cart btn btn-gray left-to btn-primary add_to_cart" title="Thêm vào giỏ"><span>
														Thêm vào giỏ</span>
													</button>
												</div>
											</form>
										</div>		
									</div>	
								</div>
							</div>
							@endforeach
						</div>
					</section>
					{{--paginate--}}
					<div class="shop-pag text-xs-right">
						<nav class="pagination-frame">
							  <ul class="pagination clearfix">
								  {{ $categoryItem->links() }}
							  </ul>
						</nav>
					</div>
				</div>
			</section>
		</div>
	</div>
@stop()