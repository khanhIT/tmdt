@extends('Layouts.main')

@section('meta')
    <title>Đăng nhập - LuzBakery</title>
@stop()

@section('content')
    <div class="module-title">
        <div class="bg-frame"></div>
        <h2>
            <a href="javascript:void(0);">
                Đăng Nhập
            </a>
        </h2>
        <section class="bread-crumb margin-bottom-10">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12" style="width: 100%">
                        <ul class="breadcrumb">
                            <li class="home">
                                <a itemprop="url" href="/" title="Trang chủ">
                                    <span itemprop="title">Trang chủ</span>
                                </a>
                                <span><i class="fa fa-angle-right"></i></span>
                            </li>
                            <li>Đăng nhập tài khoản</li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <div class="container margin-bottom-30">
        <div class="row">
            <div class="col-md-6 offset-md-3">
                @include('Frontend.Error.success')
                @include('Frontend.Error.errors')
                <div class="page-login account-box-shadow">
                    <div id="login">
                        <div class="text-center">
                            <h1 class="title-head"><span>Đăng nhập tài khoản</span></h1>
                        </div>
                        <form accept-charset="UTF-8" action="{{ route('dang_nhap') }}" method="post" class="has-validation-callback">
                            @csrf
                            <div class="form-signup" style="color: #e00;">
                            </div>
                            <div class="form-signup clearfix">
                                <fieldset class="form-group {{ !empty($errors->first('email')) ? 'has-error' : '' }}">
                                    <label>Email<span class="required">*</span></label>
                                    <input type="email" class="form-control form-control-lg" value="{{ old('email') }}" name="email" placeholder="Email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,63}$">
                                    <span class="help-block">{{ !empty($errors->first('email')) ? $errors->first('email') : '' }}</span>
                                </fieldset>
                                <fieldset class="form-group {{ !empty($errors->first('password')) ? 'has-error' : '' }}">
                                    <label>Mật khẩu<span class="required">*</span></label>
                                    <input type="password" class="form-control form-control-lg" value="{{ old('password') }}" name="password" placeholder="Mật khẩu">
                                    <span class="help-block">{{ !empty($errors->first('password')) ? $errors->first('password') : '' }}</span>
                                </fieldset>
                                 <div class="checkbox">
                                  <label>
                                      <input type="checkbox" class="remember" value="1"> Nhớ tài khoản tôi?
                                  </label>
                                </div>
                                <div class="pull-xs-left text-center" style="margin-top: 15px;">
                                    <button class="btn btn-style btn-blues" type="submit" value="Đăng nhập">Đăng nhập</button>
                                </div>
                                <div class="clearfix"></div>
                                <p class="text-center" style="border-bottom: 1px solid #ddd; padding-bottom: 15px;">
                                    <a href="#" class="btn-link-style" style="margin-top: 15px;font-weight: 600; color: #222; display: inline-block; text-decoration: none;">Quên mật khẩu?</a>
                                </p>
                                <div class="text-login text-center">
                                    <p>
                                        Bạn chưa có tài khoản. Đăng ký <a href="{{ route('dang_ky') }}" style="color: #777777;">tại đây.</a>
                                    </p>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div id="recover-password" class="form-signup" style="display:none;">
                        <div class="text-center">
                            <h1 class="title-head">
                                <span>Đặt lại mật khẩu</span>
                            </h1>
                        </div>
                        <span class="block text-center">
								Bạn quên mật khẩu? Nhập địa chỉ email để lấy lại mật khẩu qua email.
							</span>
                        <form accept-charset="UTF-8" action="/account/recover" id="recover_customer_password" method="post" class="has-validation-callback">
                            <input name="FormType" type="hidden" value="recover_customer_password">
                            <input name="utf8" type="hidden" value="true">
                            <div class="form-signup aaaaaaaa" style="color: #e00;">

                            </div>

                            <div class="form-signup clearfix">
                                <fieldset class="form-group">
                                    <label>Email<span class="required">*</span></label>
                                    <input type="email" class="form-control form-control-lg" value="" name="Email" id="recover-email" placeholder="Email" data-validation="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,63}$" data-validation-error-msg="Email sai định dạng">
                                </fieldset>
                            </div>
                            <div class="action_bottom text-center">
                                <button class="btn btn-style btn-blues" style="margin-top: 15px;" type="submit" value="Lấy lại mật khẩu">Lấy lại mật khẩu</button>
                            </div>
                            <div class="text-login text-center">
                                <p>Quay lại <a href="javascript:;" class="btn-link-style btn-register" onclick="hideRecoverPasswordForm();">tại đây.</a></p>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop()
