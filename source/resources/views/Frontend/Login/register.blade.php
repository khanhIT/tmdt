@extends('Layouts.main')

@section('meta')
    <title>Đăng ký - LuzBakery</title>
@stop()

@section('stylesheetAddon')
<style>
    .select2-container .select2-selection--single{
        height: 36px !important;
    }
    .select2-container--default .select2-selection--single {
        border-radius: 25px !important;
    }
</style>
@stop()

@section('content')
    <div class="module-title">
        <div class="bg-frame"></div>
        <h2>
            <a href="javascript:void(0);">
                Đăng Ký
            </a>
        </h2>
        <section class="bread-crumb margin-bottom-10">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12" style="width: 100%">
                        <ul class="breadcrumb" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
                            <li class="home">
                                <a itemprop="url" href="/" title="Trang chủ"><span itemprop="title">Trang chủ</span></a>
                                <span><i class="fa fa-angle-right"></i></span>
                            </li>
                            <li>Đăng Ký</li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <div class="container mr-bottom-20">
        <div class="row">
            <div class="col-md-6 offset-md-3">
                <div class="page-login account-box-shadow">
                    <div id="login">
                        <h1 class="title-head text-center">Đăng ký tài khoản</h1>
                        <div class="text-center">
                            <span>Nếu chưa có tài khoản vui lòng đăng ký tại đây</span>
                        </div>
                        <form action="{{ route('dang_ky') }}" method="post" role="form" enctype="multipart/data">
                            {{ csrf_field() }}
                            <div class="form-signup clearfix">
                                <div class="row">
                                    <div class="col-md-12">
                                        <fieldset class="form-group {{ !empty($errors->first('first_name')) ? 'has-error' : '' }}">
                                            <label>Họ<span class="required">*</span></label>
                                            <input type="text" class="form-control form-control-lg" name="first_name" value="{{ old('first_name') }}" placeholder="Họ">
                                             <span class="help-block">{{ !empty($errors->first('first_name')) ? $errors->first('first_name') : '' }}</span>
                                        </fieldset>
                                    </div>
                                    <div class="col-md-12">
                                        <fieldset class="form-group {{ !empty($errors->first('last_name')) ? 'has-error' : '' }}">
                                            <label>Tên<span class="required">*</span></label>
                                            <input type="text" class="form-control form-control-lg" name="last_name" value="{{ old('last_name') }}" placeholder="Tên">
                                            <span class="help-block">{{ !empty($errors->first('last_name')) ? $errors->first('last_name') : '' }}</span>
                                        </fieldset>
                                    </div>
                                    <div class="col-md-12">
                                        <fieldset class="form-group {{ !empty($errors->first('email')) ? 'has-error' : '' }}">
                                            <label>Email<span class="required">*</span></label>
                                            <input type="email" class="form-control form-control-lg" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,63}$" name="email" value="{{ old('email') }}" placeholder="Ex: exam.gmail.com">
                                            <span class="help-block">{{ !empty($errors->first('email')) ? $errors->first('email') : '' }}</span>
                                        </fieldset>
                                    </div>
                                    <div class="col-md-12">
                                        <fieldset class="form-group {{ !empty($errors->first('phone')) ? 'has-error' : '' }}">
                                            <label>Phone<span class="required">*</span></label>
                                            <input type="number" class="form-control form-control-lg" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,63}$" name="phone" value="{{ old('phone') }}" placeholder="123.456.789">
                                            <span class="help-block">{{ !empty($errors->first('phone')) ? $errors->first('phone') : '' }}</span>
                                        </fieldset>
                                    </div>
                                    <div class="col-md-12">
                                        <fieldset class="form-group {{ !empty($errors->first('password')) ? 'has-error' : '' }}">
                                            <label>Mật khẩu<span class="required">*</span></label>
                                            <input type="password" class="form-control form-control-lg" name="password" value="{{ old('password') }}" placeholder="******">
                                            <span class="help-block">{{ !empty($errors->first('password')) ? $errors->first('password') : '' }}</span>
                                        </fieldset>
                                    </div>
                                    <div class="col-md-12">
                                        <fieldset class="form-group">
                                            <label>Thành phố
                                                <span class="required">*</span>
                                            </label>
                                            <select name="province_id" id="province" class="form-control{{ !empty($errors->first('province_id')) ? 'has-error' : '' }}">
                                                <option value="">-- Chọn thành phố --</option>
                                                @foreach($province as $pro)
                                                <option {{ old('province_id') == $pro['id'] ? "selected" : "" }}  value="{{ $pro->id }}">{{ $pro->name }}</option>
                                                @endforeach
                                            </select>
                                            <span class="help-block">{{ !empty($errors->first('province_id')) ? $errors->first('province_id') : '' }}</span>
                                        </fieldset>
                                    </div>
                                    <div class="col-md-12">
                                        <fieldset class="form-group">
                                            <label>Quận huyện
                                                <span class="required">*</span>
                                            </label>
                                            <select name="district_id" id="district" class="form-control {{ !empty($errors->first('district_id')) ? 'has-error' : '' }}">
                                                <option value="">-- Chọn quận huyện --</option>
                                                @foreach($district as $dis)
                                                <option {{ old('district_id') == $dis['id'] ? "selected" : "" }}  value="{{ $dis->id }}">{{ $dis->name }}</option>
                                                @endforeach
                                            </select>
                                            <span class="help-block">{{ !empty($errors->first('district_id')) ? $errors->first('district_id') : '' }}</span>
                                        </fieldset>
                                    </div>
                                    <div class="col-md-12 text-center" style="margin-top:15px; padding: 0">
                                        <button type="submit" value="Đăng ký" class="btn btn-style btn-blues">Đăng ký</button>
                                        <a href="{{ route('dang_nhap') }}" class="btn-link-style btn-register btn-style-login" style="margin-left: 20px; color: #1c011f; text-decoration: underline;">Đăng nhập</a>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop()
@section('scriptAddon')
<script>
    $(document).ready(function() {
        $('#district').select2();
        $('#province').select2();

        $("#province").change(function() {
                var province =$(this).val();

                $.get("name/" + province, function (data) {
                    $('#district').html(data);
                });
            });
    });
</script>
@stop
