<section class="awe-section-4">	
	<section class="section-category margin-bottom-20">
		<div class="container">
			<div class="category-content">						
				<div class="list-category-link">				
					<div class="col-12 section-category-frame">
						<div class="section-category-img">						
							<a href="/banh-cookies" title="Bánh Cookies">
								<img class="img-responsive center-block" src="{{ asset('frontend/images/cookies-me-hat-chia-400x400.jpg') }}" data-lazyload="{{ asset('frontend/images/cookies-me-hat-chia-400x400.jpg') }}" alt="Bánh Cookies">
							</a>					
						</div>
						<div class="section-category-info">
							<h3>
								<a href="/banh-cookies" title="Bánh Cookies">Bánh Cookies</a>
							</h3>
							<div class="category-content">
								<p>Những chiếc bánh cookie&nbsp;bắt nguồn từ xứ sở của hoa Tulip với cái tên là “Koekje”. Tuy nhiên, những chiếc bánh cookie chỉ thật sự nổi tiếng khi được nước Mỹ “quảng cáo”, chúng được chế biến cùng với chocolate chip để tăng thêm hương vị ngọt...</p>
							</div>
							<div class="sec-cate-a">
								<svg class="arrow-left" width="18px" height="17px" viewBox="0 0 18 17" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
									<g transform="translate(8.500000, 8.500000) scale(-1, 1) translate(-8.500000, -8.500000)">
										<polygon class="arrow" points="16.3746667 8.33860465 7.76133333 15.3067621 6.904 14.3175671 14.2906667 8.34246869 6.908 2.42790698 7.76 1.43613596"></polygon>
										<polygon class="arrow-fixed" points="16.3746667 8.33860465 7.76133333 15.3067621 6.904 14.3175671 14.2906667 8.34246869 6.908 2.42790698 7.76 1.43613596"></polygon>
										<path d="M-1.48029737e-15,0.56157424 L-1.48029737e-15,16.1929159 L9.708,8.33860465 L-2.66453526e-15,0.56157424 L-1.48029737e-15,0.56157424 Z M1.33333333,3.30246869 L7.62533333,8.34246869 L1.33333333,13.4327013 L1.33333333,3.30246869 L1.33333333,3.30246869 Z"></path>
									</g>
								</svg>
								<a class="" href="/banh-cookies">								
									Thưởng Thức Ngay
								</a>														
							</div>
						</div>										
					</div>									
				</div>
			</div>
		</div>
	</section>
</section>