@if(session('error'))
    <div class="alert alert-warning alert-dismissible" role="alert" style="border-radius: 0">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        {{ session('error') }}
    </div>
@endif