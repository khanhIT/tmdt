@extends('Layouts.main')

@section('meta')
    <title>Lịch sử mua hàng - LuzBakery</title>
@stop()

@section('content')
    <div class="module-title">
        <div class="bg-frame"></div>
        <h2>
            <a href="javascript:void(0);">
                Lịch sử mua hàng
            </a>
        </h2>
        <section class="bread-crumb margin-bottom-10">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12" style="width: 100%">
                        <ul class="breadcrumb">
                            <li class="home">
                                <a itemprop="url" href="/" title="Trang chủ">
                                    <span itemprop="title">Trang chủ</span>
                                </a>
                                <span><i class="fa fa-angle-right"></i></span>
                            </li>
                            <li>Lịch sử</li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <div class="container margin-bottom-30">
        <div class="row">
            <div class="col-md-12 offset-md-12">
                <table class="table">
                    <caption>Lịch sử đơn hàng </caption>
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Hình ảnh sản phẩm</th>
                            <th scope="col">Số lượng</th>
                            <th scope="col">Đơn giá</th>
                        </tr>
                    </thead>
                    <tbody>
                    @php $i = 1; @endphp
                    @foreach($orders as $order)
                        <tr class="bg-warning">
                            <th scope="row">{{ $i }}</th>
                            <td>
                                <img src="{{ $order->image }}" alt="{{ $order->product_name }}" style="width: 100px; height: 100px" />
                            </td>
                            <td>{{ $order->qty }}</td>
                            <td>
                                {{ $order->total_mount }}
                            </td>
                        </tr>
                    @php $i++; @endphp    
                    @endforeach()    
                    </tbody>
                </table> 
            </div>
        </div>
    </div>
@stop()
