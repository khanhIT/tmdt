@extends('Layouts.main')

@section('meta')
	<title>Liên hệ - LuzBakery</title>
@stop()

@section('content')
	<div class="module-title">
		<div class="bg-frame"></div>
		<h2>
			<a href="javascript:void(0);">
				Liên hệ
			</a>
		</h2>
		<section class="bread-crumb margin-bottom-10">
			<div class="container">
				<div class="row">
					<div class="col-xs-12" style="width: 100%">
						<ul class="breadcrumb" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
							<li class="home">
								<a itemprop="url" href="/" title="Trang chủ"><span itemprop="title">Trang chủ</span></a>
								<span><i class="fa fa-angle-right"></i></span>
							</li>
							<li>Liên hệ</li>
						</ul>
					</div>
				</div>
			</div>
		</section>
	</div>
	<div class="container contact">
		<div class="row">
			<div class="col-lg-6 col-md-6 col-sm-12 col-12">
				<div class="sec-page-contact">
					<h1>Thông tin liên hệ</h1>
					<table>
						<tbody><tr>
							<td><i class="fa fa-home"></i></td>
							<td>Số 280 Cổ Nhuế- Bắc Từ Liêm - Hà Nội</td>
						</tr>
						<tr>
							<td><i class="fa fa-phone"></i></td>
							<td><a href="tel:0123456789">0973 605 319</a></td>
						</tr>
						<tr>
							<td><i class="fa fa-envelope"></i></td>
							<td><a href="mailto:khanhhoang22051996@gmail.com">khanhhoang22051996@gmail.com</a></td>
						</tr>
					</tbody></table>
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-12 col-12">
				<div class="sec-map-contact">
					<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d658.1715832048664!2d105.77940251399231!3d21.065443182666655!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3134552b772f8fc5%3A0xa0c61a0ef996f668!2zMjgwIEPhu5UgTmh14bq_LCBU4burIExpw6ptLCBIw6AgTuG7mWksIFZp4buHdCBOYW0!5e0!3m2!1svi!2s!4v1536169110938" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
				</div>
			</div>
			<div class="col-lg-12 col-md-12 col-sm-12 col-12 margin-top-15">
				<div class="page-login">
					<div id="login">
						<h3 class="title-head text-center">Gửi thông tin</h3>
						<span class="text-center block"></span>
						<span class="text-center margin-bottom-10 block">Bạn hãy điền nội dung tin nhắn vào form dưới đây và gửi cho chúng tôi. Chúng tôi sẽ trả lời bạn sau khi nhận được.</span>
						<form accept-charset="UTF-8" action="{{ route('contact') }}" method="post" class="has-validation-callback">
							@csrf
							<div class="form-signup clearfix">
								<div class="row">
									<div class="col-md-4 col-sm-12 col-12">
										<fieldset class="form-group {{ !empty($errors->first('full_name')) ? 'has-error' : '' }}">
											<label>Họ tên<span class="required">*</span></label>
											<input type="text" name="full_name" class="form-control form-control-lg" value="{{ old('full_name') }}">
											<span class="help-block form-error">{{ !empty($errors->first('full_name')) ? $errors->first('full_name') : '' }}</span>
										</fieldset>
									</div>
									<div class="col-md-4 col-sm-12 col-12">
										<fieldset class="form-group {{ !empty($errors->first('email')) ? 'has-error' : '' }}">
												<label>Email<span class="required">*</span></label>
												<input type="email" name="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,63}$" class="form-control form-control-lg" value="{{ old('email') }}">
												<span class="help-block form-error">{{ !empty($errors->first('email')) ? $errors->first('email') : '' }}</span>
										</fieldset>
									</div>
									<div class="col-md-4 col-sm-12 col-12">
										<fieldset class="form-group {{ !empty($errors->first('phone')) ? 'has-error' : '' }}">
											<label>Điện thoại<span class="required">*</span></label>
											<input type="number" name="phone" class="form-control form-control-lg" value="{{ old('phone') }}">
											<span class="help-block form-error">{{ !empty($errors->first('phone')) ? $errors->first('phone') : '' }}</span>
										</fieldset>
									</div>
									<div class="col-sm-12 col-12">
										<fieldset class="form-group {{ !empty($errors->first('content')) ? 'has-error' : '' }}">
											<label>Nội dung<span class="required">*</span></label>
											<textarea name="content" class="form-control form-control-lg" rows="5">{{ old('content') }}</textarea>
											<span class="help-block form-error">{{ !empty($errors->first('content')) ? $errors->first('content') : '' }}</span>
										</fieldset>
										<div class="a-center" style="margin-top:20px; margin-bottom: 20px;">
											<button type="submit" class="btn btn-blues btn-style btn-style-active">Gửi tin nhắn</button>
										</div>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
@stop
