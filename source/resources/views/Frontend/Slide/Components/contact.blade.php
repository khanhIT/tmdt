<section class="awe-section-2"> 
    <section class="section-contact">
        <div class="container">
            <ul>
                <li class="sec-ct-hotline">
                    <a href="tel:0123456789">
                        <i class="fa fa-phone"></i> 0973 605 319
                    </a>         
                </li>
                <li class="sec-ct-email">
                    <a href="mailto:luz.elioteam@gmail.com">
                        <i class="fa fa-envelope"></i> khanhhoang220596@gmail.com
                    </a> 
                </li>
                <li>
                    <a href="#" target="_blank">
                        <i class="fa fa-facebook"></i>
                    </a>  
                </li>
                <li>
                    <a href="#" target="_blank">
                        <i class="fa fa-instagram"></i>
                    </a> 
                </li>
                <li>
                    <a href="#" target="_blank">
                        <i class="fa fa-google"></i>
                    </a>    
                </li>
            </ul>
        </div>
    </section>
</section>