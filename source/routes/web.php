<?php

Route::group(['namespace' => 'Frontend', 'middleware' => 'web'], function () {
      Route::get('/', 'HomeController@index')->name('index');
      Route::post('/cart', 'HomeController@addToCart')->name('addToCart');

      //search
      Route::get('/search', 'HomeController@searchByName')->name('search');

      //introduction
      Route::get('/gioi-thieu', 'IntroductionController@index')->name('intro');

      //Product
      Route::get('/tat-ca-san-pham', 'ProductController@index')->name('product');
      Route::get('danh-muc/{id}', 'ProductController@listCake')->name('listCake');
      Route::get('/san-pham/{slug}', 'ProductController@detailProduct')->name('detailProduct');

      //Recipe
      Route::get('/cong-thuc', 'RecipesController@index')->name('recipe');
      Route::get('/cong-thuc/{id}', 'RecipesController@detailRecipe')->name('detailRecipe');
      Route::post('/cong-thuc/comment', 'RecipesController@comment')->name('commentRecipe');

      //Contact
      Route::get('/lien-he', 'ContactController@index')->name('contact');
      Route::post('/lien-he', 'ContactController@store')->name('contact');
      Route::view('/thankyou', 'Frontend/Contact/thanks');

      //login and register
      Route::get('/dang-nhap', 'LoginController@login')->name('dang_nhap');
      Route::post('/dang-nhap', 'LoginController@postLogin')->name('dang_nhap');
      Route::get('/dang-ky', 'LoginController@register')->name('dang_ky');
      Route::post('/dang-ky', 'LoginController@postRegister')->name('dang_ky');
      Route::get('/name/{id}', 'LoginController@checkName');
      Route::get('/dang_xuat', 'LoginController@logout')->name('dang_xuat');
      Route::get('/active-customer/{token}', 'LoginController@activeCustomer')->name('dang_nhap.active');


      //cart
      Route::get('/gio-hang', 'CartController@index')->name('cart.index');
      Route::get('/empty', function(){
          \Cart::destroy();
      });
      Route::delete('/{id}', 'CartController@destroy')->name('cart.destroy');
      Route::patch('gio-hang/{id}', 'CartController@update')->name('cart.update');
      Route::get('/checkout', 'CartController@checkout')->name('cart.checkout')->middleware('customer');
      Route::post('/checkout', 'CartController@postCheckout')->name('cart.checkout');
      Route::get('/name/{id}', 'CartController@loadName');

    //   lịch sử đơn hàng
    Route::resource('history', 'HistoryOrderController');
});
