<?php

Auth::routes();

Route::group(['prefix' => 'admin', 'namespace' => 'Auth', 'middleware' => 'web'], function () {
    //login
    Route::get('login', 'LoginController@showLoginForm')->name('login');
    Route::post('login', 'LoginController@postLogin');
    //logout
    Route::get('logout', 'LoginController@logout')->name('logout');
    
});

Route::group(['prefix' => 'admin', 'namespace' => 'Backend'], function () {
    //active user
    Route::get('/active-user/{token}', 'UsersController@activeUser')->name('user.active');
    Route::get('/change-password/{token}', 'UsersController@changePassword')->name('user.change-password');
    Route::post('/change-password/{token}', 'UsersController@postChangePassword')->name('user.change-password');
    
    //multi languages 
    Route::get('lang/{lang}','LangController@changeLang')->name('lang');
});

//403 permission
Route::get('admin/not-found', function () {
    return view('Backend.Errors.404');
})->name('notFound');

//404 not found
Route::get('admin/403', function () {
    return view('Backend.Errors.403');
})->name('403');


Route::group(['prefix' => 'admin', 'namespace' => 'Backend', 'middleware' => 'auth'], function () {
    Route::get('/', 'BackendController@index')->name('dashboard');

    //customer
    Route::get('/khach-hang', 'CustomerController@index')->name('customer.index');
    Route::get('/khach-hang/{id}', 'CustomerController@show')->name('customer.show');

    //product
    Route::resource('/product', 'ProductController');
    Route::get('export', 'ProductController@export')->name('product.export');

    //category
    Route::resource('/category', 'CategoryController');

    //recipes
    Route::resource('/recipe', 'RecipesController');

    //contact
    Route::resource('/contact', 'ContactController');

    //orders
    Route::resource('/order', 'OrdersController');

    //payment
    Route::resource('/payment', 'PaymentController');

    //shipper
    Route::resource('/shipper', 'ShipperController');

    //role
    Route::resource('/role', 'RoleController');

    //user
    Route::resource('/user', 'UsersController');
    Route::post('/user/change-password/{id}', 'UsersController@changePass')->name('user.changePass');

    //system information
    Route::get('/system/info', 'SystemController@index')->name('system.index');
});
