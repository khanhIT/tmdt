$.ajaxSetup({
    headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
});

$('.comment').click(function (e) {
    e.preventDefault();
    let form = $('#recipe_comments');
    $.ajax({
       type: form.attr('method'),
       url: form.attr('action'),
       data: form.serialize(),
       success: function (data) {
            if (data.commentRecipe.original) {
                setTimeout(function () {
                    toastr.error('Validate error', 'Error Alert', { timeOut: 5000 });
                }, 500);
                if (data.commentRecipe.original.full_name) {
                    $('.errorFullName').css('display', 'block');
                    $('.errorFullName').text(data.commentRecipe.original.full_name);
                }

                if (data.commentRecipe.original.email) {
                    $('.errorEmail').css('display', 'block');
                    $('.errorEmail').text(data.commentRecipe.original.email);
                }

                if (data.commentRecipe.original.content) {
                    $('.errorContent').css('display', 'block');
                    $('.errorContent').text(data.commentRecipe.original.content);
                }

                if (data.commentRecipe.original.errors) {
                    $('.error').css('display', 'block');
                    $('.error').text(data.commentRecipe.original.errors);
                    $('.error').css('border', '1px dashed #f88d81');
                }
            } else {
                toastr.success('Successfully comment!', 'Success Alert', { timeOut: 5000 });
                $(".article-comment").append('<figure class="article-comment-user-image">' +
                    '<img src="https://www.gravatar.com/avatar/c9b25479cdcfed34dab4c4599e0163d9?s=110&amp;d=identicon" alt="binh-luan" class="block">' +
                    '</figure>' +
                    '<div class="article-comment-user-comment">' +
                    '<p class="user-name-comment"><strong>'+ data.commentRecipe.full_name +'</strong></p>' +
                    '<p>'+ data.commentRecipe.content +'</p>' +
                    '</div>');
            }
       }
    });
});