jQuery(document).ready(function($) {

	var owl = $("#owl-demo-2");
  owl.owlCarousel({
      loop:true,
      nav: true,
      margin: 10,
      animateIn: 'flipInX',
      items : 4,
      responsive:{
        0: {
            items: 1
        },

        992: {
            items: 4
        },

        1000: {
            items: 4
        }, 

        768: {
            items: 3
        },

        480: {
            items: 2
        },

        320: {
            items: 1
        }
      }
  });

  var owlProduct = $("#owl-demo");
  owlProduct.owlCarousel({
      loop:true,
      nav: true,
      margin: 10,
      animateIn: 'flipInX',
      items : 4,
      responsive:{
        0: {
            items: 1
        },

        992: {
            items: 4
        },

        1000: {
            items: 4
        }, 

        768: {
            items: 3
        },

        480: {
            items: 2
        },

        320: {
            items: 1
        }
      }
  });

  $('.awe-section-3 .section-selling .thumbnail.item').matchHeight();
	
});