$(document).ready(function(){
	$('.title-slide').textillate({ 

		in: { 
			// set the effect name
			effect: 'rollIn' ,

			// set the delay factor applied to each consecutive character
			delayScale: 1.5,

			// set the delay between each character
    		delay: 50,
    		sync: true
		},

		out: {
			effect: 'fadeOut',
			sync: true
		},

		loop: true
	});
});