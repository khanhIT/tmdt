jQuery(document).ready(function($) {
  var engine = new Bloodhound({
      remote: {
          url: 'api/search?key=%query%',
            wildcard: '%query%'
      },
      datumTokenizer: Bloodhound.tokenizers.whitespace('key'),
      queryTokenizer: Bloodhound.tokenizers.whitespace
  });

  $(".search-text").typeahead({
      hint: false,
      highlight: true,
      minLength: 1,
  }, {
      source: engine.ttAdapter(),
      name: 'product-name',
      templates: {
          empty: [
            '<div class="header-title">Tên sản phẩm</div><div class="list-group search-results-dropdown"><div class="list-group-item">Không có kết quả phù hợp</div></div>'
          ],
          header: [
            '<div class="header-title">Tên sản phẩm</div><div class="list-group search-results-dropdown">'
          ],
          suggestion: function (data) {
            return '<a href="san-pham/' + data.product_name + '" class="list-group-item" style="color: #000; text-align: center">' + data.product_name + '</a>'
          }
      }
  });
});
