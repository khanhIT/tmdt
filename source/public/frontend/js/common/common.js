jQuery(document).ready(function($){
	$('.fa-angle-down').click(function(){
		//category
		$('.dropdown-menu').toggle(500)
	});

	$('.sub_menu').click(function(){
		//menu bar
		$('.submenu').toggle(500)
	});

    //filter
	$('#sort-by li').hover(function(){
		$('#sort-by ul ul').css('display', 'block')
	}, function(){
		$('#sort-by ul ul').css('display', 'none')
	});

	//search
	$('#search-frame').click(function(){
		$('#search_form').slideToggle("slow");
	});

	//readmore
	$('.show-more a').on("click", function(){
		var $this = $(this);
		var $more = $('.show-more');
     	$more.addClass('hide');
        $this.parent().prev("#fancy-image-view").css({height: 'auto'});
	});

	//menu bar mobile
	$('.menu_bar').click(function(){
		$('.menu-mobile').toggle(500)
	});

	//active color menu
	$('ul .nav-item').click(function(){
	    $('.nav-item').removeClass("active");
	    $(this).addClass("active");
	});

	//back to top
	$('#back-to-top').on('click', function (e) {
        e.preventDefault();
        $('html,body').animate({
            scrollTop: 0
        }, 1000);
    });

});
