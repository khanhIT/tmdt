var role = {
  init: function() {
    this.moduleCheckbox = $('.role-permission').find('.module-name');
    this.screenCheckbox = $('.role-permission').find('.screen-name');
    this.itemCheckbox = $('.role-permission').find('.item-name');
    this.bindEvents();
  },

  bindEvents: function() {
    this.moduleCheckbox.on('click', this.handleCheckbox);
  },

  handleCheckbox: function() {
    var moduleName = $(this).attr('name');
    if($(this).is(':checked')){
        //enable screen name for checked
        $('.screen-name[data-module="'+ moduleName +'"]').prop('disabled', false);
        //enable screen item for checked
        if($('.item-name[data-module="'+ moduleName +'"]').is(':checked')){
            var screenName = $('.item-name[data-module="'+ moduleName +'"]').data('screen');
            if($('.screen-name[name="'+ screenName +'"]').is(':checked')){
                $('.item-name[data-module="'+ moduleName +'"]').prop('disabled', false);
            }
        }
    }else{
        $('.screen-name[data-module="'+ moduleName +'"]').prop('disabled', true);
        $(role.itemCheckbox).prop('disabled', true);
    }
  }
};

$(function() {
  role.init();
});
