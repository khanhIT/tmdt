var common = {
	init: function() {
		this.body = $("body");
		this.dataTable = $("#table");
		this.buttonClickDelete = $(".btn-delete");
        this.select2  = $('.category');
		this.bindEvents();
	},

	bindEvents: function() {
		this.DataTable();
    this.category();
		this.ajaxConfig();
		this.body.on('click', '.btn-delete', this.deleteRow);
    },

    category: function() {
        var select = common.select2;
        $(select).select2();
    },

	DataTable: function() {
		var dataTable = common.dataTable;
		$(dataTable).DataTable({
			 paginate   : false,
			 processing : true,
			 info       : false,
		});
	},

	ajaxConfig: function(){
        $.ajaxSetup({
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    },

	deleteRow: function(e) {
		e.preventDefault();
		var deleteURL = $(this).data('url');
		var reloadURL = $(this).data('url-reload');
		swal({
            title: "Bạn chắc chắn muốn xóa không?",
            text: "Item này sẽ bị xóa hoàn toàn, bạn sẽ không thể backup lại!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then((willDelete) => {
        	if(willDelete) {
        		$.ajax({
        			type : "delete",
        			url : deleteURL,
        			global : false,
        			success: function (data) {
                  switch(data.status_code){
                      case 401:
                          swal('Permission denied!', data.message,{
                              icon: "warning",
                          });
                          break;
                      case 500:
                          swal('Failed', data.message, {
                              icon: "warning",
                          });
                          break;
                      case 200:
                          swal('Deleted!', data.message, {
                              icon: "success",
                          });
                          if(reloadURL !== undefined){
                              setTimeout(function(){
                                  window.location.href = reloadURL;
                              }, 2500);
                          }else{
                          }
                          break;
                      default :
                          break;
                  }
              },
              error: function (error) {

              }
        		});
        	} else {

        	}
        });
	}
};

$(function () {
    common.init();
});

//preview image before
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#preview-image')
                .attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
}
