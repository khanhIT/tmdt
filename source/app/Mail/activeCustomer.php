<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class activeCustomer extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

     public $email;
     public $url_active;
     public $token;

    public function __construct($email, $url_active, $token)
    {
        $this->email = $email;
        $this->url_active = $url_active;
        $this->token = $token;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('khanhhoang220596@gmail.com', 'LuzBakery')
                    ->subject('[LuzBakery] Xác nhận active tài khoản trên LuzBakery')
                    ->markdown('Frontend.Mail.activeCustomer');
    }
}
