<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class sendmailContact extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    public $full_name;
    public $email;
    public $phone;
    public $content;
    public $content_reply;

    public function __construct($full_name, $email, $phone, $content, $content_reply)
    {
        $this->full_name = $full_name;
        $this->email = $email;
        $this->phone = $phone;
        $this->content = $content;
        $this->content_reply = $content_reply;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('luznotify@gmail.com', 'LuzBakery')
            ->subject('[LuzBakery] Thông tin phản hồi')
            ->markdown('Backend.email.sendMailContact');     
    }
}
