<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class sendOrder extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

     public $email;

    public function __construct($email)
    {
        $this->email = $email;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('khanhhoang220596@gmail.com', 'LuzBakery')
                    ->subject('[LuzBakery] Cảm ơn bạn đã đặt hàng trên LuzBakery')
                    ->markdown('Frontend.Mail.sendOrder');
    }
}
