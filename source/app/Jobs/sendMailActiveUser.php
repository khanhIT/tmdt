<?php

namespace App\Jobs;

use App\Mail\activeUsers;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Mail;

class sendMailActiveUser implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    protected $email;
    protected $url_active_user;
    protected $token;

    public function __construct($email, $url_active_user, $token)
    {
        $this->email = $email;
        $this->url_active_user = $url_active_user;
        $this->token = $token;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Mail::to($this->email)
            ->queue(new activeUsers($this->email, $this->url_active_user, $this->token));
    }
}
