<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Mail;
use App\Mail\sendMailContact;

class mailContact implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    public $full_name;
    public $email;
    public $phone;
    public $content;
    public $content_reply;

    public function __construct($full_name, $email, $phone, $content, $content_reply)
    {
        $this->full_name = $full_name;
        $this->email = $email;
        $this->phone = $phone;
        $this->content = $content;
        $this->content_reply = $content_reply;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Mail::to($this->email)
            ->queue(new sendMailContact($this->full_name, $this->email, $this->phone, $this->content, $this->content_reply));
    }
}
