<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Recipe extends Model
{

    protected $table = 'recipe';
    protected $dates = ['created_at', 'updated_at'];

    protected $guarded = array();
}
