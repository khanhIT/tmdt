<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Shipper extends Model
{

    protected $table = 'shipper';

    protected $guarded = array();
}
