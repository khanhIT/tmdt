<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class OrdersDetail extends Model
{

    protected $table = 'orders_detail';

    protected $guarded = array();
}
