<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{

    protected $table = 'role';

    protected $guarded = array();

    public function permission()
    {
    	return $this->belongsToMany(Permission::class, 'role_permission');
    }
}
