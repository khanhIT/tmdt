<?php

namespace App\Model;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Users extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $table = 'users';

    protected $guarded = array();

    public function role()
    {
    	return $this->belongsTo(Role::class);
    }

    public function hasPermission(Permission $permission)
    {
        return !! optional(optional($this->role)->permission)->contains($permission);
    }
}
