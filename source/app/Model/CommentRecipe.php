<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class CommentRecipe extends Model
{

    protected $table = 'comment_recipe';
    protected $dates = ['created_at', 'updated_at'];

    protected $guarded = array();

    public static function commentRecipe($request)
    {
        $validator = \Validator::make($request->all(), [
            'full_name' => 'required',
            'email'     => 'required|email',
            'content'   => 'required'
        ], [
            'full_name.required' => 'Bạn chưa nhập tên',
            'email.required'     => 'Bạn chưa nhập email',
            'email.email'        => 'Sai định dạng email',
            'content.required'   => 'Bạn chưa nhập nội dung'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        $exist = CommentRecipe::where('email', $request->email);
        if ($exist->exists()) {
            return response()->json(['errors' => 'Email này đã tồn tại']);
        }

        return CommentRecipe::create($request->all());
    }
}
