<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{

    protected $table = 'contact';

    protected $date = ['created_at', 'updated_at'];

    protected $guarded = array();
}
