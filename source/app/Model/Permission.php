<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{

    protected $table = 'permission';

    protected $guarded = array();

    public function roles()
    {
    	return $this->belongsToMany(Role::class, 'role_permission');
    }
}
