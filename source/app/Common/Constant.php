<?php

namespace App\Common;

class Constant
{
    /** category table */
    const CATEGORY = "category";

    /** contact table */
    const CONTACT = "contact";

    /** customer table */
    const CUSTOMER = "customer";

    /** district table */
    const DISTRICT = "district";

    /** orders table */
    const ORDERS = "orders";

    /** orders_detail table */
    const ORDERS_DETAIL = "orders_detail";

    /** payment table */
    const PAYMENT = "payment";

    /** product table */
    const PRODUCT = "product";

    /** province table */
    const PROVINCE = "province";

    /** recipe table */
    const RECIPE = "recipe";

    /** comment recipe table */
    const COMMENT_RECIPE = 'comment_recipe';

    /** role table */
    const ROLE = "role";

    /** shipper table */
    const SHIPPER = "shipper";

    /** users table */
    const USERS = "users";

    /**  user permission */
    const PERMISSION = "permissions";

    /** user permission group */
    const ROLE_PERMISSION = 'role_permissions';
}
