<?php

namespace App\Helpers;

class Helper
{
    public static function uploadFile($image, $uploadPath)
    {
        $imageName = uniqid() . '.' . $image->getClientOriginalExtension();
        if (!is_dir($uploadPath)) {
            mkdir($uploadPath, 0777, true);
        }
        $image->move($uploadPath, $imageName);
        return '/' . $uploadPath . '/' . $imageName;
    }

    public static function getImage($name, $dir, $request)
    {
        if ($request->hasFile($name)) {
            $image = $request->file($name);
            // Upload file
            $uploadPath = $dir;
            $image = self::uploadFile($image, $uploadPath);
            return $image;
        }
        return '';
    }
}
