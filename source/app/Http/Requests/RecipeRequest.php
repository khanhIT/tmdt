<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RecipeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'     => 'required|max:255',
            'thumbnail' => 'required|image|mimes:jpeg,png,jpg|max:5000',
            'author'    => 'required',
            'content'   => 'required',
        ];
    }

    public function messages()
    {
        return [
            'title.required'     => 'Bạn chưa nhập tiêu đề',
            'title.max'          => 'Tối đa 255 ký tự',
            'thumbnail.required' => 'Bạn chưa chọn file ảnh',
            'thumbnail.image'    => 'Bạn vui lòng chọn lại đúng file ảnh',
            'thumbnail.minmes'   => 'Chỉ chấp nhận những định dạng jpeg,png,jpg',
            'thumbnail.max'      => 'Tối đa 5M',
            'author.required'    => 'Bạn chưa nhập người viết bài',
            'content.required'   => 'Bạn chưa nhập nội dung'
        ];
    }
}
