<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductEditItemRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          'product_name' => 'required',
          'category_id'  => 'required',
          'price'        => 'required',
        ];
    }

    public function messages()
    {
        return [
            'product_name.required' => 'Bạn chưa nhập tên sản phẩm',
            'category_id.required'  => 'Bạn chưa chọn danh mục',
            'price.required'        => 'Bạn chưa nhập giá sản phẩm'
        ];
    }
}
