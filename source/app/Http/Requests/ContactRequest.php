<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ContactRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'full_name' => 'required',
            'email'     => 'required|email|unique:contact',
            'phone'     => 'required|max:10',
            'content'   => 'required'
        ];
    }

    public function messages()
    {
       return [
            'full_name.required' => 'Bạn chưa nhập tên',
            'email.required'     => 'Bạn chưa nhập email',
            'email.email'        => 'Sai định dạng email',
            'email.unique'       => 'Email đã tồn tại',
            'phone.required'     => 'Bạn chưa nhập số điện thoại',
            'phone.max'          => 'Tối đa 10 số',
            'content.required'   => 'Bạn chưa nhập nội dung'
       ];
    }
}
