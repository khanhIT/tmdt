<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ContactBackendRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'content_reply' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'content_reply.required' => 'Bạn chưa nhập nội dung trả lời'
        ];
    }
}
