<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name'  => 'required|max:15',
            'last_name'   => 'required|max:15',
            'email'       => 'required|email|unique:customer,email',
            'phone'       => 'required',
            'password'    => 'required|min:6',
            'province_id' => 'required',
            'district_id' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'first_name.required' => 'Bạn chưa nhập họ',
            'first_name.max' => 'Họ không quá 15 ký tự',
            'last_name.required' => 'Bạn chưa nhập tên',
            'last_name.max' => 'Tên bạn không quá 15 ký tự',
            'email.required' => 'Bạn chưa nhập email',
            'email.unique' => 'Email bạn đã đăng ký',
            'email.email' => 'Sai định dạng email',
            'phone.required' => 'Bạn chưa nhập số điện thoại',
            'password.required' => 'Bạn chưa nhập mật khẩu',
            'password.min' => 'Mật khẩu của bạn chưa đủ mạnh. Từ 6 ký tự trở lên',
            'province_id.required' => 'Bạn chưa chọn thành phố',
            'district_id.required' => 'Bạn chưa chọn quận huyện',
        ];
    }
}
