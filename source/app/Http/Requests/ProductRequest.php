<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'product_name' => 'required',
            'category_id'  => 'required',
            'price'        => 'required',
            'image'        => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:5120',
            'qty'          => 'required' 
        ];
    }

    public function messages()
    {
        return [
            'product_name.required' => 'Bạn chưa nhập tên sản phẩm',
            'category_id.required'  => 'Bạn chưa chọn danh mục',
            'price.required'        => 'Bạn chưa nhập giá sản phẩm',
            'image.required'        => 'Bạn chưa tải ảnh lên',
            'image.image'           => 'Bạn chỉ được tải file ảnh',
            'image.mimes'           => 'Không đúng định dạng. Hiện tại chúng tôi chỉ hỗ trợ tệp mở rộng jpeg,png,jpg,gif,svg',
            'image.max'             => 'Ảnh tối đã là 5M',
            'qty.required'          => 'Bạn chưa nhập số lượng'
        ];
    }
}
