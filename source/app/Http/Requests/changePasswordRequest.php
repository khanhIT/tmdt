<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class changePasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'password' => 'required|min:6',
            'confirmPassword' => 'required|same:password'
        ];
    }

    public function messages()
    {
        return [
            'password.required'         => 'Bạn chưa nhập mật khẩu',
            'password.min'              => 'Mật khẩu không nhỏ hơn 6 ký tự',
            'confirmPassword.required'  => 'Bạn chưa nhập lại mật khẩu',
            'confirmPassword.same'      => 'Mật khẩu không trùng khớp'
        ];
    }
}
