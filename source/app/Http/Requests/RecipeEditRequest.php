<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RecipeEditRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'     => 'required|max:255',
            'author'    => 'required',
            'content'   => 'required',
        ];
    }

    public function messages()
    {
        return [
            'title.required'     => 'Bạn chưa nhập tiêu đề',
            'title.max'          => 'Tối đa 255 ký tự',
            'author.required'    => 'Bạn chưa nhập người viết bài',
            'content.required'   => 'Bạn chưa nhập nội dung'
        ];
    }
}
