<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'     => 'required|max:100',
            'email'    => 'required|email:email|max:255|unique:users',
            'password' => 'required|min:6',
            'role_id'  => 'required',
        ];
    }

    public function messages()
    {
        return [
            'name.required'      => 'Vui lòng nhập tên',
            'name.max'           => 'Tên không được vượt quá 100 ký tự',
            'email.required'     => 'Vui lòng nhập email',
            'email.email'        => 'Không đúng định dạng email',
            'email.max'          => 'Không vượt quá 255 ký tự',
            'email.unique'       => 'Email đã tồn tại',
            'password.required'  => 'Vui lòng nhập mật khẩu',
            'password.min'       => 'Mật khẩu không dưới 6 ký tự',
            'role_id.required'   => 'Vui lòng chọn quyền',
        ];
    }
}
