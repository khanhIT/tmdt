<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Users\UsersInterface;
use App\Repositories\Role\RoleInterface;
use App\Http\Requests\UserRequest;
use App\Http\Requests\changePasswordRequest;
use Illuminate\Support\Facades\Hash;
use App\Helpers\Helper;
use DB;

class UsersController extends Controller
{

    private $user;
    private $role;

    public function __construct(
        UsersInterface $user,
        RoleInterface  $role
    ) {
        $this->user = $user;
        $this->role = $role;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $this->authorize('view-user');
        } catch (\Exception $e) {
            abort(403);
        }

        $user = $this->user->getListUser();

        return view('Backend.User.index', compact('user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $role = $this->role->all('id desc');
        return view('Backend.User.add', compact('role'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        $dataFormRequest = [
            'name'     => $request->name,
            'email'    => $request->email,
            'phone'    => $request->phone,
            'address'  => $request->address,
            'birthday' => $request->birthday,
            'token'    => $request->_token,
            'password' => Hash::make($request->password),
            'role_id'  => $request->role_id,
            'avatar'   => Helper::getImage('avatar', 'upload/avatar', $request),
        ];

        //insert user
        if ($this->user->create($dataFormRequest)) {
            $url_token = route('user.active', $request->_token);
            dispatch(new \App\Jobs\sendMailActiveUser($request->email, $url_token, $request->_token));
            return redirect()->route('user.index')->with('success', sprintf(config('message.create_success'), 'Người dùng'))->withInput();
        } else {
            DB::rollBack();
            return redirect()->back()->with('error', sprintf(config('message.create_failed'), 'người dùng'))->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $role = $this->role->all('id desc');
        $detailUser = $this->user->find($id);
        if (empty($detailUser)) {
            abort(404);
        }
        return view('Backend.User.detail', compact('detailUser', 'role'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $role = $this->role->all('id desc');
        $user = $this->user->find($id);
        if (empty($user)) {
            abort(404);
        }
        return view('Backend.User.edit', compact('user', 'role'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserRequest $request, $id)
    {
         $dataUpdateUser = [
            'name'     => $request->name,
            'email'    => $request->email,
            'phone'    => $request->phone,
            'address'  => $request->address,
            'birthday' => $request->birthday,
            'role_id'  => $request->role_id
         ];

        //update user
         try {
             DB::beginTransaction();
             $this->user->update($dataUpdateUser, $id, 'id');
             DB::commit();
             return redirect()->route('user.index')->with('success', sprintf(config('message.update_success'), 'Người dùng'))->withInput();
         } catch (\Exception $e) {
                DB::rollBack();
                return redirect()->back()->with('error', sprintf(config('message.update_failed'), 'người dùng'))->withInput();
            }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $message = config('message');
            if (\Auth::user()->role_id != 1) {
                return response()->json(['status_code' => 401, 'message' => $message['delete_permission_denied']]);
            }

            //update deleted_flag
            $this->user->update(['deleted_flag' => 1], $id);
            return response()->json(['status_code' => 200, 'message' => $message['delete_success']]);
        } catch (\Exception $ex) {
            return response()->json(['status_code' => 500, 'message' => $message['internal_server_error']]);
        }
    }

    public function changePass(changePasswordRequest $request, $id)
    {
        $data = [
            'password' => Hash::make($request->password)
        ];

        try {
            DB::beginTransaction();
            $this->user->update($data, $id, 'id');
            DB::commit();
            return redirect()->route('user.index')->with('success', sprintf(config('message.change_password'), ''))->withInput();
        } catch (\Exception $e) {
            DB::rollBack();
        }
    }

    public function activeUser($token)
    {
        $activation = $this->user->findBy('token', $token);
        if ($activation == null) {
            return redirect()->route('login');
        }else{
            $this->user->update(['active' => 1], $activation->id);
            return redirect()->route('user.change-password', $activation->token);
        }
    }

     public function changePassword($token)
     {
         return view('Backend.User.Component.changePassword.formChangePassword');
     }

     public function postChangePassword(Request $request, $token)
     {
         $dataUpdatePassword = [
             'password' => Hash::make($request->password),
             'token'   => null
         ];

         //update password user
         if ($this->user->update($dataUpdatePassword, $token, 'token')) {
             return redirect()->route('login');
         } else {
             return redirect()->back()->with('error', sprintf(config('message.update_failed'), 'mật khẩu'))->withInput();
         }
     }
}
