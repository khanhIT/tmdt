<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Contact\ContactInterface;
use App\Mail\sendMailContact;
use Mail;
use DB;
use Auth;
use App\Http\Requests\ContactBackendRequest;

class ContactController extends Controller
{

    private $contact;

    public function __construct(ContactInterface $contact){
        $this->contact=$contact;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $this->authorize('view-contact');
        } catch (\Exception $e) {
            abort(403);
        }

        $contacts = $this->contact->getListContact();

        return view('Backend.Contact.index', compact('contacts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('Backend.Contact.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $contact = $this->contact->detailContact($id);
        return view('Backend.Contact.edit', compact('contact'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ContactBackendRequest $request, $id)
    {
        $full_name = $request->full_name;
        $email = $request->email;
        $phone = $request->phone;
        $content = $request->content;
        $content_reply = $request->content_reply;

        $data = [
            'content_reply' => $request->content_reply
        ];
        
        try{
            DB::beginTransaction();
            $this->contact->update($data, $id);
            dispatch(new \App\Jobs\mailContact($full_name, $email, $phone, $content, $content_reply));
            DB::commit();
            return redirect()->route('contact.index')->with('success', 'Đã trả lời thành công')->withInput();
        }catch(\Exception $e) {
            DB::rollBack();
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $message = config('message');
            $contact = $this->contact->getById($id);
            $user = Auth::user();

            if ($user->role_id !== 1) {
                return response()->json(['status_code' => 401, 'message' => $message['delete_permission_denied']]);
            }

            //update delet_flag
            $this->contact->update(['deleted_flag' => 1], $id);
            return response()->json(['status_code' => 200, 'message' => $message['delete_success']]);
        } catch (Exception $ex) {
            return response()->json(['status_code' => 500, 'message' => $message['internal_server_error']]);
        }
    }
}
