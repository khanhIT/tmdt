<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Repositories\Recipe\RecipeInterface;
use App\Repositories\CommentRecipe\CommentRecipeInterface;
use App\Repositories\Category\CategoryInterface;
use App\Http\Requests\RecipeRequest;
use App\Http\Requests\RecipeEditRequest;
use App\Helpers\Helper;
use Auth;
use DB;

class RecipesController extends Controller
{

    private $recipe;
    private $category;
    private $comment;

    public function __construct(
        RecipeInterface $recipe,
        CategoryInterface $category,
        CommentRecipeInterface $comment
    ) {
        $this->recipe = $recipe;
        $this->category= $category;
        $this->comment= $comment;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $this->authorize('view-recipe');
        } catch (\Exception $e) {
            abort(403);
        }

        $recipes = $this->recipe->listRecipe();
        return view('Backend.Recipe.index', compact('recipes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Backend.Recipe.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RecipeRequest $request)
    {
        $dataFormRequest = [
            'user_id'     => Auth::user()->id,
            'title'       => $request->title,
            'slug'        => str_slug($request->title, '-'),
            'author'      => $request->author,
            'content'     => $request->content,
            'description' => $request->description,
            'thumbnail'   => Helper::getImage('thumbnail', 'upload/recipe', $request),
            'hot'         => $request->hot
        ];

        //insert recipe
        try {
            DB::beginTransaction();
            $this->recipe->create($dataFormRequest);
            DB::commit();
            return redirect()->route('recipe.index')->with('success', sprintf(config('message.create_success'), 'Công thức'))->withInput();
        } catch (\Exception $e) {
            DB::rollBack();
            $e->getMessage();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $recipe = $this->recipe->detailRecipe($id);
        $comment = $this->comment->getCommentRecipe($id);
        if (empty($recipe)) {
            abort(404);
        }
        return view('Backend.Recipe.show', compact('recipe', 'comment'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $recipe = $this->recipe->find($id);
        if (empty($recipe)) {
            abort(404);
        }

        return view('Backend.Recipe.edit', compact('recipe'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(RecipeEditRequest $request, $id)
    {
        $data = [
            'user_id'     => Auth::user()->id,
            'title'       => $request->title,
            'slug'        => str_slug($request->title, '-'),
            'author'      => $request->author,
            'content'     => $request->content,
            'description' => $request->description,
            'hot'         => $request->hot
        ];

        if ($request->hasfile('thumbnail')) {
            $data['thumbnail'] = Helper::getImage('thumbnail', 'upload/recipe', $request);
        }

        //update recipe
        try {
            DB::beginTransaction();
            $this->recipe->update($data, $id, 'id');
            DB::commit();
            return redirect()->route('recipe.index')->with('success', sprintf(config('message.update_success'), 'Công thức'))->withInput();
        } catch (\Exception $e) {
            DB::rollBack();
            $e->getMessage();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $message = config('message');
            $recipe = $this->recipe->getById($id);
            $user = Auth::user();
            if ($user->role_id !==1 && $recipe->user_id !== $user->id) {
                return response()->json(['status_code' => 401, 'message' => $message['delete_permission_denied']]);
            }

            //update deleted_flag
            $this->recipe->update(['deleted_flag' => 1], $id);
            return response()->json(['status_code' => 200, 'message' => $message['delete_success']]);
        } catch (\Exception $e) {
            return response()->json(['status_code' => 500, 'message' => $message['internal_server_error']]);
        }
    }
}
