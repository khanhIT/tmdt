<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Category\CategoryInterface;
use App\Http\Requests\CategoryRequest;
use DB;
use Auth;

class CategoryController extends Controller
{
    private $category;

    public function __construct(
        CategoryInterface $category
    ) {
        $this->category = $category;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $this->authorize('view-category');
        } catch (\Exception $e) {
            abort(403);
        }

        $category = $this->category->getListCategory();
       
        return view('Backend.Category.index', compact('category'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Backend.Category.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoryRequest $request)
    {
        $id = Auth::user()->id;
        $dataFormRequest = [
            'name' => $request->category_name,
            'user_id' => $id
        ];

        try {
            DB::beginTransaction();
            $category = $this->category->create($dataFormRequest);
            DB::commit();
            return redirect()->route('category.index')->with('success', sprintf(config('message.create_success'), 'Danh mục'))->withInput();
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->back()->with('error', sprintf(config('message.create_failed'), 'danh mục'))->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = $this->category->getEditCategory($id);

        return view('Backend.Category.edit', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CategoryRequest $request, $id)
    {
        $dataFormRequest = [
            'name' => $request->category_name
        ];

        try {
            DB::beginTransaction();
            $this->category->update($dataFormRequest, $id);
            DB::commit();

            return redirect()->route('category.index')->with('success', sprintf(config('message.update_success'), 'Danh mục'))->withInput();
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->back()->with('error', sprintf(config('message.update_failed'), 'danh mục'))->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $message = config('message');
            $category = $this->category->getById($id);
            $user = Auth::user();

            if ($user->role_id !== 1 && $category->user_id !== $user->id) {
                return response()->json(['status_code' => 401, 'message' => $message['delete_permission_denied']]);
            }

            //update delet_flag
            $this->category->update(['deleted_flag' => 1], $id);
            return response()->json(['status_code' => 200, 'message' => $message['delete_success']]);
        } catch (Exception $ex) {
            return response()->json(['status_code' => 500, 'message' => $message['internal_server_error']]);
        }
    }
}
