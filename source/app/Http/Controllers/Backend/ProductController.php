<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\ProductRequest;
use App\Http\Requests\ProductEditItemRequest;
use App\Repositories\Product\ProductInterface;
use App\Repositories\Category\CategoryInterface;
use App\Helpers\Helper;
use Auth;
use DB;
use Maatwebsite\Excel\Facades\Excel;

class ProductController extends Controller
{
    private $product;
    private $category;

    public function __construct(
        ProductInterface   $product,
        CategoryInterface  $category
    ) {
        $this->product   = $product;
        $this->category  = $category;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $this->authorize('view-product');
        } catch (\Exception $e) {
            abort(403);
        }
        
        $product = $this->product->listIndexItemProduct();
        return view('Backend.Product.index', compact('product'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category = $this->category->all('id desc');
        return view('Backend.Product.add', compact('category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductRequest $request)
    {
        $user_id = Auth::user()->id;
        $dataFormRequest = [
          'user_id'      => $user_id,
          'category_id'  => $request->category_id,
          'product_name' => $request->product_name,
          'price'        => $request->price,
          'sale_price'   => $request->sale_price,
          'qty'          => $request->qty,
          'description'  => $request->description,
          'hot'          => $request->hot,
          'status'       => $request->status,
          'image'        => Helper::getImage('image', 'upload/product', $request)
        ];

        // insert product
        try {
            DB::beginTransaction();
            $this->product->create($dataFormRequest);
            DB::commit();
            return redirect()->route('product.index')->with('success', sprintf(config('message.create_success'), 'Sản phẩm'))->withInput();
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->back()->with('error', sprintf(config('message.create_failed'), 'sản phẩm'))->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $productdetail = $this->product->detailProduct($id);
        if (empty($productdetail)) {
            abort(404);
        }
        return view('Backend.Product.show', compact('productdetail'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = $this->category->all('id desc');
        $productEdit = $this->product->find($id);
        if (empty($productEdit)) {
            abort(404);
        }
        return view('Backend.Product.edit', compact('category', 'productEdit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProductEditItemRequest $request, $id)
    {
        $user_id = Auth::user()->id;
        $data = [
        'user_id'      => $user_id,
        'category_id'  => $request->category_id,
        'product_name' => $request->product_name,
        'price'        => $request->price,
        'sale_price'   => $request->sale_price,
        'qty'          => $request->qty,
        'description'  => $request->description,
        'hot'          => $request->hot,
        'status'       => $request->status
        ];

        if ($request->hasFile('image')) {
            $data['image'] = Helper::getImage('image', 'upload/product', $request);
        }
      // update product
        try {
            DB::beginTransaction();
            $this->product->update($data, $id, 'id');
            DB::commit();
            return redirect()->route('product.index')->with('success', sprintf(config('message.update_success'), 'Sản phẩm'))->withInput();
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->back()->with('error', sprintf(config('message.update_failed'), 'sản phẩm'))->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $message = config('message');
            $product = $this->product->getById($id);
            $user = Auth::user();

            if ($user->user_id !=1 && $product->user_id != $user->id) {
                return response()->json(['status_code' => 401, 'message' => $message['delete_permission_denied']]);
            }
          //update deleted_flag
            $this->product->update(['deleted_flag' => 1], $id);
            return response()->json(['status_code' => 200, 'message' => $message['delete_success']]);
        } catch (\Exception $e) {
            return response()->json(['status_code' => 500, 'message' => $message['internal_server_error']]);
        }
    }

    public function export()
    {
          $data = [
            ['data1', 'data2'],
            ['data3', 'data4']
           ];
           Excel::create('product', function($excel) use($data) {
              $excel->sheet('product', function($sheet) use($data) {
                  $sheet->fromArray($data);
              });
          })->export('xls');
    }
}
