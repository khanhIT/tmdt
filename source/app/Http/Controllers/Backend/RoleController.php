<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Role\RoleInterface;
use App\Http\Requests\RoleRequest;
use DB;
use Auth;

class RoleController extends Controller
{
    private $role;

    public function __construct(
        RoleInterface $role
    ) {
        $this->role = $role;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $this->authorize('view-role');
        } catch (\Exception $e) {
            abort(403);
        }

        $role = $this->role->getListRole();
        return view('Backend.Role.index', compact('role'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Backend.Role.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RoleRequest $request)
    {
        $dataFormRequest = $request->all();
        try {
            $groupName = $dataFormRequest['name'];
            DB::beginTransaction();
            $role = $this->role->create(['name' => $groupName]);
            if(!empty($dataFormRequest)) {
                unset($dataFormRequest['name']);
                unset($dataFormRequest['_token']);
                //get list permission
                $listPermission = $this->permission->all('id asc');
                foreach ($listPermission as $permission) {
                    $data = [
                        'role_id' => $role->id,
                        'users_permission_id' => $permission->id,
                        'status' => in_array($permission->id, $dataFormRequest) ? 1 : 0
                    ];
                    $this->premissionsGroup->create($data);
                }
            }
            DB::commit();
            return redirect()->route('role.index')->with('success', sprintf(config('message.create_success'), 'Quyền'))->withInput();
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->back()->with('error', sprintf(config('message.create_failed'), 'quyền'))->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $role = $this->role->EditRole($id);
        if (empty($role)) {
            abort(404);
        }

        return view('Backend.Role.edit', compact('role'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(RoleRequest $request, $id)
    {
        $dataFormRequest = [
            'name' => $request->role_name
        ];

        try {
            DB::beginTransaction();
            $role = $this->role->update($dataFormRequest, $id);
            DB::commit();

            return redirect()->route('role.index')->with('success', sprintf(config('message.update_success'), 'Quyền'))->withInput();
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->back()->with('error', sprintf(config('message.update_falied'), 'quyền'))->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $message = config('message');
            if (Auth::user()->role_id !=1) {
                return response()->json(['status_code' => 401, 'message' => $message['delete_permission_denied']]);
            }
            //update deleted_flag role
            $this->role->update(['deleted_flag' => 1], $id);

            //update deleted_flag users_permissions_group
            $this->premissionsGroup->updateByMultiConditionsModel(['deleted_flag' => 1], ['role_id' => $id]);

            return response()->json(['status_code' => 200, 'message' => $message['delete_success']]);
        } catch (\Exception $e) {
            return response()->json(['status_code' => 500, 'message' => $message['internal_server_error']]);
        }
    }
}
