<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Customer\CustomerInterface;

class CustomerController extends Controller
{

    private $customer;

    public function __construct
    (
        CustomerInterface $customer
    )
    {
        $this->customer = $customer;
    }
    public function index()
    {
        try {
            $this->authorize('view-customer');
        } catch (\Exception $e) {
            abort(403);
        }

        $customer = $this->customer->getListCustomer();

        return view('Backend.Customer.index', compact('customer'));
    }

    public function show($id)
    {
        $customer = $this->customer->detailCustomer($id);

        if(empty($customer)) {
            abort(404);
        }
        return view('Backend.Customer.show', compact('customer'));
    }
}
