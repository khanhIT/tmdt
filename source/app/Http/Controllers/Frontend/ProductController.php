<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Product\ProductInterface;
use App\Repositories\Category\CategoryInterface;

class ProductController extends Controller
{
    private $product;
    private $category;

    public function __construct
    (
        ProductInterface $product,
        CategoryInterface $category
    )
    {
        $this->product = $product;
        $this->category = $category;
    }

    public function index()
    {
        $productItem = $this->product->getAllProduct();
        return view('Frontend.Product.listProductAll', compact('productItem'));
    }

    public function detailProduct($slug)
    {
        $proDetail = $this->product->detailItemProduct($slug);
        $product = $this->product->getListProductSelling();
        return view('Frontend.Product.Components.detail', compact('proDetail', 'product'));
    }

    public function listCake($id)
    {
        $categoryItem = $this->category->getDetailCategoryItems($id);
        return view('Frontend.Product.listCake', compact('categoryItem'));
    }
}
