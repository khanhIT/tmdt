<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\District\DistrictInterface;
use App\Repositories\Province\ProvinceInterface;
use Stripe;
use Cartalyst\Stripe\Exception\CardErrorException;
use App\Model\Orders;
use App\Model\Product;

class CartController extends Controller
{
    private $province;
    private $district;

    public function __construct
    (
        ProvinceInterface  $province,
        DistrictInterface  $district
    )
    {
        $this->province = $province;
        $this->district = $district;
    }

    public function index()
    {
        return view('Frontend.Cart.index');
    }

    public function loadName($id)
    {
        $checkProvince = $this->district->listAllJoinProvince($id);

        foreach($checkProvince as $province){
            echo "<option  value='".$province->id."'>".$province->name."</option>";
        }
    }

    public function checkout()
    {
        return view('Frontend.Cart.checkout');
    }

    public function postCheckout(Request $request)
    {
        $id = \Auth::guard('customer')->user()->id;
        $total = \Cart::total();
        $contents = \Cart::content()->map(function($item){
            return $item->model->product_name.', '.$item->qty; 
        })->values()->toJson(); 

        try {
            $charge = Stripe::charges()->create([
                'amount' => $total,
                'currency' => 'CAD',
                'source' => $request->stripeToken,
                'description' => 'Orders',
                'receipt_email' => $request->email,
                'metadata' => [
                    'contents' => $contents,
                    'quantity' => \Cart::instance('default')->count()
                ],
            ]);

            foreach(\Cart::content() as $value)
            {
                $data = [
                    'cus_id' => $id,
                    'pro_id' => $value->id,
                    'total_mount' => $total,
                    'qty' => $value->qty
                ];

                if($order = Orders::create($data)) {
                    $id = $order->pro_id;
                    $Sp = Product::find($id);
                    $data = [
                        'qty' => ($Sp->qty)-($order->qty)
                    ];

                    $Sp->update($data);
                }
            }
           
            dispatch(new \App\Jobs\sendOrderCustomer($request->email));
            \Cart::instance('default')->destroy();
            return redirect()->back()->with('success', 'Giao dịch của bạn đã thành công. Cảm ơn bạn đã tin tưởng chúng tôi')->withInput();
        } catch (\Exception $e) {
            return back()->with('error', $e->getMessage());
        }
    }

    public function destroy($id)
    {
        \Cart::remove($id);
        return redirect()->back();
    }

    public function update(Request $request, $id)
    {
        \Cart::update($id, $request->quantity);
        session()->flash('success', 'Update thành công');
        return response()->json(['success' => true]);
    }
}
