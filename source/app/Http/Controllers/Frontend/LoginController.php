<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\District\DistrictRepository;
use App\Repositories\Province\ProvinceRepository;
use App\Repositories\Customer\CustomerRepository;
use App\Http\Requests\RegisterRequest;
use App\Http\Requests\LoginRequest;
use DB;
use Auth;

class LoginController extends Controller
{
    private $district;
    private $province;
    private $customer;

    public function __construct
    (
        DistrictRepository $district,
        ProvinceRepository $province,
        CustomerRepository $customer
    )
    {
        $this->district = $district;
        $this->province = $province;
        $this->customer = $customer;
    }

    public function register()
    {
        $district = $this->district->getDistrict();
        $province = $this->province->getProvince();
        return view('Frontend.Login.register', compact('district', 'province'));
    }

    public function postRegister(RegisterRequest $request)
    {
        $message = config('message.create_success');
        $name = $request->first_name;
        $data = [
            'first_name'  => $name,
            'last_name'   => $request->last_name,
            'email'       => $request->email,
            'phone'       => $request->phone,
            'password'    => bcrypt($request->password),
            'province_id' => $request->province_id,
            'district_id' => $request->district_id,
            'token'       => $request->_token
        ];

        try {
            DB::beginTransaction();
            if ($this->customer->create($data)) {
              $url = route('dang_nhap.active', $request->_token);
              dispatch(new \App\Jobs\sendMailActiveCustomer($request->email, $url, $request->_token));
            }
            DB::commit();
            return redirect()->route('dang_nhap')->with('success', sprintf($message, $name))->withInput();
        }catch(\Exception $e){
            DB::rollBack();
            return redirect()->back();
        }
    }

    public function activeCustomer($token)
    {
         $message = config('message.active_register');
         $active = $this->customer->findBy('token', $token);
         if ($active == null) {
           return redirect()->route('dang_nhap');
         }else{
           $data = [
                'status' => 20,
                'token'  => null
           ];

           $this->customer->update($data, $active->id);
           return redirect()->route('dang_nhap', $active->token)->with('success', sprintf($message))->withInput();
         }
    }

    public function checkName($id)
    {
        $checkProvince = $this->district->listAllJoinProvince($id);

        foreach($checkProvince as $province){
            echo "<option  value='".$province->id."'>".$province->name."</option>";
        }
    }

    public function login()
    {
        return view('Frontend.Login.login');
    }

    public function postLogin(LoginRequest $request)
    {
        $error = config('message.login_wrong');
        $data = [
            'email' => $request->email,
            'password' => $request->password,
            'status' => 20
        ];

        $remember = true;

        $customer = Auth::guard('customer');
        if (!$customer->attempt($data, $remember))
        {
           return redirect()->back()->with('error', $error)->withInput();
        }
        return redirect()->route('index');
    }

    public function logout()
    {
        Auth::guard('customer')->logout();
        return redirect()->route('index');
    }
}
