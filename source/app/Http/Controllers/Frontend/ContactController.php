<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Contact\ContactRepository;
use App\Http\Requests\ContactRequest;
use DB;

class ContactController extends Controller
{
    public  $contact;

    public function __construct
    (
        ContactRepository  $contact
    )
    {
        $this->contact = $contact;
    }

    public function index()
    {
        return view('Frontend.Contact.index');
    }

    public function store(ContactRequest $request)
    {
        $message = config('message.send_contact');
        $data = [
              'full_name' => $request->full_name,
              'email'     => $request->email,
              'phone'     => $request->phone,
              'content'   => $request->content
        ];

        try {
            DB::beginTransaction();
            $this->contact->create($data);
            DB::commit();
            return redirect('/thankyou');
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->back();
        }

    }
}
