<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Recipe\RecipeRepository;
use App\Repositories\Product\ProductRepository;

class HomeController extends Controller
{
    private $recipe;
    private $product;

    public function __construct
    (
        RecipeRepository $recipe,
        ProductRepository $product
    )
    {
        $this->recipe = $recipe;
        $this->product = $product;
    }

    public function index()
    {
        $recipe = $this->recipe->listRecipe();
        $product = $this->product->getListProductSelling();
        $productItem = $this->product->getAllProduct();
        return view('index', compact('recipe', 'product', 'productItem'));
    }

    public function addToCart(Request $request)
    {
        if ($request->qty == 0) {
            toastr()->error('Sản phẩm đang tạm hết hàng. Vui lòng bạn chọn sản phẩm khác. Xin cảm ơn');
            return back();
        }

       \Cart::add($request->id, $request->product_name,1,
           isset($request->sale_price) ? $request->sale_price : $request->price
        )->associate('App\Model\Product');
        return redirect()->route('cart.index')->with('success', sprintf(config('message.success_add_to_cart'), 'Sản phẩm'))->withInput();
    }

    public function searchByName(Request $request)
    {
        $productItem = $this->product->getAllProduct();
        $search = $this->product->searchName($request);
        return view('Frontend.Product.listProductAll', compact('search', 'productItem'));
    }

    public function searchApiByName(Request $request)
    {
        $search = $this->product->searchApiProductName($request);
        return response()->json($search);
    }
}
