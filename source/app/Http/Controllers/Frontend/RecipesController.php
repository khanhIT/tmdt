<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Recipe\RecipeRepository;
use App\Repositories\CommentRecipe\CommentRecipeRepository;
use App\Model\CommentRecipe;

class RecipesController extends Controller
{
    private $recipe;
    private $commentRecipe;

    public function __construct
    (
        RecipeRepository $recipe,
        CommentRecipeRepository $commentRecipe
    )
    {
        $this->recipe = $recipe;
        $this->commentRecipe = $commentRecipe;
    }

    public function index()
    {
        $recipe = $this->recipe->listRecipe();
        $hotRecipe = $this->recipe->activeRepice();
        return view('Frontend.Recipes.listAll', compact('recipe', 'hotRecipe'));
    }

    public function detailRecipe($slug)
    {
        $recipe = $this->recipe->detailRecipeFront($slug);
        $hotRecipe = $this->recipe->activeRepice();
        $commentRecipe = $this->commentRecipe->getCommentRecipe($recipe->id);
        return view('Frontend.Recipes.Components.detail', compact('recipe', 'hotRecipe', 'commentRecipe'));
    }

    public function comment(Request $request)
    {
        $commentRecipe = CommentRecipe::commentRecipe($request);
        return response()->json(['commentRecipe' => $commentRecipe], 200);
    }
}
