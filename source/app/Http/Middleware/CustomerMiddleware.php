<?php

namespace App\Http\Middleware;
use Auth;
use Illuminate\Contracts\Auth\Guard;
use Closure;

class CustomerMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    protected $customer;
    public function __construct(Guard $auth){
        $this->customer = $auth;
    }
    public function handle($request, Closure $next, $guard='customer')
    {
        if (Auth::guard($guard)->guest()) {
            if ($request->ajax() || $request->wantsJson()) {
                return response('Unauthorized.',401);
            }else{
                return redirect()->route('dang_nhap');
            }
        }
        return $next($request);
    }
}
