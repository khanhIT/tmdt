<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use App\Repositories\Category\CategoryRepository;
use Illuminate\Http\Request;
use View;

class AppServiceProvider extends ServiceProvider
{
    private $category;
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(CategoryRepository $category)
    {
        $this->category = $category;
        Schema::defaultStringLength(191);
        $category = $this->category->getAllCategory();
        View::share(compact('category'));
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(
            \App\Repositories\Category\CategoryInterface::class,
            \App\Repositories\Category\CategoryRepository::class
        );

        $this->app->singleton(
            \App\Repositories\Contact\ContactInterface::class,
            \App\Repositories\Contact\ContactRepository::class
        );

        $this->app->singleton(
            \App\Repositories\Customer\CustomerInterface::class,
            \App\Repositories\Customer\CustomerRepository::class
        );

        $this->app->singleton(
            \App\Repositories\District\DistrictInterface::class,
            \App\Repositories\District\DistrictRepository::class
        );

        $this->app->singleton(
            \App\Repositories\Orders\OrdersInterface::class,
            \App\Repositories\Orders\OrdersRepository::class
        );

        $this->app->singleton(
            \App\Repositories\OrdersDetail\OrdersDetailInterface::class,
            \App\Repositories\OrdersDetail\OrdersDetailRepository::class
        );

        $this->app->singleton(
            \App\Repositories\Payment\PaymentInterface::class,
            \App\Repositories\Payment\PaymentRepository::class
        );

        $this->app->singleton(
            \App\Repositories\Product\ProductInterface::class,
            \App\Repositories\Product\ProductRepository::class
        );

        $this->app->singleton(
            \App\Repositories\Province\ProvinceInterface::class,
            \App\Repositories\Province\ProvinceRepository::class
        );

        $this->app->singleton(
            \App\Repositories\Recipe\RecipeInterface::class,
            \App\Repositories\Recipe\RecipeRepository::class
        );

        $this->app->singleton(
            \App\Repositories\CommentRecipe\CommentRecipeInterface::class,
            \App\Repositories\CommentRecipe\CommentRecipeRepository::class
        );

        $this->app->singleton(
            \App\Repositories\Role\RoleInterface::class,
            \App\Repositories\Role\RoleRepository::class
        );

        $this->app->singleton(
            \App\Repositories\Shipper\ShipperInterface::class,
            \App\Repositories\Shipper\ShipperRepository::class
        );

        $this->app->singleton(
            \App\Repositories\Users\UsersInterface::class,
            \App\Repositories\Users\UsersRepository::class
        );
    }
}
