<?php

namespace App\Repositories\Recipe;

use App\Model\Recipe;
use App\Repositories\BaseRepository;
use App\Repositories\Recipe\RecipeInterface;
use App\Model\CommentRecipe;
use App\Common\Constant as Table;

class RecipeRepository extends BaseRepository implements RecipeInterface
{
    const DELETE_FLAG = 0;
    const LIMIT = 10;
    const PER_PAGE = 4;
    const HOT = 1;
    public function getModel()
    {
        return Recipe::class;
    }

    public function listRecipe()
    {
        return $this->model
                    ->where(Table::RECIPE. '.deleted_flag', self::DELETE_FLAG)
                    ->select(
                        Table::RECIPE. '.id',
                        Table::RECIPE. '.user_id',
                        Table::RECIPE. '.title',
                        Table::RECIPE. '.thumbnail',
                        Table::RECIPE. '.slug',
                        Table::RECIPE. '.content',
                        Table::RECIPE. '.description',
                        Table::RECIPE. '.author',
                        Table::RECIPE. '.hot',
                        Table::RECIPE. '.deleted_flag',
                        Table::RECIPE. '.created_at'
                    )
                    ->orderBy(Table::RECIPE. '.id', 'desc')
                    ->paginate(self::LIMIT);
    }

    public function detailRecipe($id)
    {
        return $this->model
                    ->where(Table::RECIPE. '.id', $id)
                    ->select(
                        Table::RECIPE. '.id',
                        Table::RECIPE. '.title',
                        Table::RECIPE. '.thumbnail',
                        Table::RECIPE. '.description',
                        Table::RECIPE. '.content',
                        Table::RECIPE. '.author',
                        Table::RECIPE. '.created_at'
                    )
                    ->first();
    }

    //    frontend
    public function detailRecipeFront($slug)
    {
        return $this->model
            ->where(Table::RECIPE. '.slug', $slug)
            ->select(
                Table::RECIPE. '.id',
                Table::RECIPE. '.title',
                Table::RECIPE. '.thumbnail',
                Table::RECIPE. '.description',
                Table::RECIPE. '.content',
                Table::RECIPE. '.author',
                Table::RECIPE. '.created_at',
                Table::RECIPE. '.slug'
            )
            ->first();
    }

    public function activeRepice()
    {
        return $this->model
            ->where(Table::RECIPE. '.hot', self::HOT)
            ->select(
                Table::RECIPE. '.id',
                Table::RECIPE. '.title',
                Table::RECIPE. '.thumbnail',
                Table::RECIPE. '.slug'
            )
            ->get();
    }
}
