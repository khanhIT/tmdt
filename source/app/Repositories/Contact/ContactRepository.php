<?php

namespace App\Repositories\Contact;

use App\Model\Contact;
use App\Repositories\BaseRepository;
use App\Repositories\Contact\ContactInterface;
use App\Common\Constant as Table;

class ContactRepository extends BaseRepository implements ContactInterface
{
    const DELETE_FLAG = 0;
    const LIMIT = 10;

    public function getModel()
    {
        return Contact::class;
    }

    public function getListContact()
    {
        return $this->model
                    ->where(Table::CONTACT. '.deleted_flag', self::DELETE_FLAG)
                    ->select(Table::CONTACT. '.*')
                    ->orderBy(Table::CONTACT. '.id', 'desc')
                    ->paginate(self::LIMIT);
    }

    public function detailContact($id)
    {
        return $this->model
                    ->where(Table::CONTACT. '.id', $id)
                    ->select(Table::CONTACT. '.*')
                    ->first();
    }
}
