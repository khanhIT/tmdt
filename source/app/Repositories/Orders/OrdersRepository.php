<?php
 
namespace App\Repositories\Orders;

use App\Model\Orders;
use App\Repositories\BaseRepository;
use App\Repositories\Orders\OrdersInterface;
use App\Common\Constant as Table;

class OrdersRepository extends BaseRepository implements OrdersInterface
{
    const LIMIT = 10;
    const DELETE_FLAG = 0;

    public function getModel()
    {
        return Orders::class;
    }

    public function getlistOrder()
    {
        return $this->model
                    ->join(Table::CUSTOMER, Table::ORDERS. '.cus_id' , '=', Table::CUSTOMER. '.id')
                    ->join(Table::PRODUCT, Table::ORDERS. '.pro_id', '=', Table::PRODUCT. '.id')
                    ->where(Table::ORDERS. '.deleted_flag', self::DELETE_FLAG)
                    ->select(Table::ORDERS. '.*', Table::CUSTOMER. '.first_name', Table::CUSTOMER. '.last_name',
                        Table::PRODUCT. '.product_name'
                    )
                    ->orderBy(Table::ORDERS. '.id', 'desc')
                    ->paginate(self::LIMIT);
    }

    public function detailOrders($id)
    {
        return $this->model
                    ->join(Table::CUSTOMER, Table::ORDERS. '.cus_id' , '=', Table::CUSTOMER. '.id')
                    ->join(Table::PRODUCT, Table::ORDERS. '.pro_id', '=', Table::PRODUCT. '.id')
                    ->where(Table::ORDERS. '.id', $id)
                    ->select(Table::ORDERS. '.*', Table::PRODUCT. '.product_name', Table::PRODUCT. '.price', Table::PRODUCT. '.sale_price', Table::CUSTOMER. '.first_name', Table::CUSTOMER. '.last_name', Table::CUSTOMER. '.email', Table::CUSTOMER. '.phone',
                    )
                    ->first();
    }

    // frontend
    // lịch sử đơn hàng
    public function getlistHistoryOrder()
    {
        return $this->model
                    ->join(Table::CUSTOMER, Table::ORDERS. '.cus_id' , '=', Table::CUSTOMER. '.id')
                    ->join(Table::PRODUCT, Table::ORDERS. '.pro_id', '=', Table::PRODUCT. '.id')
                    ->where(Table::ORDERS. '.cus_id', \Auth::guard('customer')->user()->id)
                    ->select(Table::ORDERS. '.*', Table::CUSTOMER. '.first_name', Table::CUSTOMER. '.last_name',
                        Table::PRODUCT. '.product_name', Table::PRODUCT. '.image', Table::PRODUCT. '.price', 
                        Table::PRODUCT. '.sale_price'
                    )
                    ->orderBy(Table::ORDERS. '.id', 'desc')
                    ->get();
    }
}
