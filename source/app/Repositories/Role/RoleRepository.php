<?php
 
namespace App\Repositories\Role;

use App\Model\Role;
use App\Repositories\BaseRepository;
use App\Repositories\Role\RoleInterface;
use App\Common\Constant as Table;

class RoleRepository extends BaseRepository implements RoleInterface
{

    public function getModel()
    {
        return Role::class;
    }

    public function getListRole()
    {
        return $this->model
                    ->where(Table::ROLE. '.deleted_flag', 0)
                    ->select(Table::ROLE. '.id', Table::ROLE. '.name')
                    ->orderby(Table::ROLE. '.id', 'desc')
                    ->paginate(5);
    }

    public function EditRole($id)
    {
        return $this->model
                    ->where(Table::ROLE. '.id', $id)
                    ->select(Table::ROLE. '.id', Table::ROLE. '.name')
                    ->first();
    }
}
