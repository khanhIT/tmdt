<?php
 
namespace App\Repositories\Customer;

use App\Model\Customer;
use App\Repositories\BaseRepository;
use App\Repositories\Customer\CustomerInterface;
use App\Common\Constant as Table;

class CustomerRepository extends BaseRepository implements CustomerInterface
{
    const STATUS = 20;
    const LIMIT = 10;

    public function getModel()
    {
        return Customer::class;
    }

    public function getListCustomer()
    {
        return $this->model
                    ->leftjoin(Table::PROVINCE,Table::CUSTOMER .'.province_id', '=', Table::PROVINCE .'.id')
                    ->leftjoin(Table::DISTRICT,Table::CUSTOMER .'.district_id', '=', Table::DISTRICT .'.id')
                    // ->where(Table::CUSTOMER. '.status', self::STATUS)
                    ->select(Table::CUSTOMER .'.*', Table::PROVINCE .'.name as province_name', Table::DISTRICT .'.name as district_name')
                    ->orderBy(Table::CUSTOMER .'.id', 'desc')
                    ->paginate(self::LIMIT);
    }

    public function detailCustomer($id)
    {
        return $this->model
                    ->leftjoin(Table::PROVINCE,Table::CUSTOMER .'.province_id', '=', Table::PROVINCE .'.id')
                    ->leftjoin(Table::DISTRICT,Table::CUSTOMER .'.district_id', '=', Table::DISTRICT .'.id')
                    ->where(Table::CUSTOMER. '.status', self::STATUS)
                    ->select(Table::CUSTOMER .'.*', Table::PROVINCE .'.name as province_name', Table::DISTRICT .'.name as district_name'
                    )
                    ->first();
    }
}
