<?php
 
namespace App\Repositories\District;

use App\Model\District;
use App\Repositories\BaseRepository;
use App\Repositories\District\DistrictInterface;
use App\Common\Constant as Table;

class DistrictRepository extends BaseRepository implements DistrictInterface
{

    public function getModel()
    {
        return District::class;
    }

    public function getDistrict()
    {
    	return $this->model
    				->select(Table::DISTRICT. '.*')
					->get();
    }

    public function listAllJoinProvince($id)
    {
        return $this->model
                    ->join(Table::PROVINCE, Table::DISTRICT. '.province_id', '=', Table::PROVINCE. '.id')
                    ->where(Table::DISTRICT. '.province_id', $id)
                    ->select(Table::DISTRICT. '.*', Table::PROVINCE. '.name as name_province')
                    ->get();
    }
}
