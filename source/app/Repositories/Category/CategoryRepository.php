<?php
 
namespace App\Repositories\Category;

use App\Model\Category;
use App\Repositories\BaseRepository;
use App\Repositories\Category\CategoryInterface;
use App\Common\Constant as Table;

class CategoryRepository extends BaseRepository implements CategoryInterface
{
    const LIMIT = 10;
    public function getModel()
    {
        return Category::class;
    }

    public function getListCategory()
    {
        return $this->model
                    ->select(Table::CATEGORY. '.*')
                    ->where(Table::CATEGORY. '.deleted_flag', 0)
                    ->orderBy(Table::CATEGORY. '.id', 'desc')
                    ->paginate(self::LIMIT);
    }

    public function getEditCategory($id)
    {
        return $this->model
                    ->where(Table::CATEGORY. '.id', $id)
                    ->select(Table::CATEGORY. '.id', Table::CATEGORY. '.name')
                    ->first();
    }

//    Frontend
    public function getAllCategory()
    {
        return $this->model
            ->select(Table::CATEGORY. '.id', Table::CATEGORY. '.name')
            ->get();
    }

    public function getDetailCategoryItems($id)
    {
        return $this->model
            ->join(Table::PRODUCT, Table::CATEGORY. '.id', '=', Table::PRODUCT. '.category_id')
            ->where(Table::CATEGORY. '.id', $id)
            ->select(
                Table::CATEGORY. '.id', Table::CATEGORY. '.name',
                Table::PRODUCT. '.product_name', Table::PRODUCT. '.price',
                Table::PRODUCT. '.sale_price', Table::PRODUCT. '.category_id', Table::PRODUCT. '.image',
                Table::PRODUCT. '.description', Table::PRODUCT. '.hot', Table::PRODUCT. '.status'
            )
            ->paginate(self::LIMIT);
    }
}
