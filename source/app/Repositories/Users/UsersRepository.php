<?php
 
namespace App\Repositories\Users;

use App\Model\Users;
use App\Repositories\BaseRepository;
use App\Repositories\Users\UsersInterface;
use App\Common\Constant as Table;

class UsersRepository extends BaseRepository implements UsersInterface
{

    public function getModel()
    {
        return Users::class;
    }

    public function getListUser()
    {
        $user = $this->model
                     ->leftjoin(Table::ROLE, Table::USERS. '.role_id', '=', Table::ROLE. '.id')
                     ->where(Table::USERS. '.deleted_flag', 0)
                     ->select(
                         Table::USERS. '.id',
                         Table::USERS. '.role_id',
                         Table::USERS. '.name',
                         Table::USERS. '.email',
                         Table::USERS. '.avatar',
                         Table::USERS. '.phone',
                         Table::USERS. '.address',
                         Table::USERS. '.birthday',
                         Table::USERS. '.active',
                         Table::ROLE. '.name as role_name'
                     )
                     ->orderBy(Table::USERS. '.id', 'desc')
                     ->paginate(10);

        return $user;
    }
}
