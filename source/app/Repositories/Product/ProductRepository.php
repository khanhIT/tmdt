<?php

namespace App\Repositories\Product;

use App\Model\Product;
use App\Repositories\BaseRepository;
use App\Repositories\Product\ProductInterface;
use App\Common\Constant as Table;

class ProductRepository extends BaseRepository implements ProductInterface
{
    const LIMIT = 10;
    const STATUS = 1;
    const DELETE_FLAG = 0;

    public function getModel()
    {
        return Product::class;
    }

    public function listIndexItemProduct($limit = self::LIMIT)
    {
        return $this->model
                  ->leftjoin(Table::CATEGORY, Table::PRODUCT. '.category_id', '=', Table::CATEGORY. '.id')
                  ->where(Table::PRODUCT. '.deleted_flag', self::DELETE_FLAG)
                  ->select(Table::PRODUCT. '.*', Table::CATEGORY. '.name')
                  ->orderBy(Table::PRODUCT. '.id', 'desc')
                  ->paginate($limit);
    }

    public function detailProduct($id)
    {
        return $this->model
            ->where(Table::PRODUCT. '.id', $id)
            ->select(Table::PRODUCT. '.*')
            ->first();
    }

//    Frontend
    public function getAllProduct()
    {
        return $this->model
            ->where(Table::PRODUCT. '.deleted_flag', self::DELETE_FLAG)
            ->select(Table::PRODUCT. '.*')
            ->orderBy(Table::PRODUCT. '.id', 'desc')
            ->paginate(self::LIMIT);
    }

    public function getListProductSelling()
    {
        return $this->model
                    ->where(Table::PRODUCT. '.deleted_flag', self::DELETE_FLAG)
                    ->where(Table::PRODUCT. '.hot', self::STATUS)
                    ->select(Table::PRODUCT. '.*')
                    ->orderBy(Table::PRODUCT. '.id', 'desc')
                    ->get();
    }

    public function detailItemProduct($name)
    {
        return $this->model
            ->where(Table::PRODUCT. '.product_name', $name)
            ->select(Table::PRODUCT. '.*')
            ->first();
    }

    public function searchName($request)
    {
        return $this->model
              ->where(Table::PRODUCT. '.product_name', 'like', '%'. $request->get('key') .'%')
              ->select(Table::PRODUCT. '.*')
              ->orderBy(Table::PRODUCT. '.id', 'desc')
              ->paginate(self::LIMIT);
    }

    public function searchApiProductName($request)
    {
        return $this->model
              ->where(Table::PRODUCT. '.product_name', 'like', '%'. $request->get('key') .'%')
              ->select(Table::PRODUCT. '.*')
              ->get();
    }
}
