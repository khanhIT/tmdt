<?php
 
namespace App\Repositories\Province;

use App\Model\Province;
use App\Repositories\BaseRepository;
use App\Repositories\Province\ProvinceInterface;
use App\Common\Constant as Table;

class ProvinceRepository extends BaseRepository implements ProvinceInterface
{

    public function getModel()
    {
        return Province::class;
    }

    public function getProvince()
    {
    	return $this->model
    				->select(Table::PROVINCE. '.*')
    				->get();		
    }
}
