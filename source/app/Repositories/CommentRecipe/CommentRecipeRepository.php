<?php

namespace App\Repositories\CommentRecipe;

use App\Model\CommentRecipe;
use App\Repositories\BaseRepository;
use App\Repositories\CommentRecipe\CommentRecipeInterface;
use App\Common\Constant as Table;

class CommentRecipeRepository extends BaseRepository implements CommentRecipeInterface
{

    public function getModel()
    {
        return CommentRecipe::class;
    }

    public function getCommentRecipe($id)
    {
        return $this->model
            ->select(Table::COMMENT_RECIPE. '.*')
            ->where(Table::COMMENT_RECIPE. '.recipe_id', $id)
            ->orderBy(Table::COMMENT_RECIPE. '.id', 'desc')
            ->get();
    }
}
