let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
   .sass('resources/assets/sass/app.scss', 'public/css');
mix.styles([
   'public/frontend/font/font.css',
   'public/frontend/css/base.css',
   'public/frontend/css/home/header.css',
   'public/frontend/css/home/slide.css',
   'public/frontend/css/home/productSelling.css',
   'public/frontend/css/home/banner.css',
   'public/frontend/css/home/product.css',
   'public/frontend/css/home/recipe.css',
   'public/frontend/css/home/about.css',
   'public/frontend/css/home/footer.css',
   'public/frontend/css/intro/intro.css',
   'public/frontend/css/product/products.css',
   'public/frontend/css/product/productDetail.css',
   'public/frontend/css/recipes/recipes.css',
   'public/frontend/css/recipes/recipesDetail.css',
   'public/frontend/css/cart/cart.css',
   'public/frontend/css/cart/checkout.css',
   'public/frontend/css/contact/contact.css'
], 'public/css/all.css');
