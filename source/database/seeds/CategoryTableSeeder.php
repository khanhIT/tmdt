<?php

use Illuminate\Database\Seeder;

// @codingStandardsIgnoreLine
class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            0 => [
                'name' => 'Bánh Cookies'
            ],

            2 => [
                'name' => 'Bánh Lạnh'
            ],

            3 => [
                'name' => 'Bánh Kem'
            ],

            4 => [
                'name' => 'Bánh Nướng'
            ]
        ];

        foreach ($data as $key => $value) {
            DB::table('category')->insert($value);
        }
    }
}
