<?php

use Illuminate\Database\Seeder;

// @codingStandardsIgnoreLine
class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::raw('TRUNCATE TABLE role');

        $data = [
            0 => [
                'name' => 'Admin'
            ],

            1 => [
                'name' => 'Manager'
            ],

            2 => [
                'name' => 'Super Admin'
            ],

            3 => [
                'name' => 'HR ( Human Resource )'
            ],
        ];

        foreach ($data as $key => $value) {
            DB::table('role')->insert($value);
        }
    }
}
