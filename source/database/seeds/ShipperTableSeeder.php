<?php

use Illuminate\Database\Seeder;

// @codingStandardsIgnoreLine
class ShipperTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::raw('TRUNCATE TABLE shipper');
        $data = [
            0 => [
                'name' => 'Chưa gửi'
            ],

            1 => [
                'name' => 'Đang gửi'
            ],

            2 => [
                'name' => 'Gửi xong'
            ]
        ];

        foreach ($data as $key => $value) {
            DB::table('shipper')->insert($value);
        }
    }
}
