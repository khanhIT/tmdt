<?php

use Illuminate\Database\Seeder;

// @codingStandardsIgnoreLine
class PaymentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::raw('TRUNCATE TABLE payment');
        $data = [
            0 => [
                'name' => 'Chưa thanh toán'
            ],
            1 => [
                'name' => 'Thanh toán một phần'
            ],
            2 => [
                'name' => 'Thanh toán xong'
            ],
            3 => [
                'name' => 'Khách dư tiền thừa'
            ]
        ];

        foreach ($data as $key => $value) {
            DB::table('payment')->insert($value);
        }
    }
}
